var testAccountDetails = {
    qualification: {
        RedeemProgram: false,
        Netflix: false,
        RefererResponse: {
            registerUpdated: false,
            paperless: true,
            solvent: false
        },
        NetflixResponse: {
            solvent: false,
            Account_Status: true,
            CreditLimit: true
        }
    },
    AccounInfo: {
        addressLinesField: {
            line1Field: null,
            line2Field: null,
            PropertyChanged: null
        },
        invoicesField: [],
        bANField: 693090265,
        sSNField: '************',
        emailField: 'axel.torres@claropr.com',
        contractMonthsField: null,
        accountTypeField: 'I2',
        accountSubtypeField: '4',
        banStatusField: 'O',
        billCycleField: 8,
        billDateField: '02/04/2019',
        billDueDateField: '03/04/2019',
        billBalanceField: '118.52',
        pastDueAmountField: 118.52,
        lastPaymentDateField: null,
        lastPaymentAmountField: 0,
        cycleStartDateField: '02/04/2019',
        cycleEndDateField: '03/03/2019',
        cycleDateField: 'Feb 4 a Mar 3',
        cycleDaysLeftField: '19',
        creditClassField: 'C',
        creditBalanceField: '  350.00',
        firstNameField: 'AXA',
        lastNameField: 'LOPEZ',
        addressField: 'PO BOX 13553 PO BOX 13553',
        cityField: 'SAN JUAN',
        stateField: 'PR',
        countryField: 'USA',
        zIPField: '00908',
        invoiceURLField: null,
        paperlessField: false,
        hasErrorField: false,
        errorDescField: null,
        errorNumField: 0,
        PropertyChanged: null
    },
    SubscriberInfo: [
        {
            equipmentInfoField: {
                itemIdField: '30336R',
                itemDescriptionField: 'LG K10 2017',
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'BAS20',
                sOCDescriptionField: '600MIN+PRT+MTM+N&W+500SMS/MMS',
                relatedSocCodeField: 'not_set',
                socRateField: 20,
                totalRateField: 20,
                commitmentStartDateField: '0',
                mCommitmentOrigNoMonthField: '0',
                commitmentEndDateField: '0',
                effectiveDateField: '02/15/2016',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: '17876129063',
                            offerIdField: 'UNIM15GB',
                            balanceIdField: 'PR_USA_BalanceDef_10106',
                            balanceAmountField: '1093637',
                            balanceAmountUnitField: '1.04 MB',
                            effectiveDateField: '2019-02-09T00:00:00',
                            expiryDateField: '2019-03-09T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7876129063',
                        quotaField: '16106127361',
                        quotaTextField: '15 GB',
                        displayNameField: 'IM Ilimitado PUJ 15GB',
                        nameField: 'UNIM15GB',
                        offerIdField: 'UNIM15GB',
                        usedField: '1093637',
                        usedTextField: '1.04 MB',
                        offerGroupField: 'BASE NT_PROMO_NY16 POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7876129063',
                        quotaField: '20971525',
                        quotaTextField: '20 MB',
                        displayNameField: 'Roaming Data a Granel',
                        nameField: 'ROAMPYG',
                        offerIdField: 'ROAMPYG',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'LIMITED POS ROAMINGADDITIONAL NT_ROAM_GRANEL',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                lDIUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [
                    {
                        offerIdField: 'RUP250',
                        nameField: 'RUP250',
                        displayNameField: 'Booster Recurrente 250MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$4.99',
                        dPriceField: 4.99,
                        volumeField: '262144000',
                        volumeValueField: 0,
                        volumeUnitField: '250 MB',
                        offerGroupField: 'ADDITIONAL, AUTOTOPUP_5, PRICE_4,99, POS, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP250',
                        nameField: 'UP250',
                        displayNameField: 'Booster 250MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$4.99',
                        dPriceField: 4.99,
                        volumeField: '262144000',
                        volumeValueField: 0,
                        volumeUnitField: '250 MB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'RUP1GB',
                        nameField: 'RUP1GB',
                        displayNameField: 'Booster Recurrente 1GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$9.99',
                        dPriceField: 9.99,
                        volumeField: '1073741824',
                        volumeValueField: 0,
                        volumeUnitField: '1 GB',
                        offerGroupField: 'ADDITIONAL, AUTOTOPUP_5, POS, PRICE_9,99, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP1GB',
                        nameField: 'UP1GB',
                        displayNameField: 'Booster 1GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$9.99',
                        dPriceField: 9.99,
                        volumeField: '1073741824',
                        volumeValueField: 0,
                        volumeUnitField: '1 GB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'RUP3GB',
                        nameField: 'RUP3GB',
                        displayNameField: 'Booster Recurrente 3GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$19.99',
                        dPriceField: 19.99,
                        volumeField: '3221225472',
                        volumeValueField: 0,
                        volumeUnitField: '3 GB',
                        offerGroupField: 'ADDITIONAL, AUTOTOPUP_5, PRICE_19,99, POS, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP3GB',
                        nameField: 'UP3GB',
                        displayNameField: 'Booster 3GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$19.99',
                        dPriceField: 19.99,
                        volumeField: '3221225472',
                        volumeValueField: 0,
                        volumeUnitField: '3 GB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP5GB',
                        nameField: 'UP5GB',
                        displayNameField: 'Booster 5GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$24.99',
                        dPriceField: 24.99,
                        volumeField: '5368709120',
                        volumeValueField: 0,
                        volumeUnitField: '5 GB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'RUP5GB',
                        nameField: 'RUP5GB',
                        displayNameField: 'Booster Recurrente 5GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$24.99',
                        dPriceField: 24.99,
                        volumeField: '5368709120',
                        volumeValueField: 0,
                        volumeUnitField: '5 GB',
                        offerGroupField: 'PRICE_24,99, ADDITIONAL, AUTOTOPUP_5, POS, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    }
                ],
                roamingPackagesField: [
                    {
                        offerIdField: 'ROA120',
                        nameField: 'ROA120',
                        displayNameField: 'Data Roam Int 120MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$30.00',
                        dPriceField: 30,
                        volumeField: '125829120',
                        volumeValueField: 0,
                        volumeUnitField: '120 MB',
                        offerGroupField: 'NT_ADD_ROAM_LIMITED, POS, LIMITED, ROAMINGADDITIONAL, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'ROA350',
                        nameField: 'ROA350',
                        displayNameField: 'Data Roam Int 350MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$60.00',
                        dPriceField: 60,
                        volumeField: '367001600',
                        volumeValueField: 0,
                        volumeUnitField: '350 MB',
                        offerGroupField: 'NT_ADD_ROAM_LIMITED, POS, LIMITED, ROAMINGADDITIONAL, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'ROA850',
                        nameField: 'ROA850',
                        displayNameField: 'Data Roam Int 850MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$120.00',
                        dPriceField: 120,
                        volumeField: '891289600',
                        volumeValueField: 0,
                        volumeUnitField: '850 MB',
                        offerGroupField: 'NT_ADD_ROAM_LIMITED, POS, LIMITED, ROAMINGADDITIONAL, ',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '50523H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100152159814267',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '30336R',
                        equipmentTypeField: 'J',
                        eSNField: '448896202956851',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50023',
                        equipmentTypeField: 'S',
                        eSNField: '89011100095110936085',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '40283H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100131140726673',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50114',
                        equipmentTypeField: 'I',
                        eSNField: '354195026684217',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50355',
                        equipmentTypeField: 'J',
                        eSNField: '354843040518271',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '40581H',
                        equipmentTypeField: 'J',
                        eSNField: '359208055036764',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50808H',
                        equipmentTypeField: 'J',
                        eSNField: '356924070693131',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'GMTMFREET',
                        relatedSocCodeField: 'BAS20',
                        effectiveDateField: '2/15/2016 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'BAS20',
                        effectiveDateField: '2/15/2016 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'BAS20',
                        effectiveDateField: '2/15/2016 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'DATAGRA',
                        relatedSocCodeField: '',
                        effectiveDateField: '1/9/2012 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '1/9/2012 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'SMSDONTG',
                        relatedSocCodeField: '',
                        effectiveDateField: '11/14/2011 11:00:00 PM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '7876129063',
            productTypeField: 'G',
            subscriberStatusField: 'A',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'FPNC',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        },
        {
            equipmentInfoField: {
                itemIdField: null,
                itemDescriptionField: null,
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'BASE39D',
                sOCDescriptionField: 'VOZILI+SMS/MMSILI+500MB',
                relatedSocCodeField: 'not_set',
                socRateField: 39.99,
                totalRateField: 39.99,
                commitmentStartDateField: '08/02/2013',
                mCommitmentOrigNoMonthField: '24',
                commitmentEndDateField: '08/02/2015',
                effectiveDateField: '08/02/2013',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: '19392437488',
                            offerIdField: 'ALT10M',
                            balanceIdField: 'PR_BalanceDef_CUNL_10443',
                            balanceAmountField: '0',
                            balanceAmountUnitField: '0 MB',
                            effectiveDateField: '2019-02-09T00:00:00',
                            expiryDateField: '2019-03-09T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392437488',
                        quotaField: '644245094400',
                        quotaTextField: '600 GB',
                        displayNameField: '10Mbps Down/ 1Mbps Up',
                        nameField: 'ALT10M',
                        offerIdField: 'ALT10M',
                        usedField: '0',
                        usedTextField: '0 MB',
                        offerGroupField: 'NT_NO POS BASE UNLIMITED',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [
                    {
                        offerIdField: 'RUP250',
                        nameField: 'RUP250',
                        displayNameField: 'Booster Recurrente 250MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$4.99',
                        dPriceField: 4.99,
                        volumeField: '262144000',
                        volumeValueField: 0,
                        volumeUnitField: '250 MB',
                        offerGroupField: 'ADDITIONAL, AUTOTOPUP_5, PRICE_4,99, POS, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP250',
                        nameField: 'UP250',
                        displayNameField: 'Booster 250MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$4.99',
                        dPriceField: 4.99,
                        volumeField: '262144000',
                        volumeValueField: 0,
                        volumeUnitField: '250 MB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'RUP1GB',
                        nameField: 'RUP1GB',
                        displayNameField: 'Booster Recurrente 1GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$9.99',
                        dPriceField: 9.99,
                        volumeField: '1073741824',
                        volumeValueField: 0,
                        volumeUnitField: '1 GB',
                        offerGroupField: 'ADDITIONAL, POS, AUTOTOPUP_5, PRICE_9,99, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP1GB',
                        nameField: 'UP1GB',
                        displayNameField: 'Booster 1GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$9.99',
                        dPriceField: 9.99,
                        volumeField: '1073741824',
                        volumeValueField: 0,
                        volumeUnitField: '1 GB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP3GB',
                        nameField: 'UP3GB',
                        displayNameField: 'Booster 3GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$19.99',
                        dPriceField: 19.99,
                        volumeField: '3221225472',
                        volumeValueField: 0,
                        volumeUnitField: '3 GB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'RUP3GB',
                        nameField: 'RUP3GB',
                        displayNameField: 'Booster Recurrente 3GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$19.99',
                        dPriceField: 19.99,
                        volumeField: '3221225472',
                        volumeValueField: 0,
                        volumeUnitField: '3 GB',
                        offerGroupField: 'ADDITIONAL, PRICE_19,99, POS, AUTOTOPUP_5, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'UP5GB',
                        nameField: 'UP5GB',
                        displayNameField: 'Booster 5GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$24.99',
                        dPriceField: 24.99,
                        volumeField: '5368709120',
                        volumeValueField: 0,
                        volumeUnitField: '5 GB',
                        offerGroupField: 'ADDITIONAL, POS, UNLIMITED, NT_ADD_UNLIMITED, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'RUP5GB',
                        nameField: 'RUP5GB',
                        displayNameField: 'Booster Recurrente 5GB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$24.99',
                        dPriceField: 24.99,
                        volumeField: '5368709120',
                        volumeValueField: 0,
                        volumeUnitField: '5 GB',
                        offerGroupField: 'PRICE_24,99, ADDITIONAL, POS, AUTOTOPUP_5, UNLIMITED, NT_ADD_UNLIMITED_RECUR, ',
                        PropertyChanged: null
                    }
                ],
                roamingPackagesField: [
                    {
                        offerIdField: 'ROA120',
                        nameField: 'ROA120',
                        displayNameField: 'Data Roam Int 120MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$30.00',
                        dPriceField: 30,
                        volumeField: '125829120',
                        volumeValueField: 0,
                        volumeUnitField: '120 MB',
                        offerGroupField: 'NT_ADD_ROAM_LIMITED, POS, LIMITED, ROAMINGADDITIONAL, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'ROA350',
                        nameField: 'ROA350',
                        displayNameField: 'Data Roam Int 350MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$60.00',
                        dPriceField: 60,
                        volumeField: '367001600',
                        volumeValueField: 0,
                        volumeUnitField: '350 MB',
                        offerGroupField: 'NT_ADD_ROAM_LIMITED, POS, LIMITED, ROAMINGADDITIONAL, ',
                        PropertyChanged: null
                    },
                    {
                        offerIdField: 'ROA850',
                        nameField: 'ROA850',
                        displayNameField: 'Data Roam Int 850MB',
                        isRecurringField: 'false',
                        lengthField: '1',
                        unitField: 'M',
                        priceField: '$120.00',
                        dPriceField: 120,
                        volumeField: '891289600',
                        volumeValueField: 0,
                        volumeUnitField: '850 MB',
                        offerGroupField: 'NT_ADD_ROAM_LIMITED, POS, LIMITED, ROAMINGADDITIONAL, ',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '50023',
                        equipmentTypeField: 'S',
                        eSNField: '89011100076100711167',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50014',
                        equipmentTypeField: 'I',
                        eSNField: '351961012010516',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50480H',
                        equipmentTypeField: 'J',
                        eSNField: '356936054884312',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50182',
                        equipmentTypeField: 'I',
                        eSNField: '358253032466578',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50362H',
                        equipmentTypeField: 'J',
                        eSNField: '358354040233982',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50358',
                        equipmentTypeField: 'J',
                        eSNField: '359744040717118',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50182',
                        equipmentTypeField: 'I',
                        eSNField: '351540048756738',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'FBKTWT',
                        relatedSocCodeField: '',
                        effectiveDateField: '10/30/2014 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'WHATSAPP',
                        relatedSocCodeField: '',
                        effectiveDateField: '9/23/2014 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'IPHPEN',
                        relatedSocCodeField: '',
                        effectiveDateField: '8/2/2013 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'BASE39D',
                        effectiveDateField: '8/2/2013 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'DATRO500',
                        relatedSocCodeField: 'BASE39D',
                        effectiveDateField: '8/2/2013 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GUNSMSOC',
                        relatedSocCodeField: 'BASE39D',
                        effectiveDateField: '8/2/2013 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'BASE39D',
                        effectiveDateField: '8/2/2013 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MMSPROV',
                        relatedSocCodeField: '',
                        effectiveDateField: '12/12/2012 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'DATAGRA',
                        relatedSocCodeField: '',
                        effectiveDateField: '9/5/2012 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '1/9/2012 11:00:00 PM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '9392437488',
            productTypeField: 'G',
            subscriberStatusField: 'C',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'ABTO',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        },
        {
            equipmentInfoField: {
                itemIdField: null,
                itemDescriptionField: null,
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: '1999BA350',
                sOCDescriptionField: '350 MIN+INCO+PRT+MTM',
                relatedSocCodeField: 'not_set',
                socRateField: 19.99,
                totalRateField: 19.99,
                commitmentStartDateField: '01/04/2010',
                mCommitmentOrigNoMonthField: '24',
                commitmentEndDateField: '01/04/2012',
                effectiveDateField: '01/04/2010',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [],
                roamingPackagesField: [],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '50023',
                        equipmentTypeField: 'S',
                        eSNField: '89011100095110936085',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50110',
                        equipmentTypeField: 'J',
                        eSNField: '356778022462346',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'ROAMUSA75',
                        relatedSocCodeField: '1999BA350',
                        effectiveDateField: '1/4/2010 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GENHANC0C',
                        relatedSocCodeField: '1999BA350',
                        effectiveDateField: '1/4/2010 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: '1999BA350',
                        effectiveDateField: '1/4/2010 11:00:00 PM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '9392537488',
            productTypeField: 'G',
            subscriberStatusField: 'C',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'CR',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        }
    ],
    HasError: false,
    ErrorDesc: null,
    ErrorNum: 0
};