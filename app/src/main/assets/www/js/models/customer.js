$(function() {

    // Application Model
    // ----------
    app.models.Customer = Backbone.Model.extend({

        initialize: function() {
            //...
        },

        validateAccount: function(subscriber, code, successCB, errorCB){

            const parameters = JSON.stringify({
                Subscriber: subscriber,
                code: code
            });

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};

            const method = 'validateGuestAccount';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB);
        },

        validateGuest: function(token, code, subscriber, successCB, errorCB) {

            const parameters = JSON.stringify({
                subscriber: subscriber,
                code: code,
                device: app.device
            });

            const headers = { 'Authorization': 'Bearer ' + token};

            const method = 'validateGuestAccount';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB );
        },

        accountDetails: function(subscriber, account, successCB, errorCB){

            const parameters = JSON.stringify({
                subscriber: subscriber,
                account: account
            });

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};

            const method = 'getAccountDetails';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB); // TODO, descomentar
            //var accountDetails = testAccountDetails; // TODO borrar
            //successCB(accountDetails); // TODO borrar
        },

        userAccess: function(subscriber, account, successCB, errorCB){

            const parameters = JSON.stringify({
                subscriber: subscriber,
                account: account
            });

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};

            const method = 'getAccess';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB); // TODO, descomentar
            //var accountAccess = testAccess; // TODO borrar
            //successCB(accountAccess); // TODO borrar
        },



        passwordUpdate: function(currentPassword, newPassword, successCB, errorCB){

            const parameters = JSON.stringify({
                currentPassword: currentPassword,
                newPassword: newPassword
            });

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};

            const method = 'setPasswordUpdate';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB);
        },

        getBan: function(account, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');

            const parameters = JSON.stringify({
                BAN: account,
                method: 'getBan',
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        updateBillParameters: function(account, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');

            const parameters = JSON.stringify({
                Ban: account,
                method: 'updateBillParameters',
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        getMember: function(account, successCB, errorCB){

            const parameters = JSON.stringify({
                account: account,
            });

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};

            const method = 'getMember';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB);
        },

        addMember: function(data, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};
            data.token = tokenSession;
            const parameters = JSON.stringify(data);

            const method = 'addMember';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB);
        },

        addCampaignAlerts: function(account, subscriber, emails, userLink, successCB, errorCB){

            const parameters = JSON.stringify({
                account: account,
                emails: emails,
                subscriber: subscriber,
                userLink: userLink
            });

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const headers = { 'Authorization': 'Bearer ' + tokenSession};

            const method = 'addCampaignAlerts';

            app.utils.network.requestCustomers(method, headers, parameters, successCB, errorCB);
        },



    });
});