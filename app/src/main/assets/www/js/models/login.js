$(function() {

    // Application Model
    // ----------
    app.models.Login = Backbone.Model.extend({

        initialize: function() {
            //...
        },

        login: function(username, password, successCB, errorCB){

            const parameters = JSON.stringify({
                Username: username,
                Password: password
            });

            const method = 'authenticate';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        validateSubscriber: function(subscriber, successCB, errorCB) {

            const parameters = JSON.stringify({
                Subscriber: subscriber,
            });

            const method = 'registerUserValidateSubscriber';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        validateSSNAndEmail: function(subscriber, ssn, email, successCB, errorCB){

            const parameters = JSON.stringify({
                Subscriber: subscriber,
                SSN: ssn,
                Email: email,
            });

            const method = 'registerUserValidateSSNAndEmail';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        validatePassword: function(subscriber, ssn, email, password, successCB, errorCB){

            const parameters = JSON.stringify({
                Subscriber: subscriber,
                SSN: ssn,
                Email: email,
                Password: password
            });

            const method = 'registerUser';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        /*
        * INIT GUEST METHODS
        */
        validateUser: function(subscriber, successCB, errorCB) {

            const parameters = JSON.stringify({
                subscriber: subscriber,
            });

            const method = 'validateUser';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        loginGuest: function(subscriber, successCB, errorCB){

            const parameters = JSON.stringify({
                subscriber: subscriber,
                device: app.device,
                deviceToken: app.uuid,
            });

            const method = 'authenticateAsGuest';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        registerGuest: function(subscriber, successCB, errorCB) {

            const parameters = JSON.stringify({
                subscriber: subscriber,
                device: app.device,
                deviceToken: app.uuid,
            });

            const method = 'registerGuestUser';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        resendGuestCode: function(subscriber, successCB, errorCB){

            const parameters = JSON.stringify({
                subscriber: subscriber,
            });

            const method = 'updateUserGetCode';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        updateGuest: function(code, subscriber, successCB, errorCB) {

            const parameters = JSON.stringify({
                subscriber: subscriber,
                device: app.device,
                code: code,
                deviceToken: app.uuid,
            });

            const method = 'updateUserSetToken';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },
        /*
        * END GUEST METHODS
        */

        getSecurityQuestions: function(subscriber, successCB, errorCB){

            const parameters = JSON.stringify({
                Subscriber: subscriber
            });

            const method = 'getChallengeQuestions';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        answerSecurityQuestions: function(subscriber, questions, successCB, errorCB){

            const parameters = JSON.stringify({
                Subscriber: subscriber,
                ResponseList: questions
            });

            const method = 'getPasswordRecovery';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },

        recoveryPasswordBySubscriber: function(subscriber, sms, email, successCB, errorCB){

            const parameters = JSON.stringify({
                Subscriber: subscriber,
                sms: sms,
                email: email
            });

            const method = 'getPasswordRecoveryBySubscriber';

            app.utils.network.requestLogin(method, parameters, successCB, errorCB);
        },
    });
});