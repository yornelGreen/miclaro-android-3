$(function() {

	// Application Model
	// ----------

	app.models.User = Backbone.Model.extend({

		initialize: function() {
			//...
	    },

        getUserProfile: function (token, accountNumber, successCB, errorCB) {

            if (!token) {
                return;
            }

            var method = 'GetPersonalData';
            var parameters = '{token:"' + token + '", BAN:"'+accountNumber+'"}';

            // requesting information info
            app.utils.network.request(method, parameters, successCB, errorCB);

        },

	    questionList : function(successCB, errorCB){

    		var method = 'RegisterQuestionList';
    		var parameters = '{}';

    		// requesting information info
    		app.utils.network.request(method, parameters, successCB, errorCB );

	    },

	    alertList : function(successCB, errorCB){

    		var method = 'RegisterAlerts';
    		var parameters = '{}';

    		// requesting information info
    		app.utils.network.request(method, parameters, successCB, errorCB );

	    },

	    validateUserName : function(userName, successCB, errorCB){

    		var method = 'RegisterIsUsernameAvailable';
    		var parameters = '{username:"' + userName + '"}';

    		// requesting information info
    		app.utils.network.request(method, parameters, successCB, errorCB );

	    },

	    validateEmail : function(email, successCB, errorCB){

    		var method = 'RegisterValidateEmail';
    		var parameters = '{email:"' + email + '"}';

    		// requesting information info
    		app.utils.network.request(method, parameters, successCB, errorCB );

	    },

	    registerUser : function(dataUser, successCB, errorCB){

	    	var method = 'Register';
    		var parameters = '{' +
    							'username:"' + dataUser.username + '",' +
    							'password:"' + dataUser.password + '",' +
    	                        'account:"' + dataUser.account + '",' +
    	                        'subscriber:"' + dataUser.subscriber + '",' +
    	                        'email:"' + dataUser.email + '",' +
    	                        'questionid:"' + dataUser.questionid + '",' +
    	                        'answer:"' + dataUser.answer + '",' +
    	                        'PINCSR:"' + dataUser.PINCSR + '",' +
    	                        'CSRQuestionid:"' + dataUser.CSRQuestionid + '",' +
    	                        'CSRAnswer:"' + dataUser.CSRAnswer + '",' +
    	                        'ContactPhone:"' + dataUser.ContactPhone + '",' +
    	                        'AlertList:"' + dataUser.AlertList + '"' +
    						'}';

    		// requesting information info
    		app.utils.network.request(method, parameters, successCB, errorCB );
	    },

	    validateUserEmail : function(userEmail, successCB, errorCB){

    		var method = 'PassRecoveryCheckEmail';
    		var parameters = '{email:"' + userEmail + '"}';

    		app.utils.network.request(method, parameters, successCB, errorCB );

	    },

	    validateAnswer: function(recoveryPasswordData, answer, successCB, errorCB){

    		var method = 'PassRecoveryCheckChallengeQuestion';
    		var parameters = '{' +
								'userid: "' + recoveryPasswordData.UserID + '",' +
								'questionid: "' + recoveryPasswordData.QuestionID + '",' +
					            'answer: "' + answer + '"' +
				            '}';

    		app.utils.network.request(method, parameters, successCB, errorCB );

	    },

	    registerUserDevice: function(userName, subscriber, successCB, errorCB){

	        var method = 'device/register';

	        var parameters = '{' +
	                            '"userName":' + '"' + userName + '",' +
	                            '"suscriber":' + '"' + subscriber + '",' +
	                            '"type":' + '"' + app.os + '",' +
	                            '"pnToken":' + '"' + app.registrationId + '",' +
	                            '"uuid":' + '"' + app.uuid + '"' +
	                        '}';

	        app.utils.network.apiRequest(method, parameters, successCB, errorCB, false);

	    },

        getNotificationsState: function(userName, successCB, errorCB){

            var method = 'device/getEnabled';

            var parameters = '{' +
                                '"userName":' + '"' + userName + '",' +
                                '"type":' + '"' + app.os + '",' +
                                '"pnToken":' + '"' + app.registrationId + '",' +
                                '"accountNumber":' + '"' + app.utils.Storage.getSessionItem('selected-account-value') + '",' +
                                '"uuid":' + '"' + app.uuid + '"' +
                            '}';

            app.utils.network.apiRequest(method, parameters, successCB, errorCB);

        },

        getPersonalNotifications: function(accountNumber, successCB, errorCB){
            var method = 'GetPersonalAlerts';

            var parameters = '{' +
                                '"token":' + '"' + app.utils.Storage.getSessionItem('token') + '",' +
                                '"BAN":' + '"' + accountNumber + '"' +
                            '}';

            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        updatePaperless: function(alertList, successCB, errorCB){
            var method = 'UpdateAlerts';

            var parameters = '{' +
                                '"token":' + '"' + app.utils.Storage.getSessionItem('token') + '",' +
                                '"alertList":' + '"' + alertList + '"' +
                            '}';
            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        getNotToCall: function(accountNumber, subscriber, successCB, errorCB){
            var method = 'GetNotToCallStatus';

            var parameters = '{' +
                                '"token":' + '"' + app.utils.Storage.getSessionItem('token') + '",' +
                                '"Subscriber":' + '"' + subscriber +  '",' +
                                '"BAN":' + '"' + accountNumber + '"' +
                            '}';

            console.log(parameters);

            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        updateNotToCall: function(accountNumber, subscriber, action, successCB, errorCB){
            var method = 'NotToCall';

            var parameters = '{' +
                                '"token":' + '"' + app.utils.Storage.getSessionItem('token') + '",' +
                                '"BAN":' + '"' + accountNumber + '",' +
                                '"Subscriber":' + '"' + subscriber +  '",' +
                                '"Action":' + '"' + action +  '"' +
                            '}';

            console.log(parameters);

            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        getAlerts: function(successCB, errorCB){

            var method = 'user/getAlerts',
                type = 'GET',
                authenticated = true;

            var parameters = {};

            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },

        notificationsAlert: function(subscriber, accountNumber, alertId, alertStatus, successCB, errorCB){

            var method = 'user/alerts',
                type = 'PUT',
                authenticated = true;

            var parameters = {
            	alerts: [{id:alertId, value:alertStatus}]
        	};

            app.utils.network.requestAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

        },

        updateNotification: function(deviceData, successCB, errorCB){

            var method = 'device/update',
                parameters;

            switch(deviceData.type){

            case 'INVOICE':

                parameters = '{' +
                '"userName":' + '"' + deviceData.userName + '",' +
                '"type":' + '"' + app.os + '",' +
                '"pnToken":' + '"' + app.registrationId + '",' +
                '"enabledInvoiceNotification":' + '"' + deviceData.enabledInvoiceNotification + '",' +
                '"uuid":' + '"' + app.uuid + '"' +
                '}';

                break;

            case 'PROMOTION':

                parameters = '{' +
                '"userName":' + '"' + deviceData.userName + '",' +
                '"type":' + '"' + app.os + '",' +
                '"pnToken":' + '"' + app.registrationId + '",' +
                '"enabledPromotionNotification":' + '"' + deviceData.enabledPromotionNotification + '",' +
                '"uuid":' + '"' + app.uuid + '"' +
                '}';

                break;

            case 'DATA':

                parameters = '{' +
                '"userName":' + '"' + deviceData.userName + '",' +
                '"type":' + '"' + app.os + '",' +
                '"pnToken":' + '"' + app.registrationId + '",' +
                '"enabledDataNotification":' + '"' + deviceData.enabledDataNotification + '",' +
                '"suscriber":' + '"' + deviceData.suscriber + '",' +
                '"uuid":' + '"' + app.uuid + '"' +
                '}';

                break;
            }

            app.utils.network.apiRequest(method, parameters, successCB, errorCB);

        },

        updateBasicRegister: function (token, data, successCB, errorCB) {

            var method = 'UpdateBasicRegister';
            var parameters = '{' +
            	'token:"' + token + '",' +
                'username:"' + data.userName + '",' +
                'question_id:"' + data.question1 + '",' +
                'answer:"' + data.answer1 + '",' +
                'pin_question_id:"' + data.question2 + '",' +
                'pin_question_answer:"' + data.answer2 + '",' +
                'newscrpin:"' + data.pin + '"' +
            '}';

            // requesting information info
            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        updatePin: function (token, data, successCB, errorCB) {

            var method = 'UpdatePin';
            var parameters = '{' +
            	'token:"' + token + '",' +
                'pin_question_id:"' + data.question2 + '",' +
                'pin_question_answer:"' + data.answer2 + '",' +
                'newcsrpin:"' + data.pin + '"' +
            '}';

            // requesting information info
            app.utils.network.request(method, parameters, successCB, errorCB);

        },

        updatePersonalDir: function (token, data, successCB, errorCB) {

            var method = 'UpdatePersonalDir';
            var parameters = '{' +
            	'token:"' + token + '",' +
                'BAN:"' + data.account + '",' +
                'AddressDet:"' + data.address1 + '",' +
                'AddressDet2:"' + data.address2 + '",' +
                'City:"' + data.city + '",' +
                'zip:"' + data.zip + '"' +
            '}';

            // requesting information info
            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        updatePersonalData: function (token, data, successCB, errorCB) {

            var method = 'UpdatePersonalData';
            var parameters = '{' +
            	'token:"' + token + '",' +
                'BAN:"' + data.account + '",' +
                'Email:"' + data.email + '",' +
                'ContactTelephoneNumber:"' + data.telephone1 + '",' +
                'HomeTelephoneNumber:"' + data.telephone2 + '"' +
            '}';

            // requesting information info
            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        updatePassword: function (token, data, successCB, errorCB) {

            var method = 'changePassword';
            var parameters = '{' +
            	'token:"' + token + '",' +
                'questionid:"' + data.question1 + '",' +
                'answer:"' + data.answer1 + '",' +
                'newPassword:"' + data.newPassword + '",' +
                'password:"' + data.oldPassword + '"' +
            '}';

            // requesting information info
            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        resetPassword: function (data, successCB, errorCB) {

            var method = 'user/password',
                type = 'PUT',
                authenticated = true;

            var parameters = {
                oldPassword: data.oldPassword,
                newPassword: data.newPassword,
                confirmationPassword: data.confirmationPassword
            };

            // requesting information info
            app.utils.network.requestAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        },

        resetUserPassword: function (data, successCB, errorCB) {

            var method = 'user/updateUserNameAndPassword',
                type = 'PUT',
                authenticated = true;

            var parameters = {
                newEmail: data.newEmail,
                oldPassword: data.oldPassword,
                newPassword: data.newPassword,
                confirmationPassword: data.confirmationPassword,
                channel: data.channel,
                accountNumber: data.accountNumber
            };

            // requesting information info
            app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        },

	    updatePerpetualLogin: function(value, successCB, errorCB){

	        var method = 'user/perpetual-login',
            	type = 'PUT',
            	authenticated = true;

			var parameters = {
            	value: value
            };

            //requesting Account
            app.utils.network.requestAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

	    }

	});

});
