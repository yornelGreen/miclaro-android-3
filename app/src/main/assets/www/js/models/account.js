$(function() {

	// Application Model
	// ----------
	
	app.models.Account = Backbone.Model.extend({				
		
		initialize: function() {							
			
	    },   
	    
	    /**********************************NEW METHODS****************************/
        getAccountsList: function (successCB, errorCB) {
            var method = 'user/accounts',
            	type = 'GET',
            	authenticated = true,
            	parameters = {};
           
            //requesting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
        
        setDefault: function (accountNumber, subscriberNumber, successCB, errorCB) {
            var method = 'user/account/default/' + accountNumber + '/' + subscriberNumber,
            	type = 'PUT',
            	authenticated = true,
            	parameters = {};

            //setting Default Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },

        deleteAccount: function (accountNumber, successCB, errorCB) {
            var method = 'user/account/' + accountNumber,
            	type = 'DELETE',
            	authenticated = true,
            	parameters = {};

            //deleting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
        
        addAccount: function (accountNumber, ssn, defaultAccount, successCB, errorCB) {
        	var method = 'user/account',
            	type = 'POST',
            	authenticated = true,
            	parameters = '{' + '"accountNumber": "' + accountNumber + '", "ssn": "'  + ssn + '", "defaultAccount":' + defaultAccount + '}';
            	
        		console.log(parameters);
        	
            //http://184.106.10.165:9081/api-miclaro-services-dev/miclaro/user/account

            //deleting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
        /*****************************END NEW METHODS*****************************/	    
	    
	    getAccounts: function(token, successCB, errorCB){
	    	
        	if(!token){
	    		return;
	    	}

    		method = 'Accounts';
    		parameters = '{token:"' + token+ '"}';
    		
    		// requesting Accounts
    		app.utils.network.request(method, parameters, successCB, errorCB);
     	    		
	    }, 
	    
//	    getAccountsList : function(token, successCB, errorCB){
//	    	
//	    	if(!token){
//	    		return;
//	    	}
//
//    		method = 'AccountsList';
//    		parameters = '{token:"' + token+ '"}';    		
//
//			// requesting account info
//			app.utils.network.request(method, parameters, successCB, errorCB);
//        	
//        },	          
	    
	    getAccountInfo : function(token, account, successCB, errorCB){	   
	    	
	    	if(!token || !account){
	    		return;
	    	}

    		method = 'MyAccount';
    		parameters = "{token:'" + token+ "', account: '" + account + "' }";    		

			// requesting account info
			app.utils.network.request(method, parameters, successCB, errorCB );
        	
        },
        
        getAccountUsage : function(token, account, subscriber, successCB, errorCB){
        	
        	if(!token || !account){
	    		return;
	    	}

    		method = 'Usage';
    		parameters = "{token:'" + token+ "',account:'" + account + "',subscriber:'" + subscriber + "'}";
    		
    		// requesting account info
    		app.utils.network.request(method, parameters, successCB, errorCB);
        	
        },
        
	    getAccountBill : function(token, account, successCB, errorCB){
	    	
	    	if(!token || !account){
	    		return;
	    	}

    		method = 'Bill';
    		parameters = "{token:'" + token+ "', account: '" + account + "'}";    		

			// requesting account info
			app.utils.network.request(method, parameters, successCB, errorCB );
        	
        },  

        getAccountSubscribers : function(token, account, successCB, errorCB){
        	
        	if(!token || !account){
	    		return;
	    	}

            method = 'MySubscribers';
            parameters = "{token:'" + token+ "', account: '" + account + "'}";
            
            // requesting account info
            app.utils.network.request(method, parameters, successCB, errorCB);
            
        },          
        
        doPayment : function(token, account, amount, successCB, errorCB){
        	
        	if(!token || !account){
	    		return;
	    	}
        	
    		method = 'DoPayment';
    		parameters = "{token:'" + token+ "', account: '"+account+"', amount: '" + amount + "'}";
    		
    		// requesting account info
    		app.utils.network.request(method, parameters, successCB, errorCB);
        	
        },    

        getPaymentInfo : function(token, paymentId, successCB, errorCB){
        	
        	if(!token || !paymentId){
	    		return;
	    	}
            
            method = 'PaymentInfo';
            parameters = "{token:'" + token+ "', PaymentId: '" + paymentId + "'}";
            
            // requesting account info
            app.utils.network.request(method, parameters, successCB, errorCB);
                
        },        
        
        validateSuscriber : function(subscriber, successCB, errorCB){
        	
        	if(!subscriber){
	    		return;
	    	}
        	
        	method = 'RegisterValidateSubscriber';
            parameters = "{subscriber: '" + subscriber + "'}";
            
            // requesting account info
            app.utils.network.request(method, parameters, successCB, errorCB);
        },

        getAccountDefaultSubscriber: function (account, successCB, errorCB) {
            var method = 'user/account/'+account+'/default-subscriber',
            	type = 'GET',
            	authenticated = true,
            	parameters = {};

            //requesting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },

         sendFailure: function (data, successCB, errorCB) {
             var method = 'fixed-fault-report/reportFailure',
                type = 'POST',
                authenticated = true,
                parameters = {
                             accountNumber: data.accountNumber,
                             accountType: data.accountType,
                             accountSubType: data.accountSubType,
                             subscriber: data.subscriber,
                             token: data.token,
                             failureInfo: {
                                     type: data.failureType,
                                     detail: data.failureDetail
                             }
                         };

             //requesting Account
             app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
         },

         getFailure: function (data, successCB, errorCB) {
             var method = 'fixed-fault-report/getFailure',
                type = 'POST',
                authenticated = true,
                parameters = {
                             subscriber: data.subscriber
                             };

             //requesting Account
             app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
         },

         getDirectDebitInfo: function (data, successCB, errorCB) {
             var method = 'direct-debit/getDirectDebitInfo',
                type = 'POST',
                authenticated = true,
                parameters = {
                             accountNumber: data.accountNumber
                             };

             //requesting Account
             app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
         },

         updateDirectDebit: function (data, successCB, errorCB) {
             var method = 'direct-debit/updateDirectDebit',
                type = 'POST',
                authenticated = true,
                parameters = {
                             type: data.type,
                             creditCardNo: data.creditCardNo,
                             creditCardMemberName: data.creditCardMemberName,
                             creditCardType: data.creditCardType,
                             creditCardExpDate: data.creditCardExpDate,
                             startDate: data.startDate,
                             bankAccountNo: data.bankAccountNo,
                             endDate: data.endDate,
                             bankCode: data.bankCode,
                             accountNumber: data.accountNumber
                             };

             //requesting Account
             app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
         }

	        
	});
	
});