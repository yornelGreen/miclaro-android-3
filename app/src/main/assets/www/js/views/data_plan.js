$(function() {

	// Data Plan View
	// ---------------

	app.views.DataPlanView = app.views.CommonView.extend({

		name:	'data_plan',

		selectedOffer: {
			'offerId': 		'',
			'displayName': 	'',
			'price':		''
		},

		// The DOM events specific.
		events: {
            'pagecreate':                           'pageCreate',

            // new content
            'change #select-account':               'changeAccount',
            'click .select-subscriber':             'changeSubscriber',
            'click .btn-buy':                       'buyPlan',
            'click .link-terms':                    'showTerms',
            'click #close-terms':                   'closeTerms',

            // toggle
		    'click .sectbar':                       'toggleClass',
            'click .phonebar':                      'toggleClass',

			// header
			'click .payment-step-1':                'goToPaymentStep1',
			'click .available-offer':               'selectPlan',
		},

		// Render the template elements
		render: function(callback) {

            if (app.utils.Storage.getSessionItem('token') == null) {
                document.location.href = 'index.html';
                return;
            }

            var subscribers = app.utils.Storage.getSessionItem('subscribers-info'),
                selectedAccount = app.utils.Storage.getSessionItem('selected-account');

            var self = this,
                variables = {
                    subscribers: subscribers,
                    selectedOfferId: app.utils.Storage.getSessionItem('selected-offer-id'),
                    availableOffers: app.utils.Storage.getSessionItem('select-offer-to-subscriber'),
                    wirelessAccount: (selectedAccount.prodCategory == 'WLS'),
                    typeOfTelephony: app.utils.tools.typeOfTelephony,
                    formatNumber: app.utils.tools.formatSubscriber,
                    selectedTab: app.utils.Storage.getSessionItem('selected-tab'),
                    accounts: this.getSelectTabAccounts(),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    accountSections: this.getUserAccess(),
                    showBackBth: true
                };

            app.TemplateManager.get(self.name, function(code){
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
		},

        pageCreate: function(e) {
            var self = this;
            self.activateMenu(e);

            var subscribers = app.utils.Storage.getSessionItem('subscribers-info');
            if (subscribers.length == 1) {
                $('.select-subscriber').eq(0).trigger('click');
            }
        },

        changeAccount: function(e){

            var self = this,
                analytics = null;

            app.utils.Storage.setSessionItem('selected-offer', null);
            app.utils.Storage.setSessionItem('selected-offer-id', 0);

            this.selectedOffer = {
                'offerId': 		'',
                'displayName': 	'',
                'price':		''
            };

            const newAccountNumber = $.mobile.activePage.find('#select-account').val();
            const accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            const accountList = app.utils.Storage.getSessionItem('accounts-list');

            var selectAccount = null;
            $.each(accountList, function (i, object) {
                if (object.Account == newAccountNumber) {
                    selectAccount = object;
                }
            });

            self.getAccountDetails(selectAccount,
                function (response) {
                    if(analytics != null ){
                        // send GA statistics
                        analytics.trackEvent('select', 'change', 'select account number data_plan', accountNumber);
                    }
                    self.render(function(){
                        $.mobile.activePage.trigger('pagecreate');
                    });
                },
                app.utils.network.errorRequest
            );
        },

        changeSubscriber: function(e) {
            var self = this;

            var currentIndex = $(e.currentTarget).data('index'),
                subscribers = app.utils.Storage.getSessionItem('subscribers-info'),
                selectedAccount = app.utils.Storage.getSessionItem('selected-account'),
                activeSubscriber = subscribers[currentIndex],
                offersToSubscriber = activeSubscriber.additionalpackagesField;

            app.utils.Storage.setSessionItem('select-offer-to-subscriber', offersToSubscriber);

            var htmlID = '#subscriber'+currentIndex;

            $(e.currentTarget).toggleClass('mon');
            if ($(e.currentTarget).data('search-info') == true) {
                $(e.currentTarget).data('search-info', false);
            } else {
                $(e.currentTarget).data('search-info', true);

                $(htmlID).find('.ciclo-fact').html(selectedAccount.CycleDate);
                var usageActiveSubscriber = activeSubscriber.usageInfoField;

                // START BASE PLAN
                if (usageActiveSubscriber.dataOffersField != null && usageActiveSubscriber.dataOffersField.length > 0) {

                    var mainPlan = usageActiveSubscriber.dataOffersField[0];
                    // update the subscriber
                    app.utils.Storage.setSessionItem('selected-subscriber-value', activeSubscriber.subscriberNumberField);
                    app.utils.Storage.setSessionItem('selected-subscriber', activeSubscriber);

                    var usagePercentage = Math.round(100.0 * (mainPlan.usedField / mainPlan.quotaField));
                    if (usagePercentage == 0 && mainPlan.usedField > 0) {
                        usagePercentage = 1;
                    }

                    var htmlGraphic = '<div class="c100 p' + usagePercentage + ' text-center center vcenter" style="height: 55vw; width: 55vw; font-size: 55vw">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="slice" style="font-size: 55vw">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="bar"></div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="fill"></div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t</div>';

                    $(htmlID).find('.graphic-chart').html(htmlGraphic);

                    $(htmlID).find('.data-plan-name').html(mainPlan.displayNameField);

                    var htmlUsage = '<span class="f-red">'+mainPlan.usedTextField+'</span> de '+mainPlan.quotaTextField;
                    $(htmlID).find('.data-plan-usage').html(htmlUsage);

                    var remain = mainPlan.quotaField - mainPlan.usedField;

                    $(htmlID).find('.consumido-label').html('consumidos (' + mainPlan.usedTextField + ')');

                    $(htmlID).find('.disponible-label').html('disponibles (' + app.utils.tools.transformAvailable(remain) + ')');

                } else {
                    $(htmlID).find('.plan-basic-full').hide();
                    $(htmlID).find('.plan-basic-empty').show();
                }
                // END BASE PLAN

                self.getOffers(htmlID, e);
            }
        },

        getOffers: function(htmlID, e) {
            var self = this;

            var currentIndex = $(e.currentTarget).data('index'),
                subscribers = app.utils.Storage.getSessionItem('subscribers-info'),
                activeSubscriber = subscribers[currentIndex];


            app.utils.Storage.setSessionItem('selected-offer', null);
            app.utils.Storage.setSessionItem('selected-offer-id', 0);

            this.selectedOffer = {
                'offerId': 		'',
                'displayName': 	'',
                'price':		''
            };

            // New code
            var availableOffers = app.utils.Storage.getSessionItem('select-offer-to-subscriber'),
                localPackages = availableOffers.localPackagesField,
                roamingPackages = availableOffers.roamingPackagesField,
                html = '',
                htmlIndicators = '';

            if (localPackages.length > 0 || roamingPackages.length > 0) {

                var allPackages = localPackages.concat(roamingPackages);

                $.each(allPackages, function (index, package) {

                    htmlIndicators += '<li data-target="#carousel'+currentIndex+'" data-slide-to="'+index+'" '+(index == 0 ? 'class="active"' : '') +'></li>\n';

                    html += '<div class="item'+(index == 0 ? ' active' : '') +'">\n' +
                        '\t\t\t\t\t\t\t\t\t\t<div class="plansondisps">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t<div class="row">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="plandisptitle vcenter">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="tabcell">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'+package.priceField+'\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="plandispcont">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow roboto-b text-center">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'+ package.displayNameField +'\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow m-top-ii">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="logline full"></div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow roboto-r text-center m-top-ii">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAl Mes<br/>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="f-big roboto-b">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'+package.priceField+'\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow m-top-ii">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="logline full"></div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow text-center m-top">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="servs-plans">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="basicrow">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="btns red vcenter rippleR btn-buy" data-offer-id="'+ package.offerIdField +'" data-offer-name="'+ package.displayNameField +'" data-offer-price="'+ package.dPriceField +'">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="tabcell">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tComprar&nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t\t\t\t\t</div>\n';
                });
                // put html
                $(htmlID).find('.carousel-indicators').html(htmlIndicators);
                $(htmlID).find('.carousel-inner').html(html);
                $(htmlID).find('.not-additional-packs').hide();
            } else {
                $(htmlID).find('.carousel-container').hide();
                $(htmlID).find('.not-additional-packs').show();
            }


        },

        buyPlan: function(e) {

            // select current
            var offerId = $(e.currentTarget).data('offerId');
            var offerName = $(e.currentTarget).data('offerName');
            var offerPrice = $(e.currentTarget).data('offerPrice');
            var subscriberSelected = app.utils.Storage.getSessionItem('selected-subscriber-value');

            this.selectedOffer.offerId 		= offerId;
            this.selectedOffer.displayName 	= offerName;
            this.selectedOffer.price 		= offerPrice;

            this.goToPaymentStep1();
        },

		goToPaymentStep1: function(e){

            var self = this,
                subscriberHasAvailableCredit,
                creditLimitData = {},
                offerData = {},
                usageOfferDataList,
                adicionalPackageData = false,
                adicionalPackageRoaming = false,
                selectedOfferMessage = 'Estimado cliente su cuenta ya tiene un paquete adicional en espera de uso.',
                selectedOffer = this.selectedOffer,
                selectedOfferId = this.selectedOffer.offerId,
                price = this.selectedOffer.price;

            if(selectedOfferId == ''){
                showAlert('Error', 'Debe seleccionar algun plan', 'Aceptar');
                return;

            }else{
                self.showPasswordPrompt(function() {
                    // add parameters here
                    app.utils.Storage.setSessionItem('selected-offer', selectedOffer);
                    app.utils.Storage.setSessionItem('selected-offer-id', selectedOfferId);

                    // create object to Credit Limit Data
                    creditLimitData.ban = app.utils.Storage.getSessionItem('selected-account-value');
                    creditLimitData.productPrice = parseFloat(price.replace('$','')).toFixed(2);
                    creditLimitData.accountType = app.utils.Storage.getSessionItem('selected-account').mAccountType;

                    usageOfferDataList = app.utils.Storage.getSessionItem('selected-subscriber').UsageOfferDataList;

                    // validateCreditLimit
                    self.options.offerModel.validateCreditLimit(

                        creditLimitData,

                        function(validateResponse){

                            var availableCredit = parseFloat(validateResponse.AvailableCredit.replace('$','')).toFixed(2);

                            if(!validateResponse.HasError){

                                subscriberHasAvailableCredit = parseFloat(availableCredit)>=parseFloat(creditLimitData.productPrice);

                                if(subscriberHasAvailableCredit){

                                    var invoiceMessage = 'Estimado Cliente: El paquete seleccionado será agregado con cargo a su próxima factura. ¿Está seguro de agregar el plan: '+app.utils.Storage.getSessionItem('selected-offer').displayName+' ?';

                                    showConfirm('Recargo a factura', invoiceMessage, ['Aceptar', 'Cancelar'],

                                        function(result){

                                            if(result == 1) {

                                                offerData = {
                                                        'subscriberId'          : app.utils.Storage.getSessionItem('selected-subscriber-value'),
                                                        'offerId'               : app.utils.Storage.getSessionItem('selected-offer-id'),
                                                        'charge'                : '1',
                                                        'cicle'                 : app.utils.Storage.getSessionItem('selected-account').BillCycle,
                                                    };


                                                //add offer (plan) to subscriber on INVOICE
                                                self.options.offerModel.addOfferToSubscriber(

                                                    offerData,

                                                    //success callback
                                                    function(responseOffer){

                                                        if(responseOffer.ResultCode=="0"){

                                                            app.utils.Storage.setSessionItem('invoice-charge', true);

                                                            // render payment step 3
                                                            app.router.navigate('payment_step_3', {trigger: true});

                                                        }else{

                                                            showAlert('Error', responseOffer.Description, 'Aceptar');

                                                        }

                                                    },

                                                    // error callback
                                                    app.utils.network.errorFunction
                                                );

                                            }

                                        });

                                }else{

                                    var confirmMessage = 'Estimado cliente el plan de data adicional seleccionado no califica para cargo en factura, por favor seleccione un plan de menor costo o continúe su compra efectuando el pago inmediato usando sus tarjeta de crédito.';

                                    showConfirm('Pago con tarjeta de crédito', confirmMessage, ['Aceptar', 'Cancelar'],

                                        function(result){
                                            // redirect to credit card view
                                            if(result == 1) {
                                                app.router.navigate('payment_step_1',{trigger: true});
                                            }
                                        });

                                }


                            }else{

                                showAlert('Error', validateResponse.ErrorDesc, 'Aceptar');

                            }

                        },

                         // error callback
                        app.utils.network.errorFunction

                    ); // end validateCreditLimit

                });

            }
        },

        selectPlan: function(e){

            // clean others
            $('.available-offer-check').each(function(){
                $(this).prop('checked', false);
            });

            $('.available-offer').removeClass('on');

            // select current
            $(e.currentTarget).addClass('on');
            var offerId = $(e.currentTarget).data('offerId');
            var subscriberSelected = app.utils.Storage.getSessionItem('selected-subscriber-value');
            $('#check-'+offerId+'-'+subscriberSelected).prop('checked', true);

            this.selectedOffer.offerId 		= offerId;

            this.selectedOffer.displayName 	= $.trim($('#offer-description-' + offerId).html());
            this.selectedOffer.price 		= $.trim($('#offer-price-' + offerId).html());

        },

        showTerms: function(e) {
            $('.popupbg').show();
        },

        closeTerms: function(e) {
            $('.popupbg').hide();
        },

	});
});
