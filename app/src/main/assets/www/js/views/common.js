$(function() {

	// Common View
	// ---------------
	
	app.views.CommonView = Backbone.View.extend({
		
		name:'menu',
		history: true,
		
		// Models declaration
        loginModel: null,
        customerModel: null,
        referrerModel: null,
		accountModel: null,
        userModel: null,

        // Initialize the view
		initialize: function(options){
			this.options = options;
		},

        activateMenu: function(e) {
            const self = this;

            // return
            $('#btn-back').click(() => { self.back(e); });
            // go home
            $('#btn-menu').click(() => { self.navigateHome(); });
            // open-close nav menu
            $('#nav-open').click(() => { self.openNav(); });
            $('#nav-close').click(() => { self.closeNav(); });

            // sub menu
            $('#section-head0').click(() => {$(this).toggleClass('open-ins');});
            $('#section-head1').click(() => {$(this).toggleClass('open-ins');});
            $('#section-head2').click(() => {$(this).toggleClass('open-ins');});
            $('#section-head3').click(() => {$(this).toggleClass('open-ins');});
            $('#section-head4').click(() => {$(this).toggleClass('open-ins');});
            $('#section-head5').click(() => {$(this).toggleClass('open-ins');});
            $('#section-head6').click(() => {$(this).toggleClass('open-ins');});

            // Invoices & Payments
            $('#nav-access9').click(() => { self.navigateInvoiceSummary(); });
            $('#nav-access10').click(() => { self.navigateInvoiceDownload(); });
            $('#nav-access12').click(() => { self.paymentHistory(); });
            $('#nav-access13').click(() => { self.electronicBill(); });
            // My consumptions
            $('#nav-access14').click(() => { self.navigateConsumptionData(); });
            $('#nav-access15').click(() => { self.navigateConsumptionCalls(); });
            // My services
            $('#nav-access16').click(() => { self.myServices(); });
            $('#nav-access17').click(() => { self.myChangePlan(); });
            $('#nav-access18').click(() => { self.netflix(); });
            $('#nav-access19').click(() => { self.referSystem(); });
            // Purchases
            $('#nav-access20').click(() => { self.navigatePurchases(); });
            $('#nav-access21').click(() => { self.navigatePurchaseData(); });
            $('#nav-access23').click(() => { self.myOrders(); });
            // interruption
            $('#nav-accessXX').click(() => { self.navigateInterruption(); }); // TODO, still without know access
            // My account
            $('#nav-access1').click(() => { self.myNotifications(); });
            $('#nav-access2').click(() => { self.accountsManage(); });
            $('#nav-access7').click(() => { self.support(); });
            $('#nav-access8').click(() => { self.logout(); });

            // footer
            $('#btn-help').click(() => { self.helpSection(); });

            // click tabs
            $('#tab-postpago').click(() => { self.selectPostpago(); });
            $('#tab-prepago').click(() => { self.selectPrepago(); });
            $('#tab-telephony').click(() => { self.selectTelephony(); });
            // notifications
            $('#notifications').click(() => { self.notifications(); });
        },

        clickSubMenu1: function() {
            $('#postpaid1-head').toggleClass('open-ins');
        },

        clickSubMenu2: function() {
            $('#postpaid2-head').toggleClass('open-ins');
        },

        clickSubMenu3: function() {
            $('#postpaid3-head').toggleClass('open-ins');
        },

        clickSubMenu4: function() {
            $('#postpaid4-head').toggleClass('open-ins');
        },

        clickSubMenu5: function() {
            $('#postpaid5-head').toggleClass('open-ins');
        },

        openNav: function() {
            document.getElementById("mySidenav").style.right = "0";
            $('.mnu-icon').addClass('m-open');
            app.isMenuOpen = true;
        },

        closeNav: function () {
			if (app.isMenuOpen === true) {
                document.getElementById("mySidenav").style.right = "100%";
                $('.mnu-icon').removeClass('m-open');
                app.isMenuOpen = false;
			}
        },

		back: function(e){
			
			var analytics = null;
			
			if(analytics !=null ){
				// send google statistics
				analytics.trackEvent('button', 'click', 'back');
			}
			
			e.preventDefault();
			app.router.back = true;

			app.router.backPage();
		},

		menu: function(e){
			app.router.history	= ['menu'];
			app.router.navigate('menu',{trigger: true});
			return false;			
		},		
		//
		// logout: function(){
		//
		// 	var self = this;
		//
        //     var totalSessionTime = Math.abs(new Date(app.utils.Storage.getSessionItem('init-session'))-new Date()),
        //         analytics = null;
		//
        //     // close side menu
		// 	if(app.isMenuOpen == true){
        //         self.closeNav();
		// 	}
		//
        //     if(analytics !=null ){
        //         // send google statistics
        //         analytics.trackEvent('button', 'click', 'logout');
        //     }
		//
        //     showConfirm(
        //         'Salir',
        //         '¿Esta seguro que desea cerrar la sesión?',
        //         ['Si', 'No'],
        //         function(btnIndex){
        //             if (btnIndex == 1){
        //
        //                 self.removeSession();
		//
        //             	var userModel = new app.models.User();
		//
        //                 // un register perpetual login
        //                 userModel.updatePerpetualLogin(
        //                     // disable perpetual login
        //                     false,
        //                     // success
        //                     function(success) {
        //                         console.log('#success update perpetual value');
        //                         app.router.navigate('login_guest',{trigger: true});
        //                     },
        //                     // error
        //                     function(error) {
        //                         console.log('#error update perpetual value');
        //                         app.router.navigate('login_guest',{trigger: true});
        //                     }
        //                 );
        //             }
        //         }
        //     );
		//
        //     return false;
        // },

        logout: function(){
            var self = this;

            var analytics = null;

            // close side menu
            if(app.isMenuOpen == true){
                self.closeNav();
            }

            if(analytics != null){
                // send google statistics
                analytics.trackEvent('button', 'click', 'logout');
            }

            console.log('in on logout method');

            showConfirm(
                'Salir',
                '¿Esta seguro que desea cerrar la sesión?',
                ['Si', 'No'],
                function(btnIndex){
                    if(btnIndex==1){
                        app.removeSession();
                        app.router.navigate('login_guest',{trigger: true});
                    }

                }
            );
        },

        clickMenu: function(e){
            var accessAccTypeProducTypeID = $(e.currentTarget).data('id');
			if (accessAccTypeProducTypeID === 1) {
				this.account(e);
			} else if (accessAccTypeProducTypeID === 2) {
                this.consumption(e);
            } else if (accessAccTypeProducTypeID === 27) {
                this.chat();
            } else if (accessAccTypeProducTypeID === 45) {
                this.service(e);
            } else if (accessAccTypeProducTypeID === 19) {
                this.referSystem(e);
            } else {
                showAlert('Click','Ha hecho click el item id: ' + accessAccTypeProducTypeID, 'Aceptar');
            }
        },

        navigateHome: function() {
            this.closeNav();
			app.router.navigate('menu',{trigger: true});
        },

		navigateInvoiceSummary: function() {
            this.closeNav();
            app.router.navigate('invoice', {trigger: true});
		},

        navigateInvoiceDownload: function() {
            this.closeNav();
            app.router.navigate('invoice_download', {trigger: true});
        },

        navigateConsumptionData: function() {
            this.closeNav();
            // this.getBanDetails(
            // 	function () {
            //         app.utils.Storage.setSessionItem('consumption-type-selected', 1);
            //         app.router.navigate('consumption', {trigger: true});
            //     },
            //     app.utils.network.errorRequest
			// );
            app.utils.Storage.setSessionItem('consumption-type-selected', 1);
            app.router.navigate('consumption', {trigger: true});
        },

        navigateConsumptionCalls: function() {
            this.closeNav();
            // this.getBanDetails(
            //     function () {
            //         app.utils.Storage.setSessionItem('consumption-type-selected', 2);
            //         app.router.navigate('consumption', {trigger: true});
            //     },
            //     app.utils.network.errorRequest
			// );
            app.utils.Storage.setSessionItem('consumption-type-selected', 2);
            app.router.navigate('consumption', {trigger: true});
        },

        navigatePurchases: function() {
            app.router.navigate('purchases', {trigger: true});
        },

        navigateInterruption: function() {
            app.router.navigate('fault_report_a', {trigger: true});
        },

        navigatePurchaseData: function() {
            this.closeNav();
            // this.getBanDetails(
            //     function (response) {
            //         app.router.navigate('data_plan', {trigger: true});
            //     },
            //     function (message) {
            //         showAlert('Error', message, 'Aceptar');
            //     });
            app.router.navigate('data_plan', {trigger: true});
		},

        getBanDetails: function(successFunction, errorFunction) {
            var customerModel = new app.models.Customer();
            var selectedAccountValue = app.utils.Storage.getSessionItem('selected-account-value');
            customerModel.getBan((selectedAccountValue).toString(),
                function(response) {
                    if(response.hasError){
                        errorFunction(response, 200, response.errorDisplay)
                    } else {
                        app.utils.Storage.setSessionItem('subscribers', response.Subscribers);
                        successFunction(response);
                    }
                },
                errorFunction
			);
        },

		account: function(e){

			var self = this,
			    accountNumber = app.utils.Storage.getSessionItem('selected-account-value'),
				accountModel = new app.models.Account(),
				selectedSubscriber = null,
				analytics = null;

			// showAlert('alert', accountNumber, 'aceptar');
			// return;

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}

			if(analytics !=null ){
				// send google statistics
				analytics.trackEvent('button', 'click', 'account');
			}

			if (app.utils.Storage.getSessionItem(accountNumber) == null) {
				accountModel.getAccountInfo(
					app.utils.Storage.getSessionItem('token'),
					accountNumber,

					//success callback
					function(data){

						if(!data.HasError){
							// session
							app.utils.Storage.setSessionItem('selected-account',data);

							// cache
							app.utils.Storage.setSessionItem(accountNumber, data);

							app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
							app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);

							// cache
							app.utils.Storage.setSessionItem('subscribers-'+accountNumber, data.Subscribers);

							var accountSuspend = false;

							for(var i=0 ; i<data.Subscribers.length ; i++){
								if(data.Subscribers[i].status=='S'){
									accountSuspend = true;
									break;
								}
							}

							if (!app.utils.Storage.getSessionItem('suspend-account') && accountSuspend) {
								self.showSuspendedAccount();
							}

							app.router.navigate('account',{trigger: true});

						} else{

							showAlert('Error', data.Desc, 'Aceptar');
						}
					},
					// error function
					app.utils.network.errorFunction
				);

			} else {

				var account = app.utils.Storage.getSessionItem(accountNumber);

				// cache
				app.utils.Storage.setSessionItem('selected-account', account);

				var subscribers = app.utils.Storage.getSessionItem('subscribers-'+accountNumber);

				// cache
				app.utils.Storage.setSessionItem('subscribers', subscribers);
				app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);

				// goto view
				app.router.navigate('account',{trigger: true});
			}

			return false;
		},
		
		device: function(e){
			
			var accountModel = new app.models.Account(), 
				accountNumber = app.utils.Storage.getSessionItem('selected-account-value'),
				analytics = null;
			
			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			if(analytics !=null ){
				// send google statistics
				analytics.trackEvent('button', 'click', 'device');
			}
			
			if(app.utils.Storage.getSessionItem('subscribers-'+accountNumber) == null){
			
				accountModel.getAccountSubscribers(
					
					//parameters
					app.utils.Storage.getSessionItem('token'),
					accountNumber,
	
					//success callback
					function(data){
						console.log(data);
						
						if(!data.HasError){					
							
							app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
							
							// default subscriber
							app.utils.Storage.setSessionItem('selected-subscriber',data.Subscribers[0]);
							app.utils.Storage.setSessionItem('selected-subscriber-value',data.Subscribers[0].subscriber);
							
							// cache
							app.utils.Storage.setSessionItem('subscribers-'+accountNumber, data.Subscribers);
							
							// goto view
							app.router.navigate('device',{trigger: true});
						
						}else{
								
							showAlert('Error', data.Desc, 'Aceptar');
							
						}
						
					},
					
					// error function
					app.utils.network.errorFunction	
				);		
			
			} else {
				
				//cache
				var subscribers = app.utils.Storage.getSessionItem('subscribers-'+accountNumber);
				
				app.utils.Storage.setSessionItem('subscribers', subscribers);
				
				// default subscriber
				app.utils.Storage.setSessionItem('selected-subscriber',subscribers[0]);
				app.utils.Storage.setSessionItem('selected-subscriber-value',subscribers[0].subscriber);
				
				// goto view
				app.router.navigate('device',{trigger: true});
			}
			
			return false;
		},
		
		service: function(e){
			
			var loadAccounts = app.utils.Storage.getSessionItem('load-accounts'),
				analytics = null;
			
			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			if(analytics !=null ){
				// send google statistics
				analytics.trackEvent('button', 'click', 'service');
			}
			
			if(!loadAccounts){
				app.session.menuFunction = this.service;
				app.utils.loader.show();
				return false;
			}
			
			var accountModel = new app.models.Account(),	
				accountNumber = app.utils.Storage.getSessionItem('selected-account-value');
			
			//if(typeof app.cache.Subscribers[accountNumber]=='undefined'){
			if(app.utils.Storage.getSessionItem('subscribers-'+accountNumber) == null){
			
				accountModel.getAccountSubscribers(
					//parameter
					app.utils.Storage.getSessionItem('token'),
					app.utils.Storage.getSessionItem('selected-account-value'),
	
					//success callback
					function(data){
						console.log('#success ws service');
						
						if(!data.HasError){					
							
							app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
							
							// default subscriber
							app.utils.Storage.setSessionItem('selected-subscriber-value',data.Subscribers[0].value);
							
							// cache
							//app.cache.Subscribers[accountNumber] = data.Subscribers;
							app.utils.Storage.setSessionItem('subscribers-'+accountNumber, data.Subscribers);
							
							// goto view
							app.router.navigate('service',{trigger: true});
						
						}else{
								
							showAlert('Error', data.Desc, 'Aceptar');
							
						}
						
					},
					
					// error function
					app.utils.network.errorFunction	
				);
			
			} else {
				//cache
				//var subscribers = app.cache.Subscribers[accountNumber];
				var subscribers = app.utils.Storage.getSessionItem('subscribers-'+accountNumber);
				app.utils.Storage.setSessionItem('subscribers', subscribers);
				
				// default subscriber
				app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);
				app.utils.Storage.setSessionItem('selected-subscriber-value',subscribers[0].subscriber);
				
				// goto view
				app.router.navigate('service',{trigger: true});
			}
			
			return false;	
		},

        electronicBill: function(e){
            app.router.navigate('electronic_bill', {trigger: true});
        },

        downloadInvoice: function(e){
            app.router.navigate('download_invoice', {trigger: true});
        },

        paymentHistory: function(e){
            app.router.navigate('payment_history', {trigger: true});
        },

        myConsumption: function(e){
            app.router.navigate('my_consumption', {trigger: true});
        },

        myServices: function(e){
            this.closeNav();
            // this.getBanDetails(
            //     function () {
            //         app.router.navigate('device', {trigger: true});
            //     },
            //     app.utils.network.errorRequest
            // );
            app.router.navigate('device', {trigger: true});
        },

        myChangePlan: function(e){
            app.router.navigate('change_plan', {trigger: true});
        },

        netflix: function(e){
            app.router.navigate('netflix', {trigger: true});
        },

        referSystem: function(e){
            app.router.navigate('refiere_step_1', {trigger: true});
        },

        purchases: function(e){
            app.router.navigate('purchases', {trigger: true});
        },

        dataPackages: function(e){
            app.router.navigate('data_packages', {trigger: true});
        },

        myOrders: function(e){
            app.router.navigate('my_orders', {trigger: true});
        },

        myNotifications: function(e){
            app.router.navigate('notifications_app', {trigger: true});
        },

        accountsManage: function(e){
            app.router.navigate('accounts_manage', {trigger: true});
        },

        support: function(e){
            app.router.navigate('support', {trigger: true});
        },

		
		plan: function(e){
			
			var accountModel = new app.models.Account(),
				analytics = null;
			
			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			if(analytics !=null ){
				// send google statistics
				analytics.trackEvent('button', 'click', 'plan');
			}
			
			accountModel.getAccountSubscribers(
			
				//parameter
				app.utils.Storage.getSessionItem('token'),
				app.utils.Storage.getSessionItem('selected-account-value'),

				//success callback
				function(data){
					console.log('#success ws service');
					
					if(!data.HasError){			
						
						// Default subscriber
						app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
						app.utils.Storage.setSessionItem('selected-subscriber',data.Subscribers[0]);
						
						app.utils.Storage.setSessionItem('selected-subscriber-value',data.Subscribers[0].subscriber);
						
						app.router.navigate('plan',{trigger: true});
					
					}else{
							
						showAlert('Error', data.Desc, 'Aceptar');
						
					}
					
				},
				
				// error function
				app.utils.network.errorFunction	
			);							
			
			return false;
		},
		
		profile: function(e){

			var userModel = new app.models.User(),
				accounts = app.utils.Storage.getSessionItem('accounts-list');
				analytics = null;

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}

			if(analytics !=null ){
				// send google statistics
				analytics.trackEvent('button', 'click', 'profile');
			}
			
			userModel.getUserProfile(
				//parameter
				app.utils.Storage.getSessionItem('token'),
				accounts[0].Account,

				//success callback
				function(data){

					if(!data.HasError){

						// session
						//app.session.Profiles = data.Profiles;
						app.utils.Storage.setSessionItem('profile', data);

						//goto view
						app.router.navigate('profile',{trigger: true});

					}else{

						showAlert('Error', data.Desc, 'Aceptar');

					}

				},

				// error function
				app.utils.network.errorFunction
			);

			return false;
		},
		
		consumption: function(e){
			
			var loadAccounts = app.utils.Storage.getSessionItem('load-accounts'),
				analytics = null; 

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			// send google statistics
			if(analytics !=null ){
				analytics.trackEvent('button', 'click', 'consumption');
			}
			
			if(!loadAccounts){
				app.session.menuFunction = this.consumption;
				app.utils.loader.show();
				return false;
			}
			
			var accountModel = new app.models.Account();
				app.session.Usage = {},
				accountNumber = app.utils.Storage.getSessionItem('selected-account-value');
				
			//Subscribers
			accountModel.getAccountSubscribers(
				//parameter
				app.utils.Storage.getSessionItem('token'),
				accountNumber,

				//success callback
				function(data){
					console.log('#success ws service');
					
					if(!data.HasError){				
							
						app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
						
						// default subscriber
						app.utils.Storage.setSessionItem('selected-subscriber',data.Subscribers[0]);
						app.utils.Storage.setSessionItem('selected-subscriber-value',data.Subscribers[0].subscriber);
						
						// cache
						app.utils.Storage.setSessionItem('subscribers-'+accountNumber, data.Subscribers);
						
						accountModel.getAccountUsage(
								//parameters
								app.utils.Storage.getSessionItem('token'),
								app.utils.Storage.getSessionItem('selected-account-value'),
								data.Subscribers[0].subscriber,

								//success callback
								function(data){
									console.log('#success ws account usage');					
									
									if(!data.HasError){	
										
										//app.session.Usage = data;
										app.utils.Storage.setSessionItem('usage',data);
										
										app.router.navigate('consumption',{trigger: true});
									
									}else{
											
										showAlert('Error', data.Desc, 'Aceptar');
										
									}					
									
								},
								
								// error function
								app.utils.network.errorFunction	
							);
						
						
					}else{
								
						showAlert('Error', data.Desc, 'Aceptar');
							
					}
						
				},
				
				// error function
				app.utils.network.errorFunction	
			);																				
			
			return false;
		},
		
		invoice: function(e){
			
			var accountModel = new app.models.Account(),
				analytics = null;

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			// send google statistics
			if(analytics !=null ){
				//analytics.trackEvent('button', 'click', 'invoice');
			}
			
			accountModel.getAccountBill(
				//parameters
				app.utils.Storage.getSessionItem('token'),
				app.utils.Storage.getSessionItem('selected-account-value'),

				//success callback
				function(data){									
					
					if(!data.HasError){					
						
						// account info
						app.utils.Storage.setSessionItem('accounts-bill-info',data);	
						
						// send google statistics
						if(analytics != null)
							analytics.trackEvent('button', 'click', 'invoice button', app.utils.Storage.getSessionItem('selected-account-value'));
						
						app.router.navigate('invoice',{trigger: true});
						
					}else{
							
						showAlert('Error', data.Desc, 'Aceptar');
						
					}							
											
				},
				
				// error function
				app.utils.network.errorFunction	
			);
			
			return false;
		},

		/*notifications: function() {
			
			var accounts = app.utils.Storage.getSessionItem('accounts-list'),
			 	accountModel = new app.models.Account(),
			 	userModel = new app.models.User(),
			 	username = app.utils.Storage.getSessionItem('username');

            accountModel.getAccountDefaultSubscriber(
                // parameters
                accounts[0].Account,
                // success callback
                function(dasr) {

                    if (!dasr.hasError) {

                        var subscriber = null;

                        // set default subscriber
                        if (dasr.object !== undefined) {
                            subscriber = dasr.object;
                        } else {
                            subscriber = accounts[0].Subscribers[0].subscriber;
                        }

                        userModel.getNotificationsState(
                            // parameters
                            username,
                            // success callback
                            function(gnsr){

                                if(!gnsr.hasError){
                                    app.utils.Storage.setSessionItem('notifications-state', gnsr.object);
                                    app.utils.Storage.setSessionItem('notifications-default-subscriber', subscriber);

                                    // get personal information
                                    userModel.getPersonalNotifications(
                                        // account
                                        accounts[0].Account,
                                        // success function
                                        function(resultPN) {

                                            console.log(resultPN);

                                            app.utils.Storage.setSessionItem('notifications-paperless', resultPN.QuestionList);

                                            // go to notification
                                            if (app.utils.Storage.getSessionItem('notifications-not-to-call') != null) {
                                                // go to notification
                                                app.router.navigate('manage_notifications', {trigger: true});
                                            }

                                        },
                                        // error function
                                         app.utils.network.errorFunction
                                    );

                                    // get no to call value
                                    userModel.getNotToCall(
                                        // account
                                        accounts[0].Account,
                                        // subscriber
                                        subscriber,
                                        // success function
                                        function(resultNTC) {

                                            console.log(resultNTC);

                                            app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                                            if (app.utils.Storage.getSessionItem('notifications-paperless') != null) {
                                                // go to notification
                                                app.router.navigate('manage_notifications', {trigger: true});
                                            }

                                        },
                                        // error function
                                         app.utils.network.errorFunction
                                    );


                                }

                            },
                            // error function
                            app.utils.network.errorFunction
                        );
                    }

                },
                // error callback
                app.utils.network.errorFunction
            );
		},*/


		notifications: function() {

            var self = this,
                accountNumber = app.utils.Storage.getSessionItem('selected-account-value'),
                account = app.utils.Storage.getSessionItem(accountNumber),
                accounts = app.utils.Storage.getSessionItem('accounts-list'),
                accountModel = new app.models.Account(),
                userModel = new app.models.User(),
                username = app.utils.Storage.getSessionItem('username'),
                subscriber = null;


            // close side menu
            if(app.isMenuOpen == true){
                this.closeMenu();
            }

            if (app.utils.Storage.getSessionItem(accountNumber) == null) {
                accountModel.getAccountInfo(
                    app.utils.Storage.getSessionItem('token'),
                    accountNumber,

                    //success callback
                    function(data){

                        if(!data.HasError){
                            // session
                            app.utils.Storage.setSessionItem('selected-account',data);

                            // cache
                            app.utils.Storage.setSessionItem(accountNumber, data);

                            app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
                            app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);

                            // cache
                            app.utils.Storage.setSessionItem('subscribers-'+accountNumber, data.Subscribers);

                            var accountSuspend = false;

                            for(var i=0 ; i<data.Subscribers.length ; i++){
                                if(data.Subscribers[i].status=='S'){
                                    accountSuspend = true;
                                    break;
                                }
                            }

                            if (!app.utils.Storage.getSessionItem('suspend-account') && accountSuspend) {
                                self.showSuspendedAccount();
                            }

                                subscriber = app.utils.Storage.getSessionItem('selected-subscriber');

                                userModel.getNotificationsState(
                                    // parameters
                                    username,
                                    // success callback
                                    function(gnsr){

                                        if(!gnsr.hasError){
                                            app.utils.Storage.setSessionItem('notifications-state', gnsr.object);

                                            // get Alerts
                                            userModel.getAlerts(
                                                function(resultAlerts) {
                                                    console.log(resultAlerts);

                                                    app.utils.Storage.setSessionItem('notifications-alerts', resultAlerts);
                                                    app.utils.Storage.setSessionItem('notifications-paperless', resultAlerts.object);

                                                    // go to notification
                                                    if (app.utils.Storage.getSessionItem('notifications-not-to-call') != null && app.utils.Storage.getSessionItem('notifications-paperless') != null) {
                                                        // go to notification
                                                        app.router.navigate('manage_notifications', {trigger: true});
                                                    }
                                                },
                                                // error function
                                                 app.utils.network.errorFunction
                                            );

                                            // get no to call value
                                            userModel.getNotToCall(
                                                // account
                                                accounts[0].Account,
                                                // subscriber
                                                subscriber.subscriber,
                                                // success function

                                                function(resultNTC) {
                                                    console.log(resultNTC);

                                                    app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                                                    if (app.utils.Storage.getSessionItem('notifications-paperless') != null && app.utils.Storage.getSessionItem('notifications-alerts') != null) {
                                                        // go to notification
                                                        app.router.navigate('manage_notifications', {trigger: true});
                                                    }

                                                },
                                                // error function
                                                 app.utils.network.errorFunction
                                            );

                                        }

                                    },
                                    // error function
                                    app.utils.network.errorFunction
                                );



                        } else{

                            showAlert('Error', data.Desc, 'Aceptar');
                        }
                    },
                    // error function
                    app.utils.network.errorFunction
                );

            } else {

                var account = app.utils.Storage.getSessionItem(accountNumber);

                // cache
                app.utils.Storage.setSessionItem('selected-account', account);

                var subscriber = app.utils.Storage.getSessionItem('selected-subscriber');
                var subscribers = app.utils.Storage.getSessionItem('subscribers-'+accountNumber);

                // cache
                app.utils.Storage.setSessionItem('subscribers', subscribers);
                app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);

                userModel.getNotificationsState(
                    // parameters
                    username,
                    // success callback
                    function(gnsr){

                        if(!gnsr.hasError){
                            app.utils.Storage.setSessionItem('notifications-state', gnsr.object);
                            app.utils.Storage.setSessionItem('notifications-default-subscriber', subscriber);

                            // get Alerts
                            userModel.getAlerts(
                                 function(resultAlerts) {
                                      console.log(resultAlerts);

                                      app.utils.Storage.setSessionItem('notifications-alerts', resultAlerts);
                                      app.utils.Storage.setSessionItem('notifications-paperless', resultAlerts.object);

                                      // go to notification
                                      if (app.utils.Storage.getSessionItem('notifications-not-to-call') != null && app.utils.Storage.getSessionItem('notifications-paperless') != null) {
                                                // go to notification
                                            app.router.navigate('manage_notifications', {trigger: true});
                                      }
                                 },
                                // error function
                                app.utils.network.errorFunction
                            );

                            // get no to call value
                            userModel.getNotToCall(
                                // account
                                accounts[0].Account,
                                // subscriber
                                subscriber.subscriber,
                                // success function

                                function(resultNTC) {
                                    console.log(resultNTC);

                                    app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                                    if (app.utils.Storage.getSessionItem('notifications-paperless') != null && app.utils.Storage.getSessionItem('notifications-alerts') != null) {
                                        // go to notification
                                        app.router.navigate('manage_notifications', {trigger: true});
                                    }

                                },
                                // error function
                                 app.utils.network.errorFunction
                            );

                        }

                    },
                    // error function
                    app.utils.network.errorFunction
                );

            }

            /*
            accountModel.getAccountDefaultSubscriber(
                // parameters
                accounts[0].Account,
                // success callback
                function(dasr) {

                    if (!dasr.hasError) {

                        var subscriber = null;

                        // set default subscriber
                        if (dasr.object !== undefined && dasr.object !== '') {
                            subscriber = dasr.object;
                        } else {
                            subscriber = accounts[0].Subscribers[0].subscriber;
                        }

                        userModel.getNotificationsState(
                            // parameters
                            username,
                            // success callback
                            function(gnsr){

                                if(!gnsr.hasError){
                                    app.utils.Storage.setSessionItem('notifications-state', gnsr.object);
                                    app.utils.Storage.setSessionItem('notifications-default-subscriber', subscriber);

                                    // get personal information
                                    userModel.getPersonalNotifications(
                                        // account
                                        accounts[0].Account,
                                        // success function
                                        function(resultPN) {

                                            console.log(resultPN);

                                            app.utils.Storage.setSessionItem('notifications-paperless', resultPN.QuestionList);

                                            // go to notification
                                            if (app.utils.Storage.getSessionItem('notifications-not-to-call') != null) {
                                                // go to notification
                                                app.router.navigate('manage_notifications', {trigger: true});
                                            }

                                        },
                                        // error function
                                         app.utils.network.errorFunction
                                    );

                                    // get no to call value
                                    userModel.getNotToCall(
                                        // account
                                        accounts[0].Account,
                                        // subscriber
                                        subscriber,
                                        // success function

                                        function(resultNTC) {
                                            console.log(resultNTC);

                                            app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                                            if (app.utils.Storage.getSessionItem('notifications-paperless') != null) {
                                                // go to notification
                                                app.router.navigate('manage_notifications', {trigger: true});
                                            }

                                        },
                                        // error function
                                         app.utils.network.errorFunction
                                    );


                                }

                            },
                            // error function
                            app.utils.network.errorFunction
                        );
                    }

                },
                // error callback
                app.utils.network.errorFunction
            );
            */
        },
		
		chat: function(){
			
			var browser = null,
				analytics = null;
			
			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			// send google statistics			
			if(analytics !=null ){
				analytics.trackEvent('button', 'click', 'chat button');
			}
			
            app.router.navigate('chat',{trigger: true});
			
			return false;
		},
		
		helpSection: function(e){
			
			var analytics = null; 
			
			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}
			
			// send google statistics
			if(analytics !=null ){				
				analytics.trackEvent('button', 'click', 'help section');
			}
			
			app.utils.Storage.setSessionItem('exit-help-url', this.name);
			
			//Go to help
			app.router.navigate('help_section',{trigger: true});

		},
        
		changePlan: function(element){

			var analytics = null,
				offertModel = new app.models.Offer(),
				accountModel = new app.models.Account(),
				subscriberModel = new app.models.Subscriber(),
				account = app.utils.Storage.getSessionItem('selected-account'),
				accountNumber = app.utils.Storage.getSessionItem('selected-account-value'),
				soc = null,
				technology = 'LTE',
				accountType = account.mAccountType,
				accountSubType = account.mAccountSubType,
				offerID = 0,
				subscriber = null;

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}

			// send google statistics
			if(analytics !=null ){
				analytics.trackEvent('button', 'click', 'change_plan section');
			}
			
			if(app.utils.Storage.getSessionItem('subscribers-'+accountNumber) == null){

				accountModel.getAccountSubscribers(
					//parameter
					app.utils.Storage.getSessionItem('token'),
					app.utils.Storage.getSessionItem('selected-account-value'),

					//success callback
					function(data){
						console.log('#success ws service');

						if(!data.HasError){

							app.utils.Storage.setSessionItem('subscribers', data.Subscribers);

							// default subscriber
							app.utils.Storage.setSessionItem('selected-subscriber-value', data.Subscribers[0].subscriber);
							app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);
							subscriber = data.Subscribers[0].subscriber;

							// cache
							app.utils.Storage.setSessionItem('subscribers-'+accountNumber, data.Subscribers);
							
							// soc code
							soc = data.Subscribers[0].mSocCode;
							
							technology = data.Subscribers[0].Tech;

                            // validate services
                            if(account.mAccountSubType == 'W' && account.mAccountType == 'I' && app.utils.Storage.getSessionItem('selected-subscriber').ProductType == 'O'){

                                // get offers
                                offertModel.getOffersDSL(

                                        //parameters
                                        app.utils.Storage.getSessionItem('token'),
                                        app.utils.Storage.getSessionItem('selected-subscriber').subscriber,

                                        //success callback
                                        function(data){

                                             var offersDSL = new Array();

                                             $.each(data.DSLCatalogItems, function(index, value){
                                                 offersDSL[index] = {
                                                       ProductType	    :data.DSLCatalogItems[index].MB1_UPLOAD,
                                                       contract	        :data.DSLCatalogItems[index].SOLO_GPON,
                                                       productId        :data.DSLCatalogItems[index].ADA_PRODUCT_ID,
                                                       //productId        :data.DSLCatalogItems[index].NUMERIC_CODE,
                                                       rent			    :data.DSLCatalogItems[index].PRICE,
                                                       soc				:data.DSLCatalogItems[index].ALPHA_CODE,
                                                       socDescription	:data.DSLCatalogItems[index].PRODUCT_NAME
                                                 }
                                             });


                                            app.utils.Storage.setSessionItem('offers', offersDSL);

                                            subscriberModel.getSVA(
                                                    //parameter
                                                    app.utils.Storage.getSessionItem('token'),
                                                    app.utils.Storage.getSessionItem('selected-account-value'),
                                                    subscriber,
                                                    offerID,

                                                    //success callback
                                                    function(data){

                                                        //TOPs
                                                        app.utils.Storage.setSessionItem('tops', data.TOPs);

                                                        //SVAs
                                                        app.utils.Storage.setSessionItem('svas', data.SVAs);

                                                        //Subscriber
                                                        $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
                                                            if(value.subscriber==subscriber){
                                                                app.utils.Storage.setSessionItem('selected-subscriber-result', value);
                                                            }
                                                        });

                                                        //Go to change plan 1
                                                        app.router.navigate('change_plan_1',{trigger: true});

                                                    },

                                                    // error function
                                                    app.utils.network.errorFunction
                                                );

                                        },

                                        // error callback
                                        function(error){

                                            showAlert('Error', data.Desc, 'Aceptar');

                                        }
                                    );
                            }else{

                                // get offers
                                offertModel.getOffers(
                                        //parameters
                                        soc.replace("'",''),
                                        technology,
                                        accountType,
                                        accountSubType,
                                        data.Subscribers[0].PlanRate,
                                        account.CreditClass,

                                        //success callback
                                        function(data){

                                            app.utils.Storage.setSessionItem('offers', data.object);

                                            subscriberModel.getSVA(
                                                    //parameter
                                                    app.utils.Storage.getSessionItem('token'),
                                                    app.utils.Storage.getSessionItem('selected-account-value'),
                                                    subscriber,
                                                    offerID,

                                                    //success callback
                                                    function(data){

                                                        //TOPs
                                                        app.utils.Storage.setSessionItem('tops', data.TOPs);

                                                        //SVAs
                                                        //app.session.SVAs = data.SVAs;
                                                        app.utils.Storage.setSessionItem('svas',data.SVAs);

                                                        //Subscriber
                                                        $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
                                                            if(value.subscriber==subscriber){
                                                                app.utils.Storage.setSessionItem('selected-subscriber-result', value);
                                                            }
                                                        });

                                                        //Go to change plan 1
                                                        app.router.navigate('change_plan_1',{trigger: true});

                                                    },

                                                    // error function
                                                    app.utils.network.errorFunction
                                                );

                                        },

                                        // error callback
                                        function(error){

                                            showAlert('Error', data.Desc, 'Aceptar');

                                        }
                                    );

							}
							
						} else {

							showAlert('Error', data.Desc, 'Aceptar');

						}

					},

					// error function
					app.utils.network.errorFunction
				);

			} else {
				
				//cache
				var subscribers = app.utils.Storage.getSessionItem('subscribers-'+accountNumber);
				app.utils.Storage.setSessionItem('subscribers', subscribers);

				// default subscriber
				app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);
				app.utils.Storage.setSessionItem('selected-subscriber-value', subscribers[0].subscriber);
				
				// soc code
				soc = subscribers[0].mSocCode;
				
				technology = subscribers[0].Tech;

				if(account.mAccountSubType == 'W' && account.mAccountType == 'I' && app.utils.Storage.getSessionItem('selected-subscriber').ProductType == 'O'){

                    // get offers
                    offertModel.getOffersDSL(
                            //parameters

                            app.utils.Storage.getSessionItem('token'),
                            app.utils.Storage.getSessionItem('selected-subscriber').subscriber,

                            //success callback
                            function(data){

                                var offersDSL = new Array();

                                $.each(data.DSLCatalogItems, function(index, value){
                                    offersDSL[index] = {
                                          ProductType	    :data.DSLCatalogItems[index].MB1_UPLOAD,
                                          contract	        :data.DSLCatalogItems[index].SOLO_GPON,
                                          productId         :data.DSLCatalogItems[index].ADA_PRODUCT_ID,
                                          //productId         :data.DSLCatalogItems[index].NUMERIC_CODE,
                                          rent			    :data.DSLCatalogItems[index].PRICE,
                                          soc				:data.DSLCatalogItems[index].ALPHA_CODE,
                                          socDescription	:data.DSLCatalogItems[index].PRODUCT_NAME
                                    }
                                });

                                app.utils.Storage.setSessionItem('offers', offersDSL);

                                subscriberModel.getSVA(
                                        //parameter
                                        app.utils.Storage.getSessionItem('token'),
                                        app.utils.Storage.getSessionItem('selected-account-value'),
                                        subscriber,
                                        offerID,

                                        //success callback
                                        function(data){

                                            //TOPs
                                            app.utils.Storage.setSessionItem('tops', data.TOPs);

                                            //SVAs
                                            //app.session.SVAs = data.SVAs;
                                            app.utils.Storage.setSessionItem('svas',data.SVAs);

                                            //Subscriber
                                            $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
                                                if(value.subscriber==subscriber){
                                                    app.utils.Storage.setSessionItem('selected-subscriber-result', value);
                                                }
                                            });

                                            //Go to change plan 1
                                            app.router.navigate('change_plan_1',{trigger: true});

                                        },

                                        // error function
                                        app.utils.network.errorFunction
                                    );

                            },

                            // error callback
                            function(error){

                                showAlert('Error', data.Desc, 'Aceptar');

                            }
                        );
                }else{

                    // get offers
                    offertModel.getOffers(
                        //parameters
                        soc.replace("'",''),
                        technology,
                        accountType,
                        accountSubType,
                        subscribers[0].PlanRate,
                        account.CreditClass,

                        //success callback
                        function(data){

                            app.utils.Storage.setSessionItem('offers', data.object);

                            subscriberModel.getSVA(
                                    //parameter
                                    app.utils.Storage.getSessionItem('token'),
                                    app.utils.Storage.getSessionItem('selected-account-value'),
                                    subscribers[0].subscriber,
                                    offerID,

                                    //success callback
                                    function(data){

                                        //TOPs
                                        app.utils.Storage.setSessionItem('tops', data.TOPs);

                                        //SVAs
                                        //app.session.SVAs = data.SVAs;
                                        app.utils.Storage.setSessionItem('svas',data.SVAs);

                                        //Subscriber
                                        $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
                                            if(value.subscriber==subscriber){
                                                app.utils.Storage.setSessionItem('selected-subscriber-result', value);
                                            }
                                        });

                                        //Go to change plan 1
                                        app.router.navigate('change_plan_1',{trigger: true});

                                    },

                                    // error function
                                    app.utils.network.errorFunction
                                );
                        },

                        // error callback
                        function(error){

                            showAlert('Error', data.Desc, 'Aceptar');

                        }
                    );
                }

			}			

		},	
		
		formatNumber: function(number){
			
			// validate border case
			number = (number == '' || number == null)? 0 : number;
			
			var formatedNumber = 0.0;
			formatedNumber =  parseFloat((new String(number)).replace('$','')).toFixed(2);

			return formatedNumber

		},

		getUserAccess: function() {
            var section = "";
            var fullListSections = [];
            var pages = null;
            var listSections = app.utils.Storage.getSessionItem('accounts-available-sections');

            fullListSections = listSections;
            // if (listAccess && listAccess.length > 0) {
            //     listAccess.forEach(function(access) {
            //         if (section === access.sectionName) {
            //             pages.push(access);
            //         } else {
            //             if (pages !== null) {
            //                 fullListAccess.push(pages);
            //             }
            //             section = access.sectionName;
            //             pages = [];
            //             pages.push(access);
            //         }
            //     });
            //     fullListAccess.push(pages);
			// }
            return fullListSections;
		},

        getSelectTabAccounts: function() {
            const selectedAccount = app.utils.Storage.getSessionItem('selected-account');
            var loginAccounts = [];
            if (selectedAccount.postpago) {
                loginAccounts = app.utils.Storage.getSessionItem('accounts-list-postpago');
            } else if (selectedAccount.prepago) {
                loginAccounts = app.utils.Storage.getSessionItem('accounts-list-prepago');
            } else if (selectedAccount.telefonia) {
                loginAccounts = app.utils.Storage.getSessionItem('accounts-list-telefonia');
            }
            return loginAccounts;
		},
		
		aditionalDataPlan:function(e){

            // close side menu
            if(app.isMenuOpen == true){
                this.closeMenu();
            }

        	var self = this,
        		accountModel = new app.models.Account(),
        		offerModel = new app.models.Offer();
        	
        	accountModel.getAccountSubscribers(
				//parameter
				app.utils.Storage.getSessionItem('token'),
				app.utils.Storage.getSessionItem('selected-account').Account,

				//success callback
				function(data){

					if(!data.HasError){

						app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
						app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);

						// cache
						app.utils.Storage.setSessionItem('subscribers-'+app.utils.Storage.getSessionItem('selected-account').Account, data.Subscribers);

						offerModel.getOffersToSubscriber(

		        			//parameters
		        			app.utils.Storage.getSessionItem('token'),

		        			data.Subscribers[0].subscriber,

		        			//success callback
		        			function(offerData){

		        				app.utils.Storage.setSessionItem('select-offer-to-subscriber', offerData.Offers);
		        				
		        				// redirect to data_plan
								app.router.navigate('data_plan', {trigger: true});
		        			},

		        			// error function
		        			function(data){

		        				showAlert('Error', data.Desc, 'Aceptar');
		        				
		        			}
			        			
			            );						

					}else{

						showAlert('Error', data.Desc, 'Aceptar');

					}

				},

				// error function
				app.utils.network.errorFunction
			);        	
        	
        },		
        
        getAccountByBAN:function(ban){
        	
        	var accounts = app.utils.Storage.getSessionItem('accounts-list'),
        		account = null;
        	
        	for (var i=0 ; i<accounts.length ; i++) {
        		var account = accounts[i].Account
        		if(account=ban){
        			break;
        		}
			}
        	
        	return account;
        	
        },
        
		active: function(e){
        
			
	        var	self = this;
            	url = app.utils.Storage.getSessionItem('navegation-path');
        	
            //alert('on commons.active: ' + url);
            	
            app.utils.Storage.removeSessionItem('navegation-path');
            
            switch(url) {
                case 'account':
                    self.account();
                    break;
                case 'invoice':
                    self.invoice();
                    break;
                case 'chat':
                    self.chat();
                    break;
                    
            }
        
        },
        
		giftSend: function(element){

			var self = this,
				analytics = null;

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}

			// send google statistics
			if(analytics !=null ){
				analytics.trackEvent('button', 'click', 'send gift section');
			}

			// validate suspend account
			if (app.utils.Storage.getSessionItem('suspend-account-init')) {
            	
				var message = '<br>Su cuenta esta suspendida. Para activar la misma, favor realice su pago. <br>';
            	
                showConfirm(
                	'Alerta',
                    'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.',
                    ['Cancelar', 'Pagar'],
                    function(button){
                        
                        // update suspend account value
                        app.utils.Storage.setSessionItem('suspend-account', true);
                    
    	                if(button==2) {
                    
    	                	self.invoice();
                        }
                    
                    }); 

                // set suspend account in true
                app.utils.Storage.setSessionItem('suspend-account', true);
            	
            } else {

                //Go to help
                app.router.navigate('gift_send',{trigger: true});

            }	
		},
		
		giftAccept: function(element){

			var analytics = null;

			// close side menu
			if(app.isMenuOpen == true){
				this.closeMenu();
			}

			// send google statistics
			if(analytics !=null ){
				analytics.trackEvent('button', 'click', 'accept gift section');
			}

			//Go to help
			app.router.navigate('gift_accept',{trigger: true});

		},
		
		addAccounts: function(e) {
			if (app.utils.Storage.getSessionItem('add-accounts') == null) {
	        	this.options.accountModel.getAccountsList(
	    			// success callback
	    			function(data) {
	    				var accounts = data.object;
	    					
	    				app.utils.Storage.setSessionItem('add-accounts-list', accounts);
	    				
	    				//cache
	    				app.utils.Storage.setSessionItem('add-accounts', true);
	    						
	    				app.router.navigate('add_accounts',{trigger: true});		  
	    			},
	
	    			// error function
	    			app.utils.network.errorFunction
	    		);
			}else {
				app.router.navigate('add_accounts',{trigger: true});
			}
        },

        giftSendRecharge: function(e) {
            app.router.navigate('gift_send_recharge', {
                trigger: true
            });
        },

        myOrder: function(e) {
            app.router.navigate('my_order', {
                trigger: true
            });
        },
        
        focus: function(e) {console.log('focus**');
            $('.footcont').hide();
        },
  
        focusOut: function(e) {console.log('focus out**');
            $('.footcont').show();
        },

        toggleClass: function(e){
			$(e.currentTarget).toggleClass('mon');
		},


        showSuspendedAccount: function(e){
            showConfirm(
                        'Alerta',
                        'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.',
                        ['Cancelar', 'Pagar'],
                        function(button){

                            var accountModel = new app.models.Account();

                            // update suspend account value
                            app.utils.Storage.setSessionItem('suspend-account', true);

                            if(button==2) {

                                accountModel.getAccountBill(
                                    //parameters
                                    app.utils.Storage.getSessionItem('token'),
                                    app.utils.Storage.getSessionItem('selected-account-value'),

                                    //success callback
                                    function(data){

                                        if(!data.HasError || data.Desc.toLowerCase().search('pdf') > 0){

                                            // account info
                                            app.utils.Storage.setSessionItem('accounts-bill-info', data);

                                            // send analytics statistics
                                            if(analytics!=null){
                                                analytics.trackEvent('confirm', 'Pagar', 'Suspended account');
                                            }

                                            app.router.navigate('invoice', {trigger: true});

                                        }else{

                                            var message = 'En este momento no está disponible esta factura';

                                            showAlert('Error', message, 'Aceptar');

                                        }

                                    },

                                    // error function
                                    app.utils.network.errorFunction
                                );

                                return false;

                            }

            });
        },

		showPasswordPrompt: function(callback) {

            var time = null;
            var now = new Date();
            var expire = new Date();

            time = app.utils.Storage.getSessionItem('confirmed-password-time');
            now = new Date();
            expire.setTime(time);

            if (app.utils.Storage.getLocalItem('password') != null) {

                if (time != null && now.getTime() <= time) {

                    callback();

                } else  {

                    var promptTitle = 'Contraseña Requerida',
                        promptMessage = 'Por favor introduce tu contraseña';

                    showPrompt(
                        promptMessage,
                        function(results){
                            switch(results.buttonIndex) {
                                case 1:
                                    var password = app.utils.Storage.getLocalItem('password'),
                                        inputPasswd = results.input1;

                                    if (password == inputPasswd) {
                                        var now = new Date();
                                        now.setTime(now.getTime() + (1000 * 60 * app.sessionPasswordTime));

                                        app.utils.Storage.setSessionItem('confirmed-password-time', now.getTime());
                                        app.utils.Storage.setSessionItem('confirmed-password', true);
                                        callback();
                                    }else {
                                        var alertMessage = 'Contraseña incorrecta';
                                        showAlert('Error', alertMessage, 'Aceptar');
                                    }
                                    break;
                                case 2:
                                    break;
                            }
                        },
                        promptTitle
                    );

                }
            } else {
                callback();
            }

        },

        sva: function(element) {
            app.router.history = ['menu'];
            app.router.navigate('sva_sell', {
                trigger: true
            });
            return false;
        },

        fixedSelectInput: function(e) {
            e.preventDefault();
        },

		getAccountDetails: function(selectedAccount, successFunction, errorFunction) {
            var self = this;
                self.options.customerModel.accountDetails(
				selectedAccount.DefaultSubscriber,
				selectedAccount.Account,
                function (response) {
                    if(response.hasError) {
                        errorFunction(response, 200, response.errorDisplay);
                    } else {
                        app.utils.Storage.setSessionItem('required-associate-account', false);

                        selectedAccount.AmtDue = response.AccounInfo.pastDueAmountField+"";
                        selectedAccount.LastPayment = response.AccounInfo.lastPaymentAmountField;
                        selectedAccount.BillDate = response.AccounInfo.cycleStartDateField;  // todo no estoy seguro
                        selectedAccount.BillDateEnd = response.AccounInfo.cycleEndDateField; // todo no estoy seguro
                        selectedAccount.BillCycle = response.AccounInfo.cycleDaysLeftField;
                        selectedAccount.CycleDate = response.AccounInfo.cycleDateField;
                        selectedAccount.CreditClass = response.AccounInfo.creditClassField;
                        selectedAccount.Paperless = response.AccounInfo.paperlessField;
                        selectedAccount.postpago = false;
                        selectedAccount.prepago = false;
                        selectedAccount.telefonia = false;

                        if ((selectedAccount.mAccountType == 'I2' && selectedAccount.mAccountSubType == '4') ||
                            (selectedAccount.mAccountType == 'I' && selectedAccount.mAccountSubType == 'R') ||
                            (selectedAccount.mAccountType == 'I' && selectedAccount.mAccountSubType == '4') ||
                            (selectedAccount.mAccountType == 'I' && selectedAccount.mAccountSubType == 'E') ) {
                            selectedAccount.postpago = true;
                            app.utils.Storage.setSessionItem('selected-tab', 0);
                        }
                        if ((selectedAccount.mAccountType == 'I' && selectedAccount.mAccountSubType == 'P') ||
                            (selectedAccount.mAccountType == 'I3' && selectedAccount.mAccountSubType == 'P') ){
                            selectedAccount.prepago = true;
                            app.utils.Storage.setSessionItem('selected-tab', 1);
                        }
                        if (selectedAccount.mAccountType == 'I' && selectedAccount.mAccountSubType == 'W'
							/*|| selectedAccount.productType == 'O'*/){ // Nueva regla, informar a Luis para verificar en web
                            selectedAccount.telefonia = true;
                            app.utils.Storage.setSessionItem('selected-tab', 2);
                        }

                        const subscribers = [];
                        $.each(response.SubscriberInfo, function (j, subscriberObj) {
                            var subscriber = {
                                subscriber: subscriberObj.subscriberNumberField,
                                Status: subscriberObj.subscriberStatusField,
                                ProductType: subscriberObj.productTypeField
                            };
                            // detect suspended account
                            if(subscriberObj.subscriberStatusField == 'S') {
                                app.utils.Storage.setSessionItem('suspend-account-init', true);
                            }
                            subscribers[j] = subscriber;
                        }); // unkwon if method is neccesay
                        selectedAccount.Subscribers = subscribers;

						app.utils.Storage.setSessionItem('selected-account-is-suspend',
							response.AccounInfo.banStatusField == "S"); // new method to know if account is suspend

                        app.utils.Storage.setSessionItem('selected-account-value', selectedAccount.Account);

                        app.utils.Storage.setSessionItem('selected-account', selectedAccount);

                        app.utils.Storage.setSessionItem('subscribers-info', response.SubscriberInfo);
                        app.utils.Storage.setSessionItem('account-info', response.AccounInfo);

                        var userInfo =  {
                            name: response.AccounInfo.firstNameField+' '+response.AccounInfo.lastNameField,
                            firstName: response.AccounInfo.firstNameField,
                            lastName: response.AccounInfo.lastNameField,
                        };
                        app.utils.Storage.setSessionItem('user-info', userInfo);

                        app.utils.Storage.setSessionItem('name', response.AccounInfo.firstNameField);

                        app.utils.Storage.setSessionItem('qualification', response.qualification);

                        self.getAccountAccess(selectedAccount, successFunction, errorFunction);
                    }
                },
                errorFunction
            );
        },

        getAccountAccess: function(selectedAccount, successFunction, errorFunction) {
            var self = this;
            self.options.customerModel.userAccess(
                selectedAccount.DefaultSubscriber,
                selectedAccount.Account,
                function (response) {
                    if(response.hasError){
                        errorFunction(response, 200, response.errorDisplay);
                    } else {

                        var sectionsList = response.Sections;

                    	if (sectionsList.length > 0 && sectionsList[0].sectionName.includes('MENU')) {
                            var sectionMyAccount = sectionsList.shift();
                            sectionMyAccount.sectionName = 'MI CUENTA';
                            sectionsList.push(sectionMyAccount);
						}

                        app.utils.Storage.setSessionItem('accounts-available-sections', sectionsList);
                        successFunction(response);
                    }
                },
                errorFunction
            );
        },

        selectPostpago: function(e) {
            var self = this;

            if (document.getElementById('tab-postpago').classList.contains("on")) {return;}

            if (app.utils.Storage.getLocalItem('logged-guest')) {
            	self.permissionDenied();
            	return;
            }

            var postpagoLoginAccounts = app.utils.Storage.getSessionItem('accounts-list-postpago');

            if (postpagoLoginAccounts.length > 0) {
                app.utils.Storage.setSessionItem('selected-tab', 0);
                $('#tab-prepago').removeClass('on');
                $('#tab-telephony').removeClass('on');
                $('#tab-postpago').addClass('on');
                self.selectAccount(postpagoLoginAccounts[0]);
            } else {
                var lastPage = app.router.history[app.router.history.length-1];
                app.utils.Storage.setSessionItem('is-from-dashboard', lastPage == 'menu');
                app.utils.Storage.setSessionItem('selected-tab-empty', 0);
                app.router.navigate('no_product_associated', {trigger: true});
			}
        },

        selectPrepago: function(e) {
            var self = this;

            if (document.getElementById('tab-prepago').classList.contains("on")) {return;}

            if (app.utils.Storage.getLocalItem('logged-guest')) {
                self.permissionDenied();
                return;
            }

            var prepagoLoginAccounts = app.utils.Storage.getSessionItem('accounts-list-prepago');

            if (prepagoLoginAccounts.length > 0) {
                app.utils.Storage.setSessionItem('selected-tab', 1);
                $('#tab-postpago').removeClass('on');
                $('#tab-telephony').removeClass('on');
                $('#tab-prepago').addClass('on');
            	self.selectAccount(prepagoLoginAccounts[0]);
			} else {
                var lastPage = app.router.history[app.router.history.length-1];
                app.utils.Storage.setSessionItem('is-from-dashboard', lastPage == 'menu');
                app.utils.Storage.setSessionItem('selected-tab-empty', 1);
                app.router.navigate('no_product_associated',{trigger: true});
            }
        },

        selectTelephony: function(e) {
            var self = this;

            if (document.getElementById('tab-telephony').classList.contains("on")) {return;}

            if (app.utils.Storage.getLocalItem('logged-guest')) {
                self.permissionDenied();
                return;
            }

            var fijoLoginAccounts = app.utils.Storage.getSessionItem('accounts-list-telefonia');

            if (fijoLoginAccounts.length > 0) {
                app.utils.Storage.setSessionItem('selected-tab', 2);
                $('#tab-postpago').removeClass('on');
                $('#tab-prepago').removeClass('on');
                $('#tab-telephony').addClass('on');
                self.selectAccount(fijoLoginAccounts[0]);
            } else {
                var lastPage = app.router.history[app.router.history.length-1];
                app.utils.Storage.setSessionItem('is-from-dashboard', lastPage == 'menu');
                app.utils.Storage.setSessionItem('selected-tab-empty', 2);
                app.router.navigate('no_product_associated',{trigger: true});
            }
        },

		selectAccount: function (account, back = false) {
            var self = this;
            self.getAccountDetails(account,
                function (response) {
            		if (back) {
                        app.router.back = true;
                        app.router.backPage();
					} else {
                        self.render(function(){
                            $.mobile.activePage.trigger('pagecreate');
                        });
					}
                },
                function (message, status) {
                    if (status == 404) {
                        showAlert('Error', 'Verifique su conexi&#243;n de internet.', 'Aceptar');
                    } else {
                        showAlert('Error', message, 'Aceptar');
                    }
                });
        },

        simpleChangeAccount: function(e){
            var self = this;

            const newAccountNumber = $.mobile.activePage.find('#select-account').val();
            const accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            const accountList = app.utils.Storage.getSessionItem('accounts-list');

            var selectAccount = null;
            $.each(accountList, function (i, object) {
                if (object.Account == newAccountNumber) {
                    selectAccount = object;
                }
            });

            self.getAccountDetails(selectAccount,
                function (response) {
                    if(analytics != null ){
                        analytics.trackEvent('select', 'change', 'select account number on ' + app.router.history[app.router.history.length-1], accountNumber);
                    }
                    self.render(function(){
                        $.mobile.activePage.trigger('pagecreate');
                    });

                },
                app.utils.network.errorRequest
            );
        },

        permissionDenied: function () {
            showConfirm(
                    'Error',
                    'No tienes permisos para esta opción.',
                    ['Ok'],
                    function(btnIndex){
                        if (btnIndex == 1){
                        	// nothing to do
                        }
                    }
                );
        },
	});
	
});