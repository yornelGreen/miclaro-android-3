$(function() {

    // Manage Notifications View
    // ---------------

    app.views.ManageNotificationsView = app.views.CommonView.extend({

        name: 'manage_notifications',

        // The DOM events specific.
        events: {

            //events
            'pagecreate': 'pageCreate',
            'active': 'active',

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',
            'change #select-account': 'changeAccount',
            'change #select-subscriber': 'changeSubscriber',

            // nav menu
            'click #close-nav':							'closeMenu',
            'click .close-menu':						'closeMenu',
            'click .open-menu':						    'openMenu',
            'click .btn-account':                       'account',
            'click .btn-consumption':                   'consumption',
            'click .btn-service':                       'service',
            'click .btn-change-plan':                   'changePlan',
            'click .btn-add-aditional-data':            'aditionalDataPlan',
            'click .btn-device':                        'device',
            'click .btn-profile':                       'profile',
            'click .btn-gift':                          'giftSend',
            'click .btn-invoice':                       'invoice',
            'click .btn-notifications':	                'notifications',
            'click .btn-sva':                           'sva',
            'click .btn-gift-send-recharge':            'giftSendRecharge',
            'click .btn-my-order':                      'myOrder',
            'click .btn-logout':                        'logout',
            'click .select-menu':						'clickMenu',

            // content
            'switchChange.bootstrapSwitch .notification-switch': 'changeNotification',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {

            //validate if logued
            var isLogued = false;
            var wirelessAccount = null;

            if (app.utils.Storage.getSessionItem('selected-account') != null) {
                isLogued = true;
                wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS') ? true : false;
            }

            var self = this,
                variables = {
                    accounts: app.utils.Storage.getSessionItem('accounts-list'),
                    selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    subscribers: app.utils.Storage.getSessionItem('subscribers'),
                    selectedSubscriber: app.utils.Storage.getSessionItem('selected-subscriber'),
                    isLogued: isLogued,
                    wirelessAccount: wirelessAccount,
                    hasPaperless: (app.utils.Storage.getSessionItem('notifications-alerts').object[3]) ? true : false,
                    hasConfirmSms: (app.utils.Storage.getSessionItem('notifications-alerts').object[2]) ? true : false,
                    hasSms: (app.utils.Storage.getSessionItem('notifications-alerts').object[1]) ? true : false,
                    productType: (app.utils.Storage.getSessionItem('selected-subscriber').ProductType == 'G') ? true : false,
                    accountAccess: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: true
                };

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });

        },

        pageCreate: function() {
            var self = this,
                account = app.utils.Storage.getSessionItem('selected-account'),
                notificationsState = app.utils.Storage.getSessionItem('notifications-state'),
                stateInvoice = notificationsState.enabledInvoiceNotification,
                statePromotion = notificationsState.enabledPromotionNotification,
                stateData = notificationsState.enabledDataNotification,
                doNotCallData = app.utils.Storage.getSessionItem('notifications-not-to-call');

            // update switch
            $('#invoice-switch').bootstrapSwitch('state', (stateInvoice == 'Y'), true);
            $('#promotion-switch').bootstrapSwitch('state', (statePromotion == 'Y'), true);
            $('#data-switch').bootstrapSwitch('state', (stateData == 'Y'), true);
            $('#nocall-switch').bootstrapSwitch('state', (doNotCallData.status == 'Inactivar'), true);

            if(app.utils.Storage.getSessionItem('notifications-alerts').object[3]){
                var paperlessData = app.utils.Storage.getSessionItem('notifications-alerts').object[3];
                $('#paperless-switch').bootstrapSwitch('state', (paperlessData.value == 'Y'), true);
            }
            if(app.utils.Storage.getSessionItem('notifications-alerts').object[2]){
                var confirm_sms = app.utils.Storage.getSessionItem('notifications-alerts').object[2];
                $('#confirm-switch').bootstrapSwitch('state', (confirm_sms.value == 'Y'), true);
            }
            if(app.utils.Storage.getSessionItem('notifications-alerts').object[1]){
                var sms = app.utils.Storage.getSessionItem('notifications-alerts').object[1];
                $('#sms-switch').bootstrapSwitch('state', (sms.value == 'Y'), true);
            }
        },

        changeNotification: function(e) {

            var username = app.utils.Storage.getSessionItem('username'),
                accounts = app.utils.Storage.getSessionItem('accounts-list'),
                subscriber = app.utils.Storage.getSessionItem('selected-subscriber'),
                userModel = new app.models.User(),
                switchChanged,
                switchState,
                previousState,
                deviceData = {},
                switchType = $(e.currentTarget).data('switchType'),
                switchValue = null,
                notificationsState = app.utils.Storage.getSessionItem('notifications-state'),
                doNotCallData = app.utils.Storage.getSessionItem('notifications-not-to-call');

            if(app.utils.Storage.getSessionItem('notifications-alerts').object[3]){
                var paperlessData = app.utils.Storage.getSessionItem('notifications-alerts').object[3];
            }
            if(app.utils.Storage.getSessionItem('notifications-alerts').object[2]){
                var confirm_sms = app.utils.Storage.getSessionItem('notifications-alerts').object[2];
            }
            if(app.utils.Storage.getSessionItem('notifications-alerts').object[1]){
                var sms = app.utils.Storage.getSessionItem('notifications-alerts').object[1];
            }

            deviceData.userName = username;

            switch (switchType) {

                case 'INVOICE':
                    console.log('---- INVOICE ----');

                    deviceData.type = 'INVOICE';
                    switchChanged = '#invoice-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    deviceData.enabledInvoiceNotification = (switchState) ? 'Y' : 'N';

                    userModel.updateNotification(
                        deviceData,
                        function(response) {
                            if (!response.hasError) {
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

                case 'PROMOTION':
                    console.log('---- PROMOTION ----');

                    deviceData.type = 'PROMOTION';
                    switchChanged = '#promotion-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    deviceData.enabledPromotionNotification = (switchState) ? 'Y' : 'N';

                    userModel.updateNotification(
                        deviceData,
                        function(response) {
                            if (!response.hasError) {
                                notificationsState.enabledPromotionNotification = (switchState) ? 'Y' : 'N';
                                app.utils.Storage.setSessionItem('notifications-state', notificationsState);
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

                case 'DATA':
                    console.log('---- DATA ----');

                    deviceData.type = 'DATA';
                    switchChanged = '#data-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    deviceData.enabledDataNotification = (switchState) ? 'Y' : 'N';
                    deviceData.suscriber = app.utils.Storage.getSessionItem('selected-subscriber').subscriber;

                    userModel.updateNotification(
                        deviceData,
                        function(response) {
                            if (!response.hasError) {
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

                case 'PAPERLESS':
                    console.log('---- PAPERLESS ----');

                    switchChanged = '#paperless-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    switchValue = (switchState) ? 'Y' : 'N';

                    var alertId = paperlessData.idAlert;

                    userModel.notificationsAlert(
                        subscriber.subscriber,
                        accounts[0].Account,
                        alertId,
                        switchValue,
                        function(response) {
                            if (!response.hasError) {
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

                case 'DONOCALL':

                    console.log('---- DO NOT CALL ----');

                    switchChanged = '#nocall-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    switchValue = (switchState) ? 'Inactivar' : 'Activar';

                    userModel.updateNotToCall(
                        accounts[0].Account,
                        subscriber.subscriber,
                        doNotCallData.status,
                        function(response) {
                            if (!response.hasError) {
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

                case 'SMS':

                    console.log('---- promociones de Mi Claro por SMS ----');

                    switchChanged = '#sms-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    switchValue = (switchState) ? 'Y' : 'N';

                    var alertId = sms.idAlert;

                    userModel.notificationsAlert(
                        subscriber.subscriber,
                        accounts[0].Account,
                        alertId,
                        switchValue,
                        function(response) {
                            if (!response.hasError) {
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

                case 'CONFIRM':

                    console.log('---- confirmaciones de pagos de Mi Claro por SMS ----');

                    switchChanged = '#confirm-switch';
                    switchState = $(switchChanged).bootstrapSwitch('state');
                    switchValue = (switchState) ? 'Y' : 'N';

                    var alertId = confirm_sms.idAlert;

                    userModel.notificationsAlert(
                        subscriber.subscriber,
                        accounts[0].Account,
                        alertId,
                        switchValue,
                        function(response) {
                            if (!response.hasError) {
                                showAlert('Aviso', 'Se ha cambiado el estado de la notificación', 'Aceptar');
                            } else {
                                showAlert('Aviso', response.desc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                    break;

            }

        },

        changeSwitchState: function(e) {

            var switchState = $('#data-switch').bootstrapSwitch('state');

            if (switchState) {
                $('#data-switch').bootstrapSwitch('state', false, true);
            }

        },

        changeAccount: function(e) {

            var self = this,
                analytics = null,
                username = app.utils.Storage.getSessionItem('username'),
                userModel = new app.models.User(),
                subscribers = null;

            var accountNumber = $.mobile.activePage.find('#select-account').val();
            app.utils.Storage.setSessionItem('selected-account-value', accountNumber);

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select account number', accountNumber);
            }

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function(index, value) {
                if (value.Account === app.utils.Storage.getSessionItem('selected-account-value')) {
                    app.utils.Storage.setSessionItem('selected-account', value);
                }
            });

            if (app.utils.Storage.getSessionItem(accountNumber) == null) {
                var accountModel = new app.models.Account();

                accountModel.getAccountInfo(
                    //parameter
                    app.utils.Storage.getSessionItem('token'),

                    accountNumber,

                    //success callback
                    function(data) {

                        if (!data.HasError) {

                            // session
                            app.utils.Storage.setSessionItem('selected-account', data);

                            // cache
                            app.utils.Storage.setSessionItem(accountNumber, data);
                            app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
                            app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);
                            app.utils.Storage.setSessionItem('subscribers-' + accountNumber, data.Subscribers);

                            var accountSuspend = false;

                            for (var i = 0; i < data.Subscribers.length; i++) {
                                if (data.Subscribers[i].status == 'S') {
                                    accountSuspend = true;
                                    break;
                                }
                            }

                            if (!app.utils.Storage.getSessionItem('suspend-account') &&
                                accountSuspend) {

                                showAlert('Mensaje', 'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.', 'Aceptar');

                                // set suspend account in true
                                app.utils.Storage.setSessionItem('suspend-account', true);

                                // get bank url
                                self.options.accountModel.doPayment(

                                    //parameters
                                    app.utils.Storage.getSessionItem('token'),
                                    app.utils.Storage.getSessionItem('selected-account-value'),
                                    app.utils.Storage.getSessionItem('selected-account').AmtDue.replace('$', '').replace(/[A-Za-z]/g, ''),

                                    // success
                                    function(data) {

                                        if (!data.HasError) {

                                            // open payment url
                                            browser = app.utils.browser.show(data.Url, true);

                                            app.utils.loader.show();

                                            // success event load url
                                            browser.addEventListener('loadstop', function(e) {

                                                // hiden loader
                                                app.utils.loader.hide();

                                                // show navegator
                                                browser.show();
                                            });

                                            // success event load url
                                            browser.addEventListener('loadstart', function(e) {
                                                if (e.url == 'https://ebill.claropr.com/login/login.jsf' ||
                                                    e.url == 'https://ebill.claropr.com/login/home.jsf' ||
                                                    e.url == 'https://checkout.evertecinc.com/Close.aspx'
                                                ) {
                                                    browser.close();
                                                }
                                            });

                                            // error event load url
                                            browser.addEventListener('loaderror', function(e) {

                                                // hiden loader
                                                app.utils.loader.hide();

                                                // close browser
                                                browser.close();
                                            });

                                        } else {

                                            showAlert('Error', data.Desc, 'Aceptar');
                                        }

                                        app.router.navigate('account', {
                                            trigger: true
                                        });

                                    },

                                    // error function
                                    app.utils.network.errorFunction

                                );

                                return;

                            } else {

                                var subscriber = app.utils.Storage.getSessionItem('selected-subscriber');

                                userModel.getNotificationsState(
                                    // parameters
                                    username,
                                    // success callback
                                    function(gnsr) {

                                        if (!gnsr.hasError) {
                                            app.utils.Storage.setSessionItem('notifications-state', gnsr.object);

                                            // get personal information
                                            userModel.getAlerts(
                                                // success function
                                                function(resultPN) {

                                                    app.utils.Storage.setSessionItem('notifications-paperless', resultPN.object);
                                                    app.utils.Storage.setSessionItem('notifications-alerts', resultPN);

                                                    // go to notification
                                                    if (app.utils.Storage.getSessionItem('notifications-not-to-call') != null) {
                                                        // go to notification
                                                        self.render(function() {
                                                            $.mobile.activePage.trigger('pagecreate');
                                                        });
                                                    }

                                                },
                                                // error function
                                                app.utils.network.errorFunction
                                            );

                                            // get no to call value
                                            userModel.getNotToCall(
                                                // account
                                                accountNumber,

                                                // subscriber
                                                subscriber.subscriber,

                                                // success function
                                                function(resultNTC) {

                                                    app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                                                    if (app.utils.Storage.getSessionItem('notifications-paperless') != null) {
                                                        // go to notification
                                                        self.render(function() {
                                                            $.mobile.activePage.trigger('pagecreate');
                                                        });
                                                    }

                                                },
                                                // error function
                                                app.utils.network.errorFunction
                                            );

                                        }

                                    },
                                    // error function
                                    app.utils.network.errorFunction
                                );

                            }

                        } else {

                            showAlert('Error', data.Desc, 'Aceptar');

                        }
                    },

                    // error function
                    app.utils.network.errorFunction
                );
            } else {

                account = app.utils.Storage.getSessionItem(accountNumber);

                // cache
                app.utils.Storage.setSessionItem('selected-account', account);

                subscribers = app.utils.Storage.getSessionItem('subscribers-' + accountNumber);
                subscriber = subscribers[0];

                // cache
                app.utils.Storage.setSessionItem('subscribers', subscribers);
                app.utils.Storage.setSessionItem('selected-subscriber', subscriber);

                userModel.getNotificationsState(
                    // parameters
                    username,
                    // success callback
                    function(gnsr) {

                        if (!gnsr.hasError) {
                            app.utils.Storage.setSessionItem('notifications-state', gnsr.object);
                            app.utils.Storage.setSessionItem('notifications-default-subscriber', subscriber);

                            // get personal information
                            userModel.getAlerts(
                                // success function
                                function(resultPN) {

                                    app.utils.Storage.setSessionItem('notifications-paperless', resultPN.object);
                                    app.utils.Storage.setSessionItem('notifications-alerts', resultPN);

                                    // go to notification
                                    if (app.utils.Storage.getSessionItem('notifications-not-to-call') != null) {
                                        // go to notification
                                        self.render(function() {
                                            $.mobile.activePage.trigger('pagecreate');
                                        });
                                    }

                                },
                                // error function
                                app.utils.network.errorFunction
                            );

                            // get no to call value
                            userModel.getNotToCall(

                                // account
                                accountNumber,

                                // subscriber
                                subscriber.subscriber,

                                // success function
                                function(resultNTC) {

                                    app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                                    if (app.utils.Storage.getSessionItem('notifications-paperless') != null) {
                                        // go to notification
                                        self.render(function() {
                                            $.mobile.activePage.trigger('pagecreate');
                                        });
                                    }

                                },
                                // error function
                                app.utils.network.errorFunction
                            );

                        }

                    },
                    // error function
                    app.utils.network.errorFunction
                );

            }

        },

        changeSubscriber: function(e) {

            var self = this,
                analytics = null,
                userModel = new app.models.User(),
                subscriber = $.mobile.activePage.find('#select-subscriber').val();

            var accountNumber = $.mobile.activePage.find('#select-account').val();

            app.utils.Storage.setSessionItem('selected-subscriber-value', $.mobile.activePage.find('#select-subscriber').val());

            $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value) {
                if (value.subscriber == app.utils.Storage.getSessionItem('selected-subscriber-value')) {
                    app.utils.Storage.setSessionItem('selected-subscriber', value);
                }
            });

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select subscriber', app.utils.Storage.getSessionItem('selected-subscriber'));
            }

            // get no to call value
            userModel.getNotToCall(
                // account
                accountNumber,

                // subscriber
                subscriber,

                // success function
                function(resultNTC) {

                    app.utils.Storage.setSessionItem('notifications-not-to-call', resultNTC);

                    // go to notification
                    self.render(function() {
                        $.mobile.activePage.trigger('pagecreate');
                    });
                },
                // error function
                app.utils.network.errorFunction
            );

            // get Alerts
            userModel.getAlerts(
                function(resultAlerts) {
                    console.log(resultAlerts);

                    app.utils.Storage.setSessionItem('notifications-alerts', resultAlerts);
                    app.utils.Storage.setSessionItem('notifications-paperless', resultAlerts.object);

                    // go to notification
                    self.render(function() {
                        $.mobile.activePage.trigger('pagecreate');
                    });

                },
                // error function
                app.utils.network.errorFunction
            );

        }

    });
});