$(function() {

	// Navmenu View
	// ---------------
	
	app.views.NavmenuView = app.views.CommonView.extend({

		name:'navmenu',
		
		// The DOM events specific.
		events: {

		},

		// Render the template elements        
		render: function(callback) {

            // var section = "";
            // var fullListAccess = [];
            // var pages = null;
			// var listAccess = app.utils.Storage.getSessionItem('access-list');

            // listAccess.forEach(function(access) {
            //     if (section === access.sectionName) {
            //         pages.push(access);
			// 	} else {
            //     	if (pages !== null) {
            //             fullListAccess.push(pages);
			// 		}
            //         section = access.sectionName;
            //     	pages = [];
            //     	pages.push(access);
			// 	}
            // });
            // fullListAccess.push(pages);

            // listAccess.forEach(function(access) {
            // 	access[0].sectionName;
            // });


            var self = this,
                variables = {
                    wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false,
                    //accessList: fullListAccess,
                    accountAccess: app.utils.Storage.getSessionItem('access-list'),
                    name: app.utils.Storage.getSessionItem('name')
                };
			
			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));	
		    	app.router.refreshPage();
		    	callback();	
		    	return this;
		    });					
		
		},
	});
});
