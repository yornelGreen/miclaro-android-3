$(function() {

	// Success Report View
	// ---------------
	
	app.views.SuccessReportView = app.views.CommonView.extend({

		name:'success_report',
		history: false,
		
		// The DOM events specific.
		events: {
			
			// events
            'active'							:'active',
            
			// body
			'click .btn-back'					:'back',
			'click .chat-btn'					:'chat',
			'click #btn-continue'				:'continue',

            // nav menu
            'click #close-nav':							'closeMenu',
            'click .close-menu':						'closeMenu',
            'click .open-menu':						    'openMenu',
            'click .btn-account':                       'account',
            'click .btn-consumption':                   'consumption',
            'click .btn-service':                       'service',
            'click .btn-change-plan':                   'changePlan',
            'click .btn-add-aditional-data':            'aditionalDataPlan',
            'click .btn-device':                        'device',
            'click .btn-profile':                       'profile',
            'click .btn-gift':                          'giftSend',
            'click .btn-invoice':                       'invoice',
            'click .btn-notifications':	                'notifications',
            'click .btn-sva':                           'sva',
            'click .btn-gift-send-recharge':            'giftSendRecharge',
            'click .btn-my-order':                      'myOrder',
            'click .btn-logout':                        'logout',
            'click .select-menu':						'clickMenu',

            // footer
            'click #btn-help'                   :'helpSection'
				
		},

		// Render the template elements        
		render: function(callback) {
			
			//validate if logued
			var isLogued = false;
			var wirelessAccount = false;

			if(app.utils.Storage.getSessionItem('selected-account') != null){
			    isLogued = true;
			    wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false;
			}

			var self = this,
				variables = {
				    isLogued: isLogued,
				    wirelessAccount: wirelessAccount,
                    accountAccess: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: true
				};

			app.TemplateManager.get(self.name, function(code){
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
		    });	
			
			// remove the current
			app.router.history.pop();
			
		},

		continue: function(e){
           app.router.navigate('help_section', {
                trigger: true,
                replace: true
            });
		}
        
	});
});
