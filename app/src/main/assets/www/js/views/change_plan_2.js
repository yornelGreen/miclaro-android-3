$(function() {

	// Plan Change View
	// ---------------

	app.views.ChangePlan2View = app.views.CommonView.extend({

		name:'change_plan_2',

		// The DOM events specific.
		events: {

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            'click .input-check-container': 'selectActivation',
			'click #btn-terms': 'changePlanTerms',

            // body
			'click #bth-accept': 'nextStep',

            // nav menu
            'click #close-nav':							'closeMenu',
            'click .close-menu':						'closeMenu',
            'click .open-menu':						    'openMenu',
            'click .btn-account':                       'account',
            'click .btn-consumption':                   'consumption',
            'click .btn-service':                       'service',
            'click .btn-change-plan':                   'changePlan',
            'click .btn-add-aditional-data':            'aditionalDataPlan',
            'click .btn-device':                        'device',
            'click .btn-profile':                       'profile',
            'click .btn-gift':                          'giftSend',
            'click .btn-invoice':                       'invoice',
            'click .btn-notifications':	                'notifications',
            'click .btn-sva':                           'sva',
            'click .btn-gift-send-recharge':            'giftSendRecharge',
            'click .btn-my-order':                      'myOrder',
            'click .btn-logout':                        'logout',
            'click .select-menu':						'clickMenu',

			// footer
			'click #btn-help': 'helpSection'

		},

		// Render the template elements
		render:function (callback) {

			// set spanish as default language
			moment.locale('es');
			var account = app.utils.Storage.getSessionItem('selected-account'),
			    dsl = false;

			if(account.mAccountSubType == 'W' && account.mAccountType == 'I' && app.utils.Storage.getSessionItem('selected-subscriber').ProductType == 'O'){
			    dsl = true;
			}

			var self = this,
				user = app.utils.Storage.getLocalItem('user'),
				account = account,
				today = null,
				actualCicle = moment().format('dddd, DD [de] MMMM [de] YYYY'),
				nextCicle = moment(account.cicleDateEnd,'MM-DD-YYYY').format('dddd, DD [de] MMMM [de] YYYY'),
				variables = {
					socCode: app.utils.Storage.getSessionItem('soc-code'),
					socDesc: app.utils.Storage.getSessionItem('soc-desc'),
					socRent: this.formatNumber(app.utils.Storage.getSessionItem('soc-rent')),
					actualCicle: actualCicle,
					nextClicle: nextCicle,
					wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false,
					dsl: dsl,
					dslMonths: (app.utils.Storage.getSessionItem('dslMonths') != null)? app.utils.Storage.getSessionItem('dslMonths'):'',
                    accountAccess: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: true
				};

			// save the cicle date
			app.utils.Storage.setSessionItem('actual-cicle-date', actualCicle);
			app.utils.Storage.setSessionItem('next-cicle-date', nextCicle);

			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));

		    	callback();
		    	return this;
		    });

		},

		nextStep: function (target) {

			var offertModel = new app.models.Offer(),
			    socActivation = $.trim($('input[type="checkbox"].checkbox-activation:checked').val()),
				application = $('.checkbox-activation:checked').val(),
				terms = $.trim($('#checkbox-terms-and-conditions:checked').val()),
				selectedSubscriber = app.utils.Storage.getSessionItem('selected-subscriber'),
				oldSoc = (selectedSubscriber.mSocCode != null)?$.trim(selectedSubscriber.mSocCode):'',
				newSoc = app.utils.Storage.getSessionItem('soc-code'),
				account = app.utils.Storage.getSessionItem('selected-account'),
				productType = app.utils.Storage.getSessionItem('selected-subscriber').ProductType, //app.utils.Storage.getSessionItem('product-type'),
				contract = $('.checkbox-activation').is(":checked") ? 'Y' : 'N' ,
				productId = app.utils.Storage.getSessionItem('product-id'),
				oldSocPrice = (selectedSubscriber.PlanRate != null)?$.trim(selectedSubscriber.PlanRate).replace('$',''):''; //app.utils.Storage.getSessionItem('soc-rent');

			if(socActivation !== '' && terms !== ''){

				app.utils.Storage.setSessionItem('soc-activation', $.trim(newSoc));
				app.utils.Storage.setSessionItem('soc-application', application);

				if(account.mAccountSubType == 'W' && account.mAccountType == 'I' && app.utils.Storage.getSessionItem('selected-subscriber').ProductType == 'O'){

				    offertModel.updateSubscriberPlanSocsDSL(

                        // parameters
                        app.utils.Storage.getSessionItem('selected-subscriber').subscriber,
                        productType,
                        socActivation,
                        contract,
                        account.Account,
                        oldSocPrice,
                        productId,

                        // success callback
                        function(data){

                            if(!data.HasError){

                                app.utils.Storage.setSessionItem('soc-details', []);

                                if (data.responseCode !== undefined ) {
                                	app.utils.Storage.setSessionItem('change-plan-order-number', data.orderNumber);
                                }

                                if (data.responseCode !== undefined ) {
                                	app.utils.Storage.setSessionItem('change-plan-response-code', data.responseCode);
                                }

                                if (data.ErrorDesc !== undefined ) {
                                	app.utils.Storage.setSessionItem('change-plan-description', data.ErrorDesc);
                                }

                                app.router.navigate('change_plan_adsl', {trigger: true});

                            } else {

                                showAlert('Error', data.ErrorDesc, 'Aceptar');

                            }

                        },

                        // error callback
                        function(error){
                            showAlert('Error', error.Desc, 'Aceptar');
                        }

                    );
				}else{
				    offertModel.updateSubscriberPlanSocs(

                        // parameters
                        oldSoc,
                        newSoc,
                        selectedSubscriber.subscriber,
                        socActivation,

                        // success callback
                        function(data){

                            if(!data.HasError){

                                app.router.navigate('change_plan_3', {trigger: true});

                            } else {

                                showAlert('Error', data.ErrorDesc, 'Aceptar');

                            }

                        },

                        // error callback
                        function(error){
                            showAlert('Error', error.Desc, 'Aceptar');
                        }

                    );
				}


			} else if(socActivation == '') {

				showAlert('Error', 'Debe seleccionar una fecha de activación para el nuevo plan.', 'Aceptar');

			} else if(terms == '') {

				showAlert('Error', 'Debe aceptar los términos y condiciones', 'Aceptar');

			}

		},

		selectActivation: function(e) {

			// remove all selected input
			$('.input-check-container').removeClass('on');

			// uncheck all the inputs
			$('input[type="checkbox"].css-checkbox2').each(function(){
				$(this).prop('checked', false);
			});

			//check the input
			$(e.currentTarget).find('input[type="checkbox"].css-checkbox2').prop('checked', true);

			$(e.currentTarget).addClass('on');

		},

		changePlanTerms: function(e) {

            app.router.navigate('change_plan_terms', {trigger: true});

		}
	});

});
