$(function() {

	// Profile View
	// ---------------
	
	app.views.ProfileView = app.views.CommonView.extend({

		name: 'profile',
		
		// The DOM events specific.
		events: {
			
			'pagecreate': 'pageCreate',
			'show.bs.popover div[data-toggle="popover"]': 'showPopOver',
			
			// header
			'click .btn-back': 'back',
			'click .btn-menu': 'menu',
			'click .btn-chat': 'chat',
			
			// body
			'click .btn-section': 'toogle',
			'click #btn-user-profile': 'saveUserProfile',
			'click #btn-password': 'savePassword',
			'click #btn-pin': 'savePin',
			'click #btn-address': 'saveAddress',
			'click .btn-cancel': 'cancel',
			'click #btn-help-old-passw': 'helpOldPassw',
			'click #btn-help-new-passw': 'helpNewPassw',
			'click #btn-help-pin': 'helpPin',
			'change #question1': 'changeQuestion',
			'change #question2': 'changeQuestion',
			'click #btn-add-accounts': 'addAccounts',

            // toggle
			'click .sectbar'                    :'toggleClass',
            'click .phonebar'                   :'toggleClass',

            // nav menu
            'click #close-nav':							'closeMenu',
            'click .close-menu':						'closeMenu',
            'click .open-menu':						    'openMenu',
            'click .btn-account':                       'account',
            'click .btn-consumption':                   'consumption',
            'click .btn-service':                       'service',
            'click .btn-change-plan':                   'changePlan',
            'click .btn-add-aditional-data':            'aditionalDataPlan',
            'click .btn-device':                        'device',
            'click .btn-profile':                       'profile',
            'click .btn-gift':                          'giftSend',
            'click .btn-invoice':                       'invoice',
            'click .btn-notifications':	                'notifications',
            'click .btn-sva':                           'sva',
            'click .btn-gift-send-recharge':            'giftSendRecharge',
            'click .btn-my-order':                      'myOrder',
            'click .btn-logout':                        'logout',
            'click .select-menu':						'clickMenu',

            // focus
            'focus .hideShowFooter'            :'focusFooter',
            'focusout .hideShowFooter'         :'focusOutFooter',

            // footer
            'click #btn-help'                   :'helpSection'
				
		},

		// Render the template elements        
		render:function (callback) {
			
			// user hasn't logged in
			if(app.utils.Storage.getSessionItem('token') == null){
				
				document.location.href = 'index.jsp';
				 
			} else {
				
				var self = this,
					variables = null,
					profile = app.utils.Storage.getSessionItem('profile'),
					rememberPassword = app.utils.Storage.getSessionItem('remember-password'),
					rememberPin = app.utils.Storage.getSessionItem('remember-pin');
				
				variables =	{
					questions: profile.QuestionList,
					name: profile.FirstName,
					telephone1: profile.ContactTelephoneNumber,
					telephone2: profile.HomeTelephoneNumber,
					lastName: profile.LastName,
					email: profile.Email,		
					oldPassword: (rememberPassword != null) ? rememberPassword.oldPassword : '',
					newPassword: (rememberPassword != null) ? rememberPassword.newPassword : '',
					newConfPassword: (rememberPassword != null) ? rememberPassword.newConfPassword : '',
					question1: (profile.UserQuestionResponse !== undefined) ? profile.UserQuestionResponse.pwd_question : 0,
					answer1: (rememberPassword != null) ? rememberPassword.answer1 : profile.UserQuestionResponse.pwd_quest_ans,
					pin: (rememberPin != null) ? rememberPin.pin : '',
					pinConf: (rememberPin != null) ? rememberPin.pinConf : '',
					question2: (profile.UserQuestionResponse !== undefined) ? profile.UserQuestionResponse.pin_quest : 0,
					answer2: (rememberPin != null) ? rememberPin.answer2 : profile.UserQuestionResponse.pin_quest_ans,
					address1: profile.AddressDet,
					address2: profile.AddressDet2,
					city: profile.City,
					state: profile.State,
					country: profile.Country,
					zip: profile.ZipCode,
					wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS'),
                    accountAccess: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: true
				};
				
				app.TemplateManager.get(self.name, function(code){
			    	var template = cTemplate(code.html());
			    	$(self.el).html(template(variables));	
			    	
			    	callback();	
			    	return this;
			    });	
				
			}
					
		},
		
		pageCreate: function(e) {
        
            // enable tooltips
			$('[data-toggle="popover"]').popover({
				animation: false
			});
			
			$("#password-new").popover({
		        html : true,
		        animation: false,
		        target: '#password',
		        placement: 'bottom',
		        content: function() {
		          return $('#popover-content-input-password').html();
		        }
		    });			
			
			// allow only number
			$.mobile.activePage.on("input", "#pin-conf, #telephone1, #telephone2, #pin", function() {
			    this.value = this.value.replace(/[^0-9]/g,'');
			});
            
			// allow only number
			$.mobile.activePage.on("show.bs.popover", "#popover-password", function() {
			    setTimeout(function(){
					$.mobile.activePage.find('[data-toggle="popover"]').popover('hide');
				},4000);
			});
			
			// allow only number and dash
			var SPMaskBehavior = function (val) {
					return val.replace(/\D/g, '').length === 11 ? '00000' : '00000-0000';
				},
				spOptions = {
					onKeyPress: function(val, e, field, options) {
						field.mask(SPMaskBehavior.apply({}, arguments), options);
					}
				};

			$('#zip').mask(SPMaskBehavior, spOptions);
			
			// show pasword popover
			$.mobile.activePage.on("input", "#password-new", function() {
				
				var newPassword = $("#password-new").val();
				
				// length
				if (!(newPassword.length>=8 && newPassword.length<=10)) {
					$('.validate-length').find('.img-cancel').removeClass('hide');
					$('.validate-length').find('.img-accept').addClass('hide');
				} else {
					$('.validate-length').find('.img-cancel').addClass('hide');
					$('.validate-length').find('.img-accept').removeClass('hide');
				}
				
				// lower
				if (!/(?=.*[a-z])/.test(newPassword)) {
					$('.validate-lower').find('.img-cancel').removeClass('hide');
					$('.validate-lower').find('.img-accept').addClass('hide');
				} else {
					$('.validate-lower').find('.img-cancel').addClass('hide');
					$('.validate-lower').find('.img-accept').removeClass('hide');					
				}
				
				//upper
				if (!/(.*[A-Z].*)/.test(newPassword)) {
					$('.validate-upper').find('.img-cancel').removeClass('hide');
					$('.validate-upper').find('.img-accept').addClass('hide');
				} else {
					$('.validate-upper').find('.img-cancel').addClass('hide');
					$('.validate-upper').find('.img-accept').removeClass('hide');					
				}
				
				// two digit
				if (!/((.*\d{2}.*))/.test(newPassword)) {
					$('.validate-number').find('.img-cancel').removeClass('hide');
					$('.validate-number').find('.img-accept').addClass('hide');
				} else {
					$('.validate-number').find('.img-cancel').addClass('hide');
					$('.validate-number').find('.img-accept').removeClass('hide');					
				}
				
				// no special character
				if (!/([a-zA-Z0-9]+$)/.test(newPassword)) {
					$('.validate-character').find('.img-cancel').removeClass('hide');
					$('.validate-character').find('.img-accept').addClass('hide');
				} else {
					$('.validate-character').find('.img-cancel').addClass('hide');
					$('.validate-character').find('.img-accept').removeClass('hide');					
				}	
				
				// repeat character
				if (/(.)\1{3,}/.test(newPassword)) {
					$('.validate-repeat').find('.img-cancel').removeClass('hide');
					$('.validate-repeat').find('.img-accept').addClass('hide');
				} else {
					$('.validate-repeat').find('.img-cancel').addClass('hide');
					$('.validate-repeat').find('.img-accept').removeClass('hide');					
				}	
				
			});
			
			// hide pasword popover
			$.mobile.activePage.on('focusout', '#password-new', function(e) {
				$("#password-new").popover('hide');
			});
			
			
			// active previous section
			if (app.utils.Storage.getSessionItem('remember-password') !== null) {				
				// hidden all container
				$('.info-container').addClass('hide');
				// show selected container
				$('#password').removeClass('hide');
				// change arrow position
				$('#btn-toggle-password').find('.arrow-up').removeClass('hide');				
				$('#btn-toggle-password').find('.arrow-down').addClass('hide');
			} else if (app.utils.Storage.getSessionItem('remember-pin') !== null) {
				// hidden all container
				$('.info-container').addClass('hide');
				// show selected container
				$('#pin-info').removeClass('hide');
				// change arrow position
				$('#btn-toggle-pin').find('.arrow-up').removeClass('hide');				
				$('#btn-toggle-pin').find('.arrow-down').addClass('hide');
			}
			
			// delete from session
			app.utils.Storage.removeSessionItem('remember-password');
			app.utils.Storage.removeSessionItem('remember-pin');			
			
		}, 

        focusFooter: function(e) {
            $('.footcont').hide();
        },

        focusOutFooter: function(e) {
            $('.footcont').show();
        },

		toogle: function (e) {
			
			var container = $(e.currentTarget).data('container');
			
			if ($('#' + container).hasClass('hide')) {
				// hidden all container
				$('.info-container').addClass('hide');
				
				// show selected container
				$('#' + container).removeClass('hide');
				
				// change arrow position
				$(e.currentTarget).find('.arrow-up').removeClass('hide');				
				$(e.currentTarget).find('.arrow-down').addClass('hide');
				
			} else {
				// hidden all container
				$('.info-container').addClass('hide');
				
				// change arrow position
				$(e.currentTarget).find('.arrow-up').addClass('hide');				
				$(e.currentTarget).find('.arrow-down').removeClass('hide');				
				
				// change arrow position
				$(e.currentTarget).removeClass('arrow-up').addClass('arrow-down');
			}
			
			$(e.currentTarget).find('anchor').focus();
			
		},
		
		cancel: function (e) {

		    var self = this,
		        target = $(e.currentTarget).data('target');
			
			// hidden all container
			$(target).collapse('toggle');

			//setting initial values
            self.render(function () {
                $.mobile.activePage.trigger('pagecreate');
            });
			
		},
		
		saveUserProfile: function (e) {
			console.log('click save user');
			
			var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			
			var accounts = app.utils.Storage.getSessionItem('accounts-list'),
				email = $.mobile.activePage.find('#email').val(),
				telephone1 = $.mobile.activePage.find('#telephone1').val(),
				telephone2 = $.mobile.activePage.find('#telephone2').val();
			
			// validate form
			if(!email.length>0 ){
				message = 'Debe colocar su correo electrónico';
				showAlert('Error', message, 'Aceptar', function(){});
				return;
			}else if (!emailReg.test(email)){ 
				message = 'Correo eléctronico no es un formato válido';
				showAlert('Error', message, 'Aceptar', function(){});
				//setting initial values
                self.render(function () {
                    $.mobile.activePage.trigger('pagecreate');
                });
				return;
			}else if(!telephone1.length>0){
				message = 'Debe colocar Tel. de Contacto';
				showAlert('Error', message, 'Aceptar', function(){});
				return;
			}else if (!telephone2.length>0) {
				message = 'Debe colocar Tel. de Contacto';
				showAlert('Error', message, 'Aceptar', function(){});
				return;
			}else if(telephone1.length !== 10 ||
					telephone2.length !== 10) {
				message = 'Formato de teléfono invalido. Favor ingresar los 10 números de teléfono.';
				showAlert('Error', message, 'Aceptar', function(){});
				return;
			}	
			
			// send data to services
			var userModel = new app.models.User();
			
			var data = {
				account: accounts[0].Account,
				email: email,
				telephone1: telephone1,
				telephone2: telephone2
			};
				
			userModel.updatePersonalData(
				// parameters
				app.utils.Storage.getSessionItem('token'),
				data,
				// success callback
				function(success){
					
					if (!success.HasError) {
						
						var profile = app.utils.Storage.getSessionItem('profile');
						
						profile.ContactTelephoneNumber = telephone1;
						profile.HomeTelephoneNumber =  telephone2
						profile.Email = email;
						
						// show alert
						showAlert(
								'Mensaje', 
								'Sus datos fueron actualizados',
								'Aceptar',
								function(e){}
							);
					} else {
						// show alert
						showAlert(
								'Error', 
								success.ErrorDesc,
								'Aceptar',
								function(e){}
							);
					} 					
					
				},
				// error callback
				app.utils.network.errorFunction
			);			
			
			
		},
		
		savePassword: function (e) {
			console.log('click save password');
			
			var oldPassword = $.mobile.activePage.find('#password-old').val(),
				newPassword = $.mobile.activePage.find('#password-new').val(),
				confNewpassword = $.mobile.activePage.find('#password-new-conf').val(),
				profile = app.utils.Storage.getSessionItem('profile'),
				questions1 = profile.UserQuestionResponse.pwd_question,
				answer1 = profile.UserQuestionResponse.pwd_quest_ans;
			
			// validate
			if(!oldPassword.length>0 ){
				message = 'Debe colocar su contaseña anterior';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			} if ((!newPassword.length>=8 &&
						!newPassword.length<=10) || // length 8-10
						!/(?=.*[a-z])/.test(newPassword) || // almost one lower case
						!/(.*[A-Z].*)/.test(newPassword) || // almost one upper case
						!/((.*\d{2}.*))/.test(newPassword) || // two digit 
						!/([a-zA-Z0-9]+$)/.test(newPassword) || // special character 
						/(.)\1{3,}/.test(newPassword) ){ // repeat 4 times 
				message = 'La Contraseña debe tener entre 8 y 10 caracteres, incluyendo un mínimo de 2 números y 2 letras, diferenciando entre máyuscula y minúsculas ("case sensitive"). No se puede repetir el mismo carácter más de 4 veces consecutivas';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;				
			}else if(!confNewpassword.length>0){
				message = 'Debe confirmar la contraseña';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			}else if(newPassword != confNewpassword){
				message = 'La nueva contraseña no coincide con la confirmación de la contraseña ';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;				
			}else if (!answer1.length>0) {
				message = 'Debe colocar su respuesta';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			}	
			
			// send data to services
			var userModel = new app.models.User();
			
			var data = {
				question1: questions1,
				answer1: answer1,
				newPassword: newPassword,
				oldPassword: oldPassword
			};
				
			userModel.updatePassword(
				// parameters
				app.utils.Storage.getSessionItem('token'),
				data,
				// success callback
				function(success){
					
					if (!success.HasError) {
						
						// reset input 
						$.mobile.activePage.find('#password-old').val('');
						$.mobile.activePage.find('#password-new').val('');
						$.mobile.activePage.find('#password-new-conf').val('');					
						$.mobile.activePage.find('#answer1').val('');
                        
                        // update password for touchId
                        if (app.utils.Storage.getLocalItem('password') != null) {
                        	app.utils.Storage.setLocalItem('password', newPassword);
                        }
						
						// show alert
						showAlert(
								'Mensaje', 
								'Sus datos fueron actualizados', 
								'Aceptar',
								function(e){}
							);
					} else {
						// show alert
						showAlert(
								'Error', 
								success.ErrorDesc, 
								'Aceptar',
								function(e){}
							);
					}  
					
				},
				// error callback
				app.utils.network.errorFunction
			);			
			
		},
		
		savePin: function (e) {
			
			var accounts = app.utils.Storage.getSessionItem('accounts-list'),
				pin = $.mobile.activePage.find('#pin').val(),
				pinConf = $.mobile.activePage.find('#pin-conf').val(),
				questions2 = $.mobile.activePage.find('#question2').val(),
				answer2 = $.mobile.activePage.find('#answer2').val(),
				userName = app.utils.Storage.getSessionItem('username'),
				profile = app.utils.Storage.getSessionItem('profile');
			
			// validate 
			if (!pin.length>0 ) { 
				message = 'Debe colocar PIN de CSR';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			} else if (!pinConf.length>0) { 
				message = 'Debe confirmar el PIN de CSR';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			} else if (!/(?=.*[0-9]{4})/.test(pin)) { 
				message = 'Debe colocar solo carácteres numéricos';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			}else if(pin != pinConf){
				message = 'El nuevo pin no coincide con la confirmación ';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;								
			}else if (!answer2.length>0) {
				message = 'Debe colocar su respuesta';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			}	
			
			// send data to services
			var userModel = new app.models.User();
			
			var data = {
				account: accounts[0].Account,
				question2: questions2,
				answer2: answer2,
				pin: pin
			};
			
			userModel.updatePin(
				// parameters
				app.utils.Storage.getSessionItem('token'),
				data,
				// success callback
				function(success){
					
					if (!success.HasError) {
						
						// reset input values
						$.mobile.activePage.find('#pin').val('');
						$.mobile.activePage.find('#pin-conf').val('');
						$.mobile.activePage.find('#answer2').val('');
						
						// show alert
						showAlert(
								'Mensaje', 
								'Sus datos fueron actualizados',
								'Aceptar',
								function(e){}
							);
					} else {
						// show alert
						showAlert(
								'Error', 
								success.ErrorDesc,
								'Aceptar',
								function(e){}
							);
					}  

				},
				// error callback
				app.utils.network.errorFunction
			);
			
		},
		
		saveAddress: function (e) {
			
			var accounts = app.utils.Storage.getSessionItem('accounts-list'),
				address1 = $.mobile.activePage.find('#address1').val(),
				address2 = $.mobile.activePage.find('#address2').val(),
				city = $.mobile.activePage.find('#city').val(),
				country = $.mobile.activePage.find('#country').val(),
				zip = $.mobile.activePage.find('#zip').val();
			
			// validate form
			if(!address1.length>0 ){
				message = 'Debe colocar la dirección';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			}else if (!city.length>0){ 
				message = 'Debe confirmar la ciudad';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;
			}else if (!zip.length>0) {
				message = 'Debe colocar el código postal';
				showAlert('Error', message, 'Aceptar', function(e){});
				return;				
			}	
			
			// send data to services
			var userModel = new app.models.User();
			
			var data = {
				account: accounts[0].Account,
				address1: address1,
				address2: address2,
				city: city,
				zip: zip
			};
			
			userModel.updatePersonalDir(
				// parameters
				app.utils.Storage.getSessionItem('token'),
				data,
				// success callback
				function(success){
					
					var profile = app.utils.Storage.getSessionItem('profile');
				
					profile.AddressDet = address1;
					profile.AddressDet2 = address2;
					profile.City = city;
					profile.ZipCode = zip;
					
					app.utils.Storage.setSessionItem('profile', profile);				
					
					showAlert(
							'Mensaje', 
							'Sus datos fueron actualizados',
							'Aceptar',
							function(e){}
						);	
					
				},
				// error callback
				app.utils.network.errorFunction
			);
			
		},
		
		helpOldPassw: function(e) {
			
			// save current data
			var remeberPassword = {
				oldPassword: $.mobile.activePage.find('#password-old').val(),
				newPassword: $.mobile.activePage.find('#password-new').val(),
				newConfPassword: $.mobile.activePage.find('#password-new-conf').val(),
				questions1: $.mobile.activePage.find('#question1').val(),
				answer1: $.mobile.activePage.find('#answer1').val()
			};
			app.utils.Storage.setSessionItem('remember-password', remeberPassword);
			app.router.navigate('help_old_passw_profile',{trigger: true});
		},
		
		helpNewPassw: function(e) {
			
			// save current data
			var remeberPassword = {
				oldPassword: $.mobile.activePage.find('#password-old').val(),
				newPassword: $.mobile.activePage.find('#password-new').val(),
				newConfPassword: $.mobile.activePage.find('#password-new-conf').val(),
				questions1: $.mobile.activePage.find('#question1').val(),
				answer1: $.mobile.activePage.find('#answer1').val()
			};
			app.utils.Storage.setSessionItem('remember-password', remeberPassword);			
			app.router.navigate('help_new_passw_profile',{trigger: true});
		},
		
		helpPin: function(e) {
			// save current data
			var remeberPin = {
				pin: $.mobile.activePage.find('#pin').val(),
				pinConf: $.mobile.activePage.find('#pin-conf').val(),
				questions2: $.mobile.activePage.find('#question2').val(),
				answer2: $.mobile.activePage.find('#answer2').val()
			};
			app.utils.Storage.setSessionItem('remember-pin', remeberPin);
			app.router.navigate('help_pin_profile',{trigger: true});			
			
		},
		
		showPopOver: function(e) {
			setTimeout(function(){
				$.mobile.activePage.find('[data-toggle="popover"]').popover('hide');				
			},4000);			
		},
        
        changeQuestion: function(e) {
        	
        	var profile = app.utils.Storage.getSessionItem('profile'),
    		questions = profile.QuestionList,
    		questionSelect = $(e.currentTarget).attr('name') == 'question1' ? '#question1-value' : '#question2-value',
    		questionIndex = 0;
	    	
	    	for (var i=0 ; i<questions.length ; i++) {
	    		if ($(e.currentTarget).val() == questions[i].QuestionID) {
	    			questionIndex = i;
	    			break;
	    		}
	    	}
	    	
	    	$(questionSelect).html(questions[questionIndex].Question);
        	
        }
	
	});
});