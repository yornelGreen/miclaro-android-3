$(function() {

	// About View
	// ---------------
	
	app.views.AboutView = app.views.CommonView.extend({

		name:'about',
		
		// The DOM events specific.
		events: {
			
			// header
			'click .btn-back'					:'back',
			'click .chat-btn'					:'chat',

           // menu
           'click .btn-account'                :'account',
           'click .btn-consumption'            :'consumption',
           'click .btn-service'                :'service',
           'click .btn-change-plan'            :'changePlan',
           'click .btn-add-aditional-data'     :'aditionalDataPlan',
           'click .btn-device'                 :'device',
           'click .btn-profile'                :'profile',
           'click .btn-gift'                   :'giftSend',
           'click .btn-invoice'                :'invoice',
           'click .close-menu'                 :'closeMenu',
           'click .open-menu'                  :'openMenu',
           'click .btn-sva'                    :'sva',
           'click .btn-gift-send-recharge'     :'giftSendRecharge',
           'click .btn-my-order'               :'myOrder',

           // footer
           'click #btn-help'                   :'helpSection'
				
		},

		// Render the template elements        
		render: function(callback) {

			//validate if logued
            var isLogued = false;
            var wirelessAccount = null;

            if(app.utils.Storage.getSessionItem('selected-account') != null){
                isLogued = true;
                wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false;
            }

			var self = this,
				variables = {
					version: app.version,
					isLogued: isLogued,
					showBackBth: true,
                    wirelessAccount: wirelessAccount
				};
			
			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));	
		    	callback();	
		    	return this;
		    });				
		
		},
	
	});
});
