$(function() {

    // Device View
    // ---------------

    app.views.DeviceView = app.views.CommonView.extend({

        name: 'device',

        deviceSwiper: null,

        // The DOM events specific.
        events: {

            // events
            'pagecreate':                               'pageCreate',

            // content
            'change #select-account':                   'simpleChangeAccount',

            'click #details-plan':                      'showDetailsPlan',
            'click #details-device':                    'showDetailsDevice',
            'click .select-subscriber-plan':            'changeSubscriberPlan',
            'click .select-subscriber-device':          'changeSubscriberDevice',
            'click .btn-fixed-failure':                 'goFixedFailureReport',
            'click #btn-pay-quota':                     'goPayQuota',

            // toggle
            'click .sectbar': 'toggleClass',
            'click .phonebar': 'toggleClass',

        },

        // Render the template elements
        render: function(callback) {

            if (app.utils.Storage.getSessionItem('token') == null) {
                document.location.href = 'index.html';
                return;
            }

            var subscribers = app.utils.Storage.getSessionItem('subscribers-info');

            var self = this,
                variables = {
                    typeOfTelephony: app.utils.tools.typeOfTelephony,
                    subscribers: subscribers,
                    accounts: this.getSelectTabAccounts(),
                    formatNumber: app.utils.tools.formatSubscriber,
                    selectedTab: app.utils.Storage.getSessionItem('selected-tab'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    accountSections: this.getUserAccess(),
                    showBackBth: true
                };

            app.TemplateManager.get(self.name, function(code){
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
        },

        pageCreate: function(e) {
            var self = this;
            self.activateMenu(e);

            var subscribers = app.utils.Storage.getSessionItem('subscribers');
            if (subscribers.length == 1) {
                $('.select-subscriber-plan').eq(0).trigger('click');
                $('.select-subscriber-device').eq(0).trigger('click');
            }
        },

        goFixedFailureReport: function(e) {
            //Go to Fixed Failure Report
            app.router.navigate('fixed_failure_report', {
                trigger: true
            });
        },

        goPayQuota: function(e) {
            //Go to Pay Quotas Device
            app.router.navigate('pay_quota', {
                trigger: true
            });
        },

        changeSubscriberPlan: function(e) {

            var currentIndex = $(e.currentTarget).data('index'),
                subscribers = app.utils.Storage.getSessionItem('subscribers-info'),
                subscriber = subscribers[currentIndex];

            var plan = subscriber.planInfoField;

            // var planFullName = subscriber.Plan;
            // var planName = planFullName.split(':')[0].trim();
            // var planAmount = subscriber.PlanRate;
            // var subPlans = planFullName.split(':')[1].split('-');
            //
            // var htmlPacks = '';
            // subPlans.forEach(function(pack) {
            //     htmlPacks += '<div class="redstat m-top-ii">\n' +
            //         '\t\t\t<div class="basicrow roboto-r">\n' +
            //         '\t\t\t\t'+pack.trim()+'\n' +
            //         '\t\t\t</div>\n' +
            //         '\t\t</div>'
            // });

            var htmlID = '#subscriber-plan'+currentIndex;

            $(htmlID).find('.plan-name').html(plan.sOCDescriptionField);
            $(htmlID).find('.plan-amount').html("$"+plan.totalRateField);
            // $(htmlID).find('.plan-packs').html(htmlPacks);






            $(e.currentTarget).toggleClass('mon');
        },

        changeSubscriberDevice: function(e) {

            var currentIndex = $(e.currentTarget).data('index'),
                subscribers = app.utils.Storage.getSessionItem('subscribers-info'),
                subscriber = subscribers[currentIndex];

            var info = subscriber.equipmentInfoField;

            var image = subscriber.imgDivice;


            var htmlID = '#subscriber-device'+currentIndex;

            $(htmlID).find('.equip-name').html(info.itemDescriptionField);

            $(htmlID).find('.equip-photo').attr("src", image);

            $(e.currentTarget).toggleClass('mon');
        },



        showDetailsPlan: function() {
            $('#details-plan').addClass('on');
            $('#details-device').removeClass('on');

            $('.list-plan').show();
            $('.list-device').hide();
        },

        showDetailsDevice: function() {
            $('#details-plan').removeClass('on');
            $('#details-device').addClass('on');

            $('.list-plan').hide();
            $('.list-device').show();
        },

    });
});
