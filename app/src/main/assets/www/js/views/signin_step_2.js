$(function() {

    // Register step 1 View
    // ---------------

    app.views.SigninStep2View = app.views.CommonView.extend({

        name: 'signin_step_2',

        // The DOM events specific.
        events: {
            // event
            'pagecreate':                           	'pageCreate',

            // content
            'click #btn-validate':                      'validateData',
            'click #btn-login':                         'login',
            'click #btn-resend':                        'send',
            'change #checkbox-terms':                   'availableNext',
            'click #close-terms':                       'closeTerms',
            'click #link-terms':                        'showTerms',
            'click #container_text-terms':              'invertCheckbox',
            'input #ssn':                               'ssnChanged',
            'input #code':                              'availableNext',
            'input #email':                             'availableNext',

            // footer
            'click #btn-help':							'helpSection'
        },

        // Render the template elements
        render: function(callback) {
            var number = app.utils.Storage.getLocalItem('register-number');
            var number_cut = number.substr(number.length - 4);
            var self = this,
                variables = {
                    showBackBth: true,
                    number_cut: number_cut,
                    isPrepago: app.utils.Storage.getSessionItem('register-number-is-prepago'),
                    number: number,
                };
            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
        },

        pageCreate: function(){
            var self = this;
            // removing any enter event
            $('body').unbind('keypress');

            /**
             * set enter event
             */
            $('body').on('keypress', function(e){
                if (e.which === 13 || e.keyCode === 13) {
                    self.validateData();
                }
            });

            $('#ssn').on('click focus', function (e) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#ssn").offset().top-20
                }, 1000);
            });
            $('#code').on('click focus', function (e) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#code").offset().top-20
                }, 1000);
            });
            $('#email').on('click focus', function (e) {

                if (app.utils.Storage.getSessionItem('register-number-is-prepago')) {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#code").offset().top-20
                    }, 1000);
                } else {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#ssn").offset().top-20
                    }, 1000);
                }
            });
        },

        login: function(e) {
            
            //Go to next
            app.router.navigate('login', {
                trigger: true
            });

        },

        ssnChanged: function(e) {
            self = this;

            var number = $.mobile.activePage.find('#ssn').val();

            if (number.length > 4) {
                number = number.slice(0,4);
                $.mobile.activePage.find('#ssn').val(number);
            }
            self.availableNext(e);
        },

        validateData: function(e) {
            self = this;

            var ssn = $.mobile.activePage.find('#ssn').val();
            var email = $.mobile.activePage.find('#email').val();
            var code = $.mobile.activePage.find('#code').val();

            var isPrepago = app.utils.Storage.getSessionItem('register-number-is-prepago');

            // validate
            if(!isPrepago && !ssn.length == 4){
                message = 'Debe ingresar los datos solicitados.';
                showAlert('Error', message, 'Aceptar');
                return;
            }
            if(isPrepago && code.length == 0){
                message = 'Debe ingresar los datos solicitados.';
                showAlert('Error', message, 'Aceptar');
                return;
            }
            if(!email.length > 0){
                message = 'Debe ingresar los datos solicitados.';
                showAlert('Error', message, 'Aceptar');
                return;
            }
            if (!app.utils.tools.validateEmail(email)) {
                message = 'Debe ingresar un correo electrónico válido.';
                showAlert('Error', message, 'Aceptar');
                return;
            }
            $('#ssn').blur();
            $('#email').blur();
            $('#code').blur();
            var check = $('#checkbox-terms').is(':checked');
            if (!check) {
                showAlert('Error' , 'Debe seleccionar los términos y condiciones para poder continuar.', 'OK');
                return
            }

            if (isPrepago) { // temporal
                ssn = code;
            }

            var number = app.utils.Storage.getLocalItem('register-number');

            self.options.loginModel.validateSSNAndEmail(number, ssn, email,
                function (response) {
                    if(response.hasError){
                        showAlert('Error', response.errorDisplay, 'Aceptar');
                    } else {
                        app.utils.Storage.setLocalItem('register-number', number);
                        app.utils.Storage.setLocalItem('register-email', email);
                        app.utils.Storage.setLocalItem('register-ssn', ssn);
                        app.router.navigate('signin_step_3', {
                            trigger: true
                        });
                    }
                },
                app.utils.network.errorRequest
            );
        },

        invertCheckbox: function (e) {
            var self = this;
            var check = $('#checkbox-terms').is(':checked');
            $('#checkbox-terms').prop('checked', !check);
            self.availableNext(e);
        },

        showTerms: function(e) {
            $('.popupbg').show();
        },

        availableNext: function(e) {

            var isPrepago = app.utils.Storage.getSessionItem('register-number-is-prepago');
            var ssn = $.mobile.activePage.find('#ssn').val();
            var email = $.mobile.activePage.find('#email').val();
            var code = $.mobile.activePage.find('#code').val();
            var check = $('#checkbox-terms').is(':checked');

            if ((check && isPrepago && code.length > 0 && app.utils.tools.validateEmail(email)) ||
                (check && !isPrepago && ssn.length == 4 && app.utils.tools.validateEmail(email))) {
                $('#btn-validate').removeClass('gray');
                $('#btn-validate').addClass('red');
                $('#btn-validate').addClass('rippleR');
            } else {
                $('#btn-validate').removeClass('red');
                $('#btn-validate').removeClass('rippleR');
                $('#btn-validate').addClass('gray');
            }
        },

        closeTerms: function(e) {
            $('.popupbg').hide();
        },

        send: function(e) { // TODO; poner a funcionar
            showAlert('', 'Su código de verificación ha sido enviado nuevamente.', 'Ok');
        },

    });

});
