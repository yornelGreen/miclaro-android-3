$(function() {

	// Login View
	// ---------------

	app.views.LoginGuestView = app.views.CommonView.extend({

		name:'login_guest',

		// The DOM events specific.
		events: {

            // event
			'pagecreate':                           	'pageCreate',

			// content
			'click #btn-login': 						'login',
			'click #btn-forgot': 						'forgot',
			'click #btn-register': 						'register',
            'click #btn-normal':                        'activeLoginNormal',
            'input #number':                            'numberChanged',

			// footer
			'click #btn-help':							'helpSection'

		},

		// Render the template elements
		render:function (callback) {

			var outdatedApp = app.utils.Storage.getLocalItem('outdated-app');

			// if app is outdated
			if(outdatedApp != null && outdatedApp){
				navigator.app.exitApp();
			} else {
                var self = this;
				var user = (app.utils.Storage.getLocalItem('user') != null) ? app.utils.Storage.getLocalItem('user') : '',
                    passw = (app.utils.Storage.getLocalItem('password') != null) ? app.utils.Storage.getLocalItem('password') : '',
                    variables = {
                    	login: (user != null)? user: '',
	                    password: passw,
    	                remember: (app.utils.Storage.getLocalItem('remember') !== null),
        	        };

                var navegationPath = app.utils.Storage.getSessionItem('navegation-path');

				//clear session
				app.utils.Storage.clearSessionStorage();

				// set navegation path
                app.utils.Storage.setSessionItem('navegation-path', navegationPath);

				//Delete Register Data
				app.utils.Storage.removeSessionItem('register-data');
				app.utils.Storage.removeSessionItem('register-alert');

				app.TemplateManager.get(self.name, function(code){
			    	var template = cTemplate(code.html());
			    	$(self.el).html(template(variables));

			    	callback();
			    	return this;
			    });
			}

		},

		pageCreate: function(){
            var self = this;
            // removing any enter event
            $('body').unbind('keypress');

            /**
             * if is logged
             */
            const isLogged = app.utils.Storage.getLocalItem('isLogged');
            if (isLogged) {
                const isGuest = app.utils.Storage.getLocalItem('logged-guest');
                if (isGuest) {
                    setTimeout(function() { // delay porque si no la vista de loading no se muestra
                        const number = app.utils.Storage.getLocalItem('logged-subscriber');
                        self.signOnGuest(number);
                    }, 100);
                } else {
                    app.router.navigate('login', {
                        trigger: true
                    });
                }
            } else {
                var loginGuest = true;
                var loginModeGuest = app.utils.Storage.getLocalItem('loginModeGuest');
                if (loginModeGuest != null) {
                    loginGuest = loginModeGuest;
                }
                if (!loginGuest) {
                    app.router.navigate('login', {
                        trigger: true
                    });
                }
            }

            /**
             * set enter event
             */
			$('body').on('keypress', function(e){
				if (e.which === 13 || e.keyCode === 13) {
			    	self.login();
			    }
			});

            $('#number').on('click focus', function (e) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#number").offset().top-20
                }, 1000);
            });
		},

        activeLoginNormal: function(){
            app.utils.Storage.setLocalItem('loginModeGuest', false);
            app.router.navigate('login', {
                trigger: true
            });
        },

        numberChanged: function() {
            var number = $.mobile.activePage.find('#number').val();

            if (number.length > 10) {
                number = number.slice(0,10);
                $.mobile.activePage.find('#number').val(number);
            }
        },

		login: function(){
            var self = this;

            const number = $.mobile.activePage.find('#number').val();
            // validate
            if (number == null || !number.length > 0) {
                message = 'Debe ingresar un número de suscriptor válido.';
                showAlert('Error', message, 'Aceptar');
                return;
            } else if (number.length < 10) {
                message = 'Debe ingresar un número de suscriptor válido.';
                showAlert('Error', message, 'Aceptar');
                return;
            }
            $('#number').blur();
            self.validateUser(number);
		},

        validateUser: function(number) {
            var self = this;
            self.options.loginModel.validateUser(number,
                function (response) {
                    if(response.hasError){
                        app.removeSession();
                        app.utils.network.errorRequest(response, 200, response.errorDisplay);
                    } else {
                        if (!response.AccountExist) {
                            message = 'Debe ingresar un número de suscriptor válido.';
                            showAlert('Error', message, 'Aceptar');
                        } else {
                            if (response.userExist) {
                                if (response.isGuest) {
                                    self.signOnGuest(number);
                                } else {
                                    self.updateGuestUser(number);
                                }
                            } else {
                                self.registerGuestUser(number);
                            }
                        }
                    }
                },
                app.utils.network.errorRequest
            );
        },

        signOnGuest: function(number) {
            var self = this;
            self.options.loginModel.loginGuest(number,
                function (response) {
                    if(response.hasError) {
                        app.removeSession();
                        if (response.errorDesc == 'device token incorrecto') {
                            self.updateGuestUser(number);
                        } else {
                            app.utils.network.errorRequest(response, 200, response.errorDisplay);
                        }
                    } else {
                        self.onSignonSuccess(response);
                    }
                },
                app.utils.network.errorRequest
            );
        },

        onSignonSuccess: function(response) {

            /**
             * first check if need any update
             */

            var self = this;
            app.utils.Storage.setLocalItem('isLogged', true);
            app.utils.Storage.setLocalItem('logged-subscriber', response.subscriber);
		    app.utils.Storage.setLocalItem('logged-guest', true);
            app.utils.Storage.setSessionItem('token', response.token);

            const loginAccounts = [];
            const postpagoLoginAccounts = [];
            const prepagoLoginAccounts = [];
            const fijoLoginAccounts = [];

            if (response.accounts
                && response.accounts.AccountList
                && response.accounts.AccountList.length > 0) {
                $.each(response.accounts.AccountList, function (i, object) {

                    var postpago = false;
                    var prepago = false;
                    var telefoniaFijo = false;

                    if ((object.accountType == 'I2' && object.accountSubType == '4') ||
                        (object.accountType == 'I' && object.accountSubType == 'R') ||
                        (object.accountType == 'I' && object.accountSubType == '4') ||
                        (object.accountType == 'I' && object.accountSubType == 'E') ) {
                        postpago = true;
                    }
                    if ((object.accountType == 'I' && object.accountSubType == 'P') ||
                        (object.accountType == 'I3' && object.accountSubType == 'P') ){
                        prepago = true;
                    }
                    if (object.accountType == 'I' && object.accountSubType == 'W'){
                        telefoniaFijo = true;
                    }

                    const account = {
                        Account: object.account,
                        DefaultSubscriber: object.subsriberByDefault,
                        mAccountType: object.accountType,
                        mAccountSubType: object.accountSubType,
                        mSubsriberByDefault: object.subsriberByDefault,
                        prodCategory: (object.productType=='G'
                            || object.productType=='C')? 'WLS' : 'WRL',
                        active: object.active,
                        postpago: postpago,
                        prepago: prepago,
                        telefonia: telefoniaFijo
                    };
                    loginAccounts[i] = account;

                    if (postpago) {
                        postpagoLoginAccounts[postpagoLoginAccounts.length] = account;
                    } else if (prepago) {
                        prepagoLoginAccounts[prepagoLoginAccounts.length] = account;
                    } else if (telefoniaFijo) {
                        fijoLoginAccounts[fijoLoginAccounts.length] = account;
                    }
                });
            } else {
                var postpago = false;
                var prepago = false;
                var telefoniaFijo = false;

                if ((response.accountType == 'I2' && response.accountSubType == '4') ||
                    (response.accountType == 'I' && response.accountSubType == 'R') ||
                    (response.accountType == 'I' && response.accountSubType == '4') ||
                    (response.accountType == 'I' && response.accountSubType == 'E') ) {
                    postpago = true;
                }
                if ((response.accountType == 'I' && response.accountSubType == 'P') ||
                    (response.accountType == 'I3' && response.accountSubType == 'P') ){
                    prepago = true;
                }
                if (response.accountType == 'I' && response.accountSubType == 'W'){
                    telefoniaFijo = true;
                }

                const account = {
                    Account: response.account,
                    DefaultSubscriber: response.subscriber,
                    mAccountType: response.accountType,
                    mAccountSubType: response.accountSubType,
                    prodCategory: (response.productType=='G'
                        || response.productType=='C')? 'WLS' : 'WRL',
                    postpago: postpago,
                    prepago: prepago,
                    telefonia: telefoniaFijo
                };
                loginAccounts[0] = account;

                if (postpago) {
                    postpagoLoginAccounts[0] = account;
                } else if (prepago) {
                    prepagoLoginAccounts[0] = account;
                } else if (telefoniaFijo) {
                    fijoLoginAccounts[0] = account;
                }
            }
            app.utils.Storage.setSessionItem('confirmed-password-time',
                app.utils.tools.dateForTimePassword().getDate());

            // accounts
            app.utils.Storage.setSessionItem('accounts-list', loginAccounts);
            app.utils.Storage.setSessionItem('accounts-list-postpago', postpagoLoginAccounts);
            app.utils.Storage.setSessionItem('accounts-list-prepago', prepagoLoginAccounts);
            app.utils.Storage.setSessionItem('accounts-list-telefonia', fijoLoginAccounts);

            self.getAccountDetails(loginAccounts[0],
                function (response) {
                    app.router.navigate('menu',{trigger: true});
                },
                app.utils.network.errorRequest
            );
        },

        registerGuestUser: function(number) {
            var self = this;
            self.options.loginModel.registerGuest(number,
                function (response) {
                    if(response.hasError){
                        app.utils.network.errorRequest(response, 200, response.errorDisplay);
                    } else {
                        app.utils.Storage.setLocalItem('register-number', number);
                        app.utils.Storage.setLocalItem('register-token', response.token);
                        app.utils.Storage.setLocalItem('register-guest-update', false);
                        // navigate to validate account
                        app.router.navigate('signin_guest', {
                            trigger: true
                        });
                    }
                },
                app.utils.network.errorRequest
            );
        },

        updateGuestUser: function(number) {
            var self = this;
            self.options.loginModel.resendGuestCode(number,
                function (response) {
                    if(response.hasError){
                        app.utils.network.errorRequest(response, 200, response.errorDisplay);
                    } else {
                        app.utils.Storage.setLocalItem('register-number', number);
                        app.utils.Storage.setLocalItem('register-token', response.token);
                        app.utils.Storage.setLocalItem('register-guest-update', true);
                        // navigate to validate account
                        app.router.navigate('signin_guest', {
                            trigger: true
                        });
                    }
                },
                app.utils.network.errorRequest
            );
        },

		forgot: function(){
            app.router.navigate('password_step_1', {
                trigger: true
            });
		},

        register: function(){
            app.router.navigate('signin_step_1', {
                trigger: true
            });
		},

	});

});
