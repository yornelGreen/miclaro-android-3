$(function() {

	// Plan Change View
	// ---------------
	
	app.views.ChangePlanTermsView = app.views.CommonView.extend({

		name:'change_plan_terms',
		
		// The DOM events specific.
		events: {
        
        	// evets
            'active': 'active',

            // nav menu
            'click #close-nav':							'closeMenu',
            'click .close-menu':						'closeMenu',
            'click .open-menu':						    'openMenu',
            'click .btn-account':                       'account',
            'click .btn-consumption':                   'consumption',
            'click .btn-service':                       'service',
            'click .btn-change-plan':                   'changePlan',
            'click .btn-add-aditional-data':            'aditionalDataPlan',
            'click .btn-device':                        'device',
            'click .btn-profile':                       'profile',
            'click .btn-gift':                          'giftSend',
            'click .btn-invoice':                       'invoice',
            'click .btn-notifications':	                'notifications',
            'click .btn-sva':                           'sva',
            'click .btn-gift-send-recharge':            'giftSendRecharge',
            'click .btn-my-order':                      'myOrder',
            'click .btn-logout':                        'logout',
            'click .select-menu':						'clickMenu',


			
            'click .btn-back': 'back',
				
		},
		
		// Render the template elements        
		render:function (callback) {
			
			var self = this,
				user = app.utils.Storage.getLocalItem('user'),
				variables = {
                    accountAccess: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: true
				};
			
			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));	
		    	callback();
		    	return this;
		    });					
			
		}
	
	});

});