$(function() {

    // Register step 1 View
    // ---------------

    app.views.AccountsManageView = app.views.CommonView.extend({

        name: 'accounts_manage',

        // The DOM events specific.
        events: {
            // events
            'pagecreate':                           'pageCreate',
        },

        // Render the template elements
        render: function(callback) {
            var self = this,
                variables = {
                    accounts: this.getSelectTabAccounts(),
                    selectedTab: app.utils.Storage.getSessionItem('selected-tab'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    accountSections: this.getUserAccess(),
                    showBackBth: true
                };
            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
        },

        pageCreate: function(e) {
            var self = this;
            self.activateMenu(e);
        },

        next: function(e) {

            // //Go to next
            // app.router.navigate('access_step_3', {
            //     trigger: true
            // });
            showAlert('', 'Actualización de datos completada', 'Ok', function (e) {
                app.router.navigate('login', {
                    trigger: true
                });
            });

        },

    });

});
