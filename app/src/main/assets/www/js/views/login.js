$(function() {

	// Login View
	// ---------------

	app.views.LoginView = app.views.CommonView.extend({

		name:'login',

		// The DOM events specific.
		events: {

            // event
			'pagecreate':                           	'pageCreate',

			// content
			'click #btn-login': 						'login',
			'click #btn-forgot': 						'forgot',
			'click #btn-register': 						'register',
            'click #btn-guest':                         'activeLoginGuest',
            'input #login':                             'userTextSizeChanged',

			// footer
			'click #btn-help':							'helpSection'

		},

		// Render the template elements
		render:function (callback) {

			var outdatedApp = app.utils.Storage.getLocalItem('outdated-app');

			// if app is outdated
			if(outdatedApp != null && outdatedApp){
				navigator.app.exitApp();
			} else {
                var self = this;
				var user = (app.utils.Storage.getLocalItem('user') != null) ? app.utils.Storage.getLocalItem('user') : '',
                    passw = (app.utils.Storage.getLocalItem('password') != null) ? app.utils.Storage.getLocalItem('password') : '',
                    variables = {
                    	login: (user != null)? user: '',
	                    password: passw,
    	                remember: (app.utils.Storage.getLocalItem('remember') !== null),
        	        };

                var navegationPath = app.utils.Storage.getSessionItem('navegation-path');

				//clear session
				app.utils.Storage.clearSessionStorage();

				// set navegation path
                app.utils.Storage.setSessionItem('navegation-path', navegationPath);

				//Delete Register Data
				app.utils.Storage.removeSessionItem('register-data');
				app.utils.Storage.removeSessionItem('register-alert');

				app.TemplateManager.get(self.name, function(code){
			    	var template = cTemplate(code.html());
			    	$(self.el).html(template(variables));

			    	callback();
			    	return this;
			    });
			}

		},

		pageCreate: function(){
            var self = this;
		    // removing any enter event
            $('body').unbind('keypress');
            /**
             * if is logged
             */
            const isLogged = app.utils.Storage.getLocalItem('isLogged');
            if (isLogged) {
                const isGuest = app.utils.Storage.getLocalItem('logged-guest');
                if (isGuest) {
                    app.router.navigate('login_guest', {
                        trigger: true
                    });
                } else {
                    const username = app.utils.Storage.getLocalItem('username');
                    const password = app.utils.Storage.getLocalItem('password');
                    self.signOn(username, password);
                }
            } else {
                var loginGuest = true;
                var loginModeGuest = app.utils.Storage.getLocalItem('loginModeGuest');
                if (loginModeGuest != null) {
                    loginGuest = loginModeGuest;
                }
                if (loginGuest) {
                    app.router.navigate('login_guest', {
                        trigger: true
                    });
                }
            }

            /**
             * set enter event
             */
			$('body').on('keypress', function(e){
				if (e.which === 13 || e.keyCode === 13) {
			    	self.login();
			    }
			});

            $('#login').on('click focus', function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $('#login').offset().top-20
                }, 1000);
            });

            $('#password').on('click focus', function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $('#login').offset().top-20
                }, 1000);
            });
		},

        userTextSizeChanged: function() {
            var user = $.mobile.activePage.find('#login').val();

            if (user.length > 40) {
                user = user.slice(0,40);
                $.mobile.activePage.find('#login').val(user);
            }
        },

        help: function(){
            app.router.navigate('help', {
                trigger: true
            });
        },

        activeLoginGuest: function(){
            app.utils.Storage.setLocalItem('loginModeGuest', true);
            app.router.navigate('login_guest', {
                trigger: true
            });
        },

		login: function(){
            var self = this;
            var username = $.mobile.activePage.find('#login').val();
            username = username.trim();
            var password = $.mobile.activePage.find('#password').val();
            password = password.trim();

            /**
             * validate
             */
            if(!username.length > 0){
                showAlert('Error', 'Debe ingresar su usuario y contraseña.', 'Aceptar');
                return;
            } else if(!password.length > 0){
                showAlert('Error', 'Debe ingresar su usuario y contraseña.', 'Aceptar');
                return;
            }
            /**
             * password encrypt
             */
            // const cipherPassword = window.aes(password, function(cipherText){
            //     return cipherText;
            // });

            /**
             * call login service
             */
            $('#login').blur();
            $('#password').blur();
            self.signOn(username, password); // TODO, change to cipherPassword
		},

        openChat: function(account, subscriber, question) {
		    var url = 'https://chat2.claropr.com/webapiserver/ECSApp/ChatWidget3/ChatPanel.aspx';
            url += '?BAN='+account;
            url += '&subcriptor='+subscriber;
            url += '&Department=2';
            url += '&firstname=Error';
            url += '&lastname=Acceso';
            url += '&Question='+question;

            console.log(url);
            var browser = app.utils.browser.show(url, true);

            app.utils.loader.show();

            // success event load url
            browser.addEventListener('loadstop', function(e) {

                // hiden loader
                app.utils.loader.hide();

                // show navegator
                browser.show();
            });
        },

        signOn: function(username, password) {
            var self = this;
            self.options.loginModel.login(
                username,
                password,
                function (response) {
                    if(response.hasError) {
                        self.removeSession();
                        if (response.errorNum > 0) {
                            showConfirm(
                                'Error',
                                response.errorDisplay,
                                ['Ir a asistencia', 'Salir'],
                                function(btnIndex){
                                    if(btnIndex==1){
                                        var question = 'Error%20de%20acceso';
                                        if (response.errorNum == 30) {
                                            question = 'Error%20de%20contraseña';
                                        } else if (response.errorNum == 32) {
                                            question = 'Usuario%20Bloqueado';
                                        }
                                        self.openChat(response.account, response.subscriber, question)
                                    }
                                });
                        } else {
                            app.utils.network.errorRequest(response, 200, response.errorDisplay);
                        }
                    } else {
                        self.checkRequiredUpdatePassword(response, username, password);
                    }
                },
                app.utils.network.errorRequest
            );
        },

        checkRequiredUpdatePassword: function(response, username, password) {
            if (response.requiredPasswordReset) {
                app.utils.Storage.setSessionItem('token', response.token);
                app.router.navigate('change_password', {
                    trigger: true
                });
                return;
            } else {
                this.checkRequiredUpdateAccount(response, username, password);
            }
        },

        checkRequiredUpdateAccount: function(response, username, password) {
            if (response.requiredAccountUpdate) {
                $('.popup-update-account').show();
                return;
            } else {
                this.checkRequiredUpdateEmail(response, username, password);
            }
        },

        checkRequiredUpdateEmail: function(response, username, password) {
            if (response.requiredEmailUpdate) {
                this.showPopupUpdateEmail(response, username, password);
                return
            } else {
                this.checkRequiredUpdatePaperless(response, username, password);
            }
        },

        checkRequiredUpdatePaperless: function(response, username, password) {
            if (response.requiredPaperless) {
                this.showPopupUpdatePaperless(response, username, password);
                return
            } else {
                this.checkRequiredUpdateQuestions(response, username, password);
            }
        },

        checkRequiredUpdateQuestions: function(response, username, password) {
            if (response.requiredQuestions) {
                showAlert('Actualización de Datos', 'El sistema requiere que actualize sus preguntas de seguridad.', 'Aceptar');
                return
            } else {
                this.onSignonSuccess(response, username, password);
            }
        },

        onSignonSuccess: function(response, username, password) {
            var self = this;

            app.utils.Storage.setSessionItem('token', response.token);
            app.utils.Storage.setLocalItem('isLogged', true);
            app.utils.Storage.setLocalItem('logged-subscriber', response.subscriber);
		    app.utils.Storage.setLocalItem('logged-guest', response.guest);
		    app.utils.Storage.setLocalItem('username', username);
		    app.utils.Storage.setLocalItem('password', password);

            const loginAccounts = [];
            const postpagoLoginAccounts = [];
            const prepagoLoginAccounts = [];
            const fijoLoginAccounts = [];

            if (response.accounts
                && response.accounts.AccountList
                && response.accounts.AccountList.length > 0) {
                $.each(response.accounts.AccountList, function (i, object) {

                    var postpago = false;
                    var prepago = false;
                    var telefoniaFijo = false;

                    if ((object.accountType == 'I2' && object.accountSubType == '4') ||
                        (object.accountType == 'I' && object.accountSubType == 'R') ||
                        (object.accountType == 'I' && object.accountSubType == '4') ||
                        (object.accountType == 'I' && object.accountSubType == 'E') ) {
                        postpago = true;
                    }
                    if ((object.accountType == 'I' && object.accountSubType == 'P') ||
                        (object.accountType == 'I3' && object.accountSubType == 'P') ){
                        prepago = true;
                    }
                    if (object.accountType == 'I' && object.accountSubType == 'W'){
                        telefoniaFijo = true;
                    }

                    const account = {
                        Account: object.account,
                        DefaultSubscriber: object.subsriberByDefault,
                        mAccountType: object.accountType,
                        mAccountSubType: object.accountSubType,
                        mProductType: object.productType,
                        prodCategory: (object.productType=='G'
                            || object.productType=='C')? 'WLS' : 'WRL',
                        active: object.active,
                        postpago: postpago,
                        prepago: prepago,
                        telefonia: telefoniaFijo
                    };
                    loginAccounts[i] = account;

                    if (postpago) {
                        postpagoLoginAccounts[postpagoLoginAccounts.length] = account;
                    } else if (prepago) {
                        prepagoLoginAccounts[prepagoLoginAccounts.length] = account;
                    } else if (telefoniaFijo) {
                        fijoLoginAccounts[fijoLoginAccounts.length] = account;
                    }
                });
            } else {
                var postpago = false;
                var prepago = false;
                var telefoniaFijo = false;

                if ((response.accountType == 'I2' && response.accountSubType == '4') ||
                    (response.accountType == 'I' && response.accountSubType == 'R') ||
                    (response.accountType == 'I' && response.accountSubType == '4') ||
                    (response.accountType == 'I' && response.accountSubType == 'E') ) {
                    postpago = true;
                }
                if ((response.accountType == 'I' && response.accountSubType == 'P') ||
                    (response.accountType == 'I3' && response.accountSubType == 'P') ){
                    prepago = true;
                }
                if (response.accountType == 'I' && response.accountSubType == 'W'){
                    telefoniaFijo = true;
                }

                const account = {
                    Account: response.account,
                    DefaultSubscriber: response.subscriber,
                    mAccountType: response.accountType,
                    mAccountSubType: response.accountSubType,
                    mProductType: response.productType,
                    prodCategory: (response.productType=='G'
                        || response.productType=='C')? 'WLS' : 'WRL',
                    postpago: postpago,
                    prepago: prepago,
                    telefonia: telefoniaFijo
                };
                loginAccounts[0] = account;

                if (postpago) {
                    postpagoLoginAccounts[0] = account;
                } else if (prepago) {
                    prepagoLoginAccounts[0] = account;
                } else if (telefoniaFijo) {
                    fijoLoginAccounts[0] = account;
                }
            }
            app.utils.Storage.setSessionItem('confirmed-password-time',
                app.utils.tools.dateForTimePassword().getDate());

            // accounts
            app.utils.Storage.setSessionItem('accounts-list', loginAccounts);
            app.utils.Storage.setSessionItem('accounts-list-postpago', postpagoLoginAccounts);
            app.utils.Storage.setSessionItem('accounts-list-prepago', prepagoLoginAccounts);
            app.utils.Storage.setSessionItem('accounts-list-telefonia', fijoLoginAccounts);

            var defaultAccount = loginAccounts[0];
            for (var i = 0; i < loginAccounts.length; i++) {
                if (loginAccounts[i].postpago) { // la primera cuenta postpago que se encuentre
                    defaultAccount = loginAccounts[i];
                    i = loginAccounts.length; // para salir del for
                }
            }

            self.getAccountDetails(defaultAccount,
                function () {
                    app.router.navigate('menu',{trigger: true}); // TODO, correcto
                    //self.myServices(); // TODO, borrar
                },
                app.utils.network.errorRequest
            );
        },

        showPopupUpdateEmail: function(response, username, password) {
            var self = this;
            $('#old-email').val(response.email);
            $('.popup-update-email').show();
            $('#cancel-update-email').click(function () {
                $('.popup-update-email').hide();
                self.checkRequiredUpdatePaperless(response, username, password);
            });
        },

        showPopupUpdatePaperless: function(response, username, password) {
            var self = this;
            $('#radio-paperless-y').prop('checked', true);
            $('.popup-update-paperless').show();
            $('#cancel-update-paperless').click(function () {
                $('.popup-update-paperless').hide();
                self.checkRequiredUpdateQuestions(response, username, password);
            });
        },

		forgot: function(){
            app.router.navigate('password_step_1', {
                trigger: true
            });
		},

        register: function(){
            app.router.navigate('signin_step_1', {
                trigger: true
            });
		},
	});

});
