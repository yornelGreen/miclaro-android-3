$(function() {

    // Plan Change View
    // ---------------

    app.views.ChangePlanAdslView = app.views.CommonView.extend({

        name:'change_plan_adsl',

        // The DOM events specific.
        events: {

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            // nav menu
            'click #close-nav':							'closeMenu',
            'click .close-menu':						'closeMenu',
            'click .open-menu':						    'openMenu',
            'click .btn-account':                       'account',
            'click .btn-consumption':                   'consumption',
            'click .btn-service':                       'service',
            'click .btn-change-plan':                   'changePlan',
            'click .btn-add-aditional-data':            'aditionalDataPlan',
            'click .btn-device':                        'device',
            'click .btn-profile':                       'profile',
            'click .btn-gift':                          'giftSend',
            'click .btn-invoice':                       'invoice',
            'click .btn-notifications':	                'notifications',
            'click .btn-sva':                           'sva',
            'click .btn-gift-send-recharge':            'giftSendRecharge',
            'click .btn-my-order':                      'myOrder',
            'click .btn-logout':                        'logout',
            'click .select-menu':						'clickMenu',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render:function (callback) {

            var reponseCode = app.utils.Storage.getSessionItem('change-plan-response-code');
            var orderNumber = app.utils.Storage.getSessionItem('change-plan-order-number');
            var description = app.utils.Storage.getSessionItem('change-plan-description');
            var application = app.utils.Storage.getSessionItem('soc-application');

            // remove 'moths' for the text
            application = application.replace(/months/g,'');

            var self = this,
                user = app.utils.Storage.getLocalItem('user'),
                actualCicle = app.utils.Storage.getSessionItem('actual-cicle-date'),
                nextCicle = app.utils.Storage.getSessionItem('next-cicle-date'),
                activationDate = (app.utils.Storage.getSessionItem('soc-application') === 'now') ? actualCicle : nextCicle,

                variables = {
                    showBackBth: false,
                    socCode: app.utils.Storage.getSessionItem('soc-code'),
                    socDesc: app.utils.Storage.getSessionItem('soc-desc'),
                    socRent: this.formatNumber(app.utils.Storage.getSessionItem('soc-rent')),
                    socDetails: app.utils.Storage.getSessionItem('soc-details'),
                    activationDate: activationDate,
                    wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS'),
                    orderNumber: orderNumber,
                    reponseCode: reponseCode,
                    description: description,
                    showDescription: reponseCode == '003' || reponseCode == '004',
                    showOrderNumber: orderNumber !== '000000',
                    application: application,
                    accountAccess: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: true
                };

            // delete from session
            app.utils.Storage.removeSessionItem('actual-cicle-date');
            app.utils.Storage.removeSessionItem('next-cicle-date');
            app.utils.Storage.removeSessionItem('soc-code');
            app.utils.Storage.removeSessionItem('soc-desc');
            app.utils.Storage.removeSessionItem('soc-rent');
            app.utils.Storage.removeSessionItem('soc-details');
            app.utils.Storage.removeSessionItem('soc-application');
            app.utils.Storage.removeSessionItem('change-plan-response-code');
            app.utils.Storage.removeSessionItem('change-plan-order-number');
            app.utils.Storage.removeSessionItem('change-plan-description');
            app.utils.Storage.removeSessionItem('actual-cicle-date');
            app.utils.Storage.removeSessionItem('change-plan-contract');


            var subscriberIndex = null;

            $.each(app.utils.Storage.getSessionItem('subscribers'), function (index, value) {
                if (value.subscriber == app.utils.Storage.getSessionItem('selected-subscriber-value')) {
                    subscriberIndex = index;
                }
            });

            var subscribers = app.utils.Storage.getSessionItem('subscribers');
            subscribers[subscriberIndex].DSLQualification.HasError = true ;
            subscribers[subscriberIndex].DSLQualification.ErrorDesc = 'El número ingresado refleja una orden de servicio pendiente. Si desea más información por favor utilice nuestro Chat.';

            // update session subscribers
            app.utils.Storage.setSessionItem('subscribers', subscribers);

            app.TemplateManager.get(self.name, function(code){
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));

                callback();
                return this;
            });

        }

    });

});
