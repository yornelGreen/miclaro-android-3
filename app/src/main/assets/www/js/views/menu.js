$(function() {

	// Menu View
	// ---------------
	
	app.views.MenuView = app.views.CommonView.extend({

		name:'menu',
				
		// The DOM events specific.
		events: {
        	
            // events
        	'active':                                       'active',
        	'pagecreate':                                   'pageCreate',

        	'show.bs.popover div[data-toggle="popover"]':   'showPopOver',
            'change #select-account':					    'changeAccount',
            'click #btn-bill': 					            'billPayment',
            'click #btn-apply': 					        'applyCredits',
            'click .dashdirect-chat':                       'chat',
            'click #paperless-switch':                      'changePaperless',

            // toggle
            'click .sectbar':							    'toggleClass',
            'click .phonebar':							    'toggleClass',

            'click .select-subscriber':					    'changeSubscriber',

            // Home menu
            'click .dash-refer':					        'referSystem',
            'click .dash-digital-bill':				        'electronicBill',
            'click .dash-account':				            'accountsManage',


		},
		
		// Render the template elements
		render:function (callback) {
			var self = this,
				variables = {};

			// user hasn't logged in
			if(app.utils.Storage.getSessionItem('token') == null){
                document.location.href = 'index.html';
			} else {

			    var selectedAccount = app.utils.Storage.getSessionItem('selected-account');
                var accountInfo = app.utils.Storage.getSessionItem('account-info');
                var qualification = app.utils.Storage.getSessionItem('qualification');

                var amountDue = accountInfo.billBalanceField.indexOf('CR') >= 0 ? accountInfo.billBalanceField : accountInfo.pastDueAmountField;
                amountDue = accountInfo.billBalanceField.indexOf('CR') >= 0 ? amountDue : parseFloat(String(amountDue)).toFixed(2);
                amountDue = app.utils.tools.formatNumber(amountDue);
                var latPaymentAmount = accountInfo.lastPaymentAmountField;
                latPaymentAmount = parseFloat(String(latPaymentAmount)).toFixed(2);
                latPaymentAmount = app.utils.tools.formatNumber(latPaymentAmount);
                var amountPayable = accountInfo.billBalanceField.indexOf('CR') >= 0 ? 0 : accountInfo.pastDueAmountField;
                amountPayable = parseFloat(String(amountPayable)).toFixed(2);
                var redeemProgram = qualification.RefererResponse.registerUpdated && qualification.RefererResponse.paperless;
                var paperless = accountInfo.paperlessField;

                variables = {
                    selectedTab: app.utils.Storage.getSessionItem('selected-tab'),
                    requiredAssociate: app.utils.Storage.getSessionItem('required-associate-account'),
                    accounts: this.getSelectTabAccounts(),
                    selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                    selectedAccount: selectedAccount,
                    wirelessAccount: selectedAccount.prodCategory === 'WLS',
                    name: accountInfo.firstNameField,
                    billDate: accountInfo.billDateField,
                    billDueDate: accountInfo.billDueDateField,
                    lastPaymentDate: accountInfo.lastPaymentDateField,
                    amountDue: amountDue,
                    latPaymentAmount: latPaymentAmount,
                    amountPayable: amountPayable,
                    redeemProgramAvailable: redeemProgram,
                    paperless: paperless,
                    currentSubscribers: app.utils.Storage.getSessionItem('subscribers-info'),
                    referralSystemEnabled: true,
					guest: app.utils.Storage.getLocalItem('logged-guest'),
                    typeOfTelephony: app.utils.tools.typeOfTelephony,
                    account: app.utils.Storage.getSessionItem('account'),
                    access: app.utils.Storage.getSessionItem('access-list'),
                    accountSections: this.getUserAccess(),
                    convertCaseStr: app.utils.tools.convertCase,
                    showBackBth: false,
                };
				
				app.TemplateManager.get(self.name, function(code){
			    	var template = cTemplate(code.html());
			    	$(self.el).html(template(variables));
			    	callback();	
			    	return this;
			    });			
				
			}
			$(document).scrollTop();
		},

		pageCreate: function(e) {
            var self = this;
            // removing any enter event
            $('body').unbind('keypress');
            self.activateMenu(e);

            // enable tooltips
			$('[data-toggle="popover"]').popover({
				animation: false
			});

			$.mobile.activePage.on("show.bs.popover", "#popover-payment", function() {
			    setTimeout(function(){
					$.mobile.activePage.find('[data-toggle="popover"]').popover('hide');
				}, 3000);
			});
			
			// allow only number and dot
			$.mobile.activePage.on('input', '#due-amount', function() {
			    this.value = this.value.replace(/[^\d\.]/g, '') //Replace non-numeric with ''
									    .replace(/\./, 'x') //Replace first . with x
									    .replace(/\./g, '') //Replace remaining . with ''
									    .replace(/x/, '.'); //Replace x with .
			});

			// allow only number
            $.mobile.activePage.on("show.bs.popover", "#popover-password", function() {
                setTimeout(function(){
                       $.mobile.activePage.find('[data-toggle="popover"]').popover('hide');
                },4000);
            });
			
			// remove when amount is zero 
			$.mobile.activePage.on('focus', '#due-amount', function() {
				if(this.value==0) {
					this.value = '';
				}
			});		
			
			// add zero 
			$.mobile.activePage.on('focusout', '#due-amount', function() {
				if(this.value=='') {
					this.value = '0.00';
				}
			});		
			
            if (app.utils.Storage.getSessionItem('selected-account-is-suspend')) {
            	
                showConfirm(
                	'Alerta',
                    'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.',
                    ['Cancelar', 'Pagar'],
                    function(button){
                    
	                    // var accountModel = new app.models.Account();
                        // update suspend account value
                        app.utils.Storage.setSessionItem('suspend-account', true);
                    
    	                if(button==2) {
                            self.navigateInvoiceSummary();
                        }
                    
                    });

            }
            
            var gifts = app.utils.Storage.getSessionItem('gifts'),
        		newGift = null;            
        
	        // gift            
	        if(gifts !== null) {
	        	
	            for (var i=0 ; i<gifts.length ; i++) {
	            	
	            	if (gifts[i].proccess !== true) {
	
	            		newGift = gifts[i];
	            		
	            		// mark as proccessed
	            		gifts[i].proccess = true;
	            		
	            		// update value
	            		app.utils.Storage.setSessionItem('gifts', gifts);
	
	            		// escape from loop
	            		break;
	            	}
	            }
	        	
	            // check if exists a new gift
	            if (newGift != null)  {
	            	// set item
	            	app.utils.Storage.setSessionItem('gift', newGift);
	            	
	            	//goto gift
					app.router.navigate('gift_accept',{trigger: true});                	
	            }
	        	
	        }
	        /* obtain credits for user */
            self.getUserCredits();
		},

        getUserCredits() {
            var self = this;
            var selectedAccountValue = app.utils.Storage.getSessionItem('selected-account-value');
            self.options.referrerModel.getCredits(selectedAccountValue,
                function (success) {
                    if (!success.hasError) {

                        var sumAvialable = '$0';
                        if (success.PayOutsDetailsItems.length > 0) {
                            sumAvialable = success.PayOutsDetailsItems[0].SumAvialable;
                        }

                        $('#sumAvialable').html(sumAvialable+'');
                    } else {
                        showAlert('Error', response.errorDisplay, 'Aceptar');
                    }
                },
                app.utils.network.errorRequest
            );
        },

		billPayment: function(e){
			
			var self = this, 
				browser = null,
				dueAmount = parseFloat($('#due-amount').val()),
	            selectedAccount = app.utils.Storage.getSessionItem('selected-account'),
				accountsBillInfo = app.utils.Storage.getSessionItem('accounts-bill-info'),
	            creditAmtDue = 0;
		
			if(!$.isNumeric(dueAmount)){
				showAlert('Error','El monto a pagar no es un número válido.','Aceptar');
				return;
			} else if (dueAmount < 5) {
				showAlert('Error','El monto no puede ser menor a $5.00','Aceptar');
				return;
			} else if (dueAmount > 500) {
				showAlert('Error','El monto no puede ser mayor a $500.00','Aceptar');
				return;			
			} else if(parseFloat(selectedAccount.AmtDue) < 0){
					//creditAmtDue = '$' + Math.abs(selectedAccount.AmtDue) + 'CR';
					creditAmtDue = Math.abs(selectedAccount.AmtDue);
					
					if ((creditAmtDue + dueAmount) >= 500) {
						showAlert('Error','La cantidad a pagar no puede ser mayor a $500.00','Aceptar');
						return;
					}
					
					self.doPayment();				
			} else if (parseFloat(dueAmount) > parseFloat(selectedAccount.AmtDue)) {
				showConfirm(
						'Confirmación',
						'La cantidad ingresada es mayor al balance de su factura, la diferencia será acreditada a su cuenta.',
						['Cancelar','Pagar'], 
						function(button){
							if(button == 2) {
								self.doPayment();
							}							
						}
					);
			} else {
				self.doPayment();	
			}
			
			return false;
			
		},

        changePaperless: function(e) {
            var self = this;

            var check = $('#paperless-switch').is(':checked');
            if (check) {
                setTimeout(function() {
                    var account = app.utils.Storage.getSessionItem('selected-account-value');
                    self.options.customerModel.updateBillParameters(account,
                        function (success) {
                            if (!success.HasError) {
                                var selectedAccount = app.utils.Storage.getSessionItem('selected-account');
                                self.getAccountDetails(selectedAccount,
                                    function (response) {
                                        self.render(function(){
                                            $.mobile.activePage.trigger('pagecreate');
                                        });
                                    },
                                    app.utils.network.errorRequest
                                );
                            } else {
                                $('#paperless-switch').prop('checked', false);
                                showAlert('Error', response.ErrorDesc, 'Aceptar');
                            }
                        },
                        function (data, status) {
                            $('#paperless-switch').prop('checked', false);
                            app.utils.network.errorRequest(data, status);
                        },
                    );
                }, 500);
            }
        },
        
		invoice: function(e){
            var self = this;

			self.options.accountModel.getAccountBill(
				//parameters
				app.utils.Storage.getSessionItem('token'),
				app.utils.Storage.getSessionItem('selected-account-value'),

				//success callback
				function(data){

					if(!data.HasError || data.Desc.toLowerCase().search('pdf') > 0){

						// account info
						app.utils.Storage.setSessionItem('accounts-bill-info', data);

						app.router.navigate('invoice', {trigger: true});

					}else{

						var message = 'En este momento no está disponible esta factura';

						showAlert('Error', message, 'Aceptar');

					}

				},

				// error function
				app.utils.network.errorFunction
			);

			return false;
		},
		
		changeAccount: function(e){

			var self = this,
				analytics = null;

            const newAccountNumber = $.mobile.activePage.find('#select-account').val();
            const accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            const accountList = app.utils.Storage.getSessionItem('accounts-list');

            var selectAccount = null;
            $.each(accountList, function (i, object) {
            	if (object.Account == newAccountNumber) {
                    selectAccount = object;
				}
            });

            //showAlert('Cambio de Cuenta', selectAccount.mSubsriberByDefault + " " +selectAccount.Account, 'Aceptar');

			self.getAccountDetails(selectAccount,
                function (response) {
                    if(analytics != null ){
                        // send GA statistics
                        analytics.trackEvent('select', 'change', 'select account number', accountNumber);
                    }
                    self.render(function(){
                        $.mobile.activePage.trigger('pagecreate');
                    });
                },
                function (message, status) {
                    if (status == 404) {
                        showAlert('Error', 'Verifique su conexi&#243;n de internet.', 'Aceptar');
                    } else {
                        showAlert('Error', message, 'Aceptar');
                    }
                });
		},

	    percentUsageAsWidth: function(usageRatio){
        	var percentage = 100.0 * usageRatio;
            return parseInt(Math.round(percentage)) + '%';
        },

        getSubscriber: function (subscriberValue) {
            var subscribers = app.utils.Storage.getSessionItem('subscribers');

            for (var i = 0; i < subscribers.length; i++) {
                if (subscribers[i].subscriber == subscriberValue) {
                    return subscribers[i];
                }
            }
        },

		changeSubscriber: function(e){
            var subscriberValue = $(e.currentTarget).data('subscriber'),
                currentIndex = $(e.currentTarget).data('index'),
                subscribers = app.utils.Storage.getSessionItem('subscribers'),
                wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS'),
                mainPlan,
                self = this;

            var accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            if(app.utils.Storage.getSessionItem(accountNumber) == null) {

                this.options.accountModel.getAccountInfo(
                    //parameter
                    app.utils.Storage.getSessionItem('token'),

                    accountNumber,

                    //success callback
                    function(data){

                        if(!data.HasError){

                            // session
                            app.utils.Storage.setSessionItem('selected-account',data);

                            // cache
                            //app.cache.Accounts[accountNumber] = data;
                            app.utils.Storage.setSessionItem(accountNumber, data);

                            var subscribers = data.Subscribers;
                            app.utils.Storage.setSessionItem('subscribers', subscribers);
                            app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);

                            // cache
                            app.utils.Storage.setSessionItem('subscribers-'+accountNumber, subscribers);

                            $.each(subscribers, function (index, subscriber) {
                                if (subscriber.subscriber == subscriberValue) {
                                    activeSubscriber = subscriber;
                                    usageActiveSubscriber = activeSubscriber.UsageOfferDataList;
                                    return;
                                }
                            });

                            if($(e.currentTarget).data('search-info') == true){
                                // set flag search data
                                $(e.currentTarget).data('search-info', false);
                            } else {
                                // set flag search data
                                $(e.currentTarget).data('search-info', true);

                                self.options.accountModel.getAccountUsage(

                                    //parameters
                                    app.utils.Storage.getSessionItem('token'),
                                    app.utils.Storage.getSessionItem('selected-account-value'),
                                    subscriberValue,

                                    //success callback
                                    function (data) {

                                        if (!data.HasError) {

                                            var html = '';

                                            if (app.utils.tools.typeOfTelephony(subscribers[0].ProductType)=="Móvil") {

                                                var init = moment(data.cicleDateStart, 'MM/DD/YYYY'),
                                                    end = moment(data.cicleDateEnd, 'MM/DD/YYYY'),
                                                    totalDays = end.diff(init, 'days'),
                                                    cicleDaysPast = moment().diff(init, 'days'),
                                                    cicleDaysLeft = end.diff(moment(), 'days');

                                                    data.porcDaysPast = (cicleDaysPast * 100) / totalDays;
                                                    data.cicleDaysLeft = cicleDaysLeft;

                                                // update usage
                                                app.utils.Storage.setSessionItem('usage', data);

                                                mainPlan = usageActiveSubscriber[0];

                                                // find subscriber
                                                var subscriber = self.getSubscriber(subscriberValue);

                                                // update the subscriber
                                                app.utils.Storage.setSessionItem('selected-subscriber-value', subscriber);

                                                if(usageActiveSubscriber.length > 0){
                                                    var firstOffer = usageActiveSubscriber.shift();
                                                    var mainUsageRatio = firstOffer.Used / firstOffer.Quota,
                                                        percentage = 100.0 * mainUsageRatio,
                                                        widthSize = parseInt(Math.round(percentage)) + '%';
                                                }

                                                html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'PLAN'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="autobar diff-i">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow m-top">';
                                                            if (mainPlan != '') {
                                                                html += '<div class="contspace full f-med f-black text-justify f-bold">'
                                                                    + mainPlan.DisplayName +
                                                                '</div>';
                                                            }else{
                                                                html += '<div class="contspace full f-med f-black text-justify f-bold">'
                                                                    + data.DPlanName +
                                                                '</div>';
                                                            }
                                                        html += '</div>'+

                                                        '<div class="g-btn text-center f-med f-white vcenter btn-change-plan">'+
                                                            '<div class="tabcell">'+
                                                                'Cambiar Plan'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'CONSUMO'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow text-center">'+
                                                            '<span class="f-little f-dgray din-b">Ciclo de Facturaci&oacute;n</span><br/>'+
                                                            '<span class="f-med f-black din-b">'+ data.CicloFact +'</span><br/>'+
                                                            '<div class="c100 p'+Math.round(data.porcDaysPast)+' text-center center vcenter">'+
                                                                '<span class="change f-black">'+
                                                                    '<span class="f-little">Quedan</span>'+
                                                                    '<span class="f-big-m din-b">'+ data.cicleDaysLeft +'</span>'+
                                                                    '<span class="f-little">D&iacute;as</span>'+
                                                                '</span>'+
                                                              '<div class="slice">'+
                                                                '<div class="bar"></div>'+
                                                                '<div class="fill"></div>'+
                                                              '</div>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow f-little din-b f-gray">Consumo Actual</div>';

                                                        if (mainPlan != '') {

                                                            var mainUsageRatio = mainPlan.Used / mainPlan.Quota;

                                                            html += '<div class="basicrow m-top-ii">'+
                                                                '<div class="contspace full f-med f-dgray text-justify f-bold">'
                                                                    + mainPlan.UsedText + ' de ' + mainPlan.QuotaText + ' Usados'+
                                                                '</div>'+
                                                            '</div>'+

                                                            '<div class="basicrow m-top-ii">'+
                                                                '<div class="consmpt-bar">';
                                                                     /*if (parseInt(subscriber.PUJ_USED) >= parseInt(subscriber.PUJ_LIMIT)) {
                                                                         html += '<div class="consmpt-in data-progress-bar" style="width:75%"></div>';
                                                                     } else {
                                                                         html += '<div class="consmpt-in data-progress-bar" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                                     }*/
                                                                     if (mainUsageRatio <= 0.8) {
                                                                         html += '<div class="consmpt-green" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                                     } else if (mainUsageRatio > 0.8 && mainUsageRatio < 0.9)  {
                                                                         html += '<div class="consmpt-yellow" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                                     } else if (mainUsageRatio >= 0.9 && mainUsageRatio < 1.0) {
                                                                         html += '<div class="consmpt-in " style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                                     } else if (mainUsageRatio > 1.0 ) {
                                                                         mainUsageRatio = 1.0;
                                                                         html += '<div class="consmpt-in" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                                     }
                                                                html += '</div>'+
                                                            '</div>';
                                                        } else {

                                                            '<div class="basicrow m-top-ii">'+
                                                                '<div class="contspace full f-med f-dgray text-justify f-bold">'
                                                                    + data.DLocal + ' de ' + data.DLoaclTop + ' Usados'+
                                                                '</div>'+
                                                            '</div>';

                                                            var mainUsageRatio = mainPlan.Used / mainPlan.Quota;

                                                            html += '<div class="basicrow m-top-ii">'+
                                                                '<div class="consmpt-bar">';
                                                                     /*if (parseInt(subscriber.PUJ_USED) >= parseInt(subscriber.PUJ_LIMIT)) {
                                                                         html += '<div class="consmpt-in data-progress-bar" style="width:75%"></div>';
                                                                     } else {
                                                                         html += '<div class="consmpt-in data-progress-bar" style="width: '+ data.DLoaclPorc.toString() +'%' +'"></div>';
                                                                     }*/
                                                                     if (mainUsageRatio <= 0.8) {
                                                                          html += '<div class="consmpt-green" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                                     } else if (mainUsageRatio > 0.8 && mainUsageRatio < 0.9)  {
                                                                          html += '<div class="consmpt-yellow" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                                     } else if (mainUsageRatio >= 0.9 && mainUsageRatio < 1.0) {
                                                                          html += '<div class="consmpt-in" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                                     } else if (mainUsageRatio > 1.0 ) {
                                                                          mainUsageRatio = 1.0;
                                                                          html += '<div class="consmpt-in" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                                     }
                                                                html += '</div>'+
                                                            '</div>';
                                                        }

                                                    html += '</div>'+
                                                '</div>'+

                                                '<div class="autobar diff-i">'+
                                                    '<div class="container">'+
                                                        '<div class="r-btn text-center f-med f-white vcenter btn-consumption" data-subscriber="' + subscriber.subscriber + '">'+
                                                            '<div class="tabcell" >'+
                                                                'Ver Consumo'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';

                                            } else {
                                                //html += '<div class="nbott din-b f-little text-center f-red m-error">Informaci&oacute;n no disponible para servicios de telefon&iacute;a fija.</div>';
                                                var html = '';

                                                if (mainPlan != undefined) {

                                                    html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                            '<div class="tabcell">'+
                                                                'PLAN'+
                                                            '</div>'+
                                                        '</div>'+

                                                        '<div class="autobar diff-i">'+
                                                            '<div class="container">'+
                                                                '<div class="basicrow m-top">';
                                                                        html += '<div class="contspace full f-med f-black text-left f-bold">'
                                                                        + mainPlan.DisplayName +
                                                                          '</div>';
                                                        html += '</div>';

                                                        // validate if exits dsl order
                                                        if(subscribers[0].DSLQualification !== undefined && subscribers[0].DSLQualification.dslOrder){
                                                            html += '<div class="g-btn text-center f-med f-white vcenter btn-change-plan">'+
                                                                        '<div class="tabcell">'+
                                                                          'Cambiar Plan'+
                                                                        '</div>'+
                                                                    '</div>';
                                                        }

                                                        html +='</div>'+
                                                            '</div>';

                                                }

                                                html += '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow f-little din-b f-gray">Minutos Usados</div>'+

                                                        '<div class="basicrow m-top-ii">'+
                                                            '<div class="contspace full f-med f-black text-justify f-bold">'+
                                                                + data.MinutesCount +
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow f-little din-b f-gray">Larga Distancia</div>'+

                                                        '<div class="basicrow m-top-ii">'+
                                                            '<div class="contspace full f-med f-black text-justify f-bold">'+
                                                                + data.LDQty +
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow f-little din-b f-gray">Larga Distancia Internacional</div>'+

                                                        '<div class="basicrow m-top-ii">'+
                                                            '<div class="contspace full f-med f-black text-justify f-bold">'+
                                                                + data.LDQty +
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';
                                            }

                                            $('#phone'+currentIndex).html(html);

                                        } else {
                                            showAlert('Error', data.Desc, 'Aceptar');
                                        }

                                    },

                                    // error function
                                    app.utils.network.errorFunction
                                );

                            }

                        } else {

                            showAlert('Error', data.Desc, 'Aceptar');

                        }

                    },

                    // error function
                    app.utils.network.errorFunction
                );


            } else {

                // ************************************************************
                // *************** CHANGE WITH A FUNCTION
                // ************************************************************

                $.each(subscribers, function (index, subscriber) {
                    if (subscriber.subscriber == subscriberValue) {
                        activeSubscriber = subscriber;
                        usageActiveSubscriber = activeSubscriber.UsageOfferDataList;
                        return;
                    }
                });

                if($(e.currentTarget).data('search-info') == true){
                    // set flag search data
                    $(e.currentTarget).data('search-info', false);
                } else {
                    // set flag search data
                    $(e.currentTarget).data('search-info', true);

                    self.options.accountModel.getAccountUsage(

                        //parameters
                        app.utils.Storage.getSessionItem('token'),
                        app.utils.Storage.getSessionItem('selected-account-value'),
                        subscriberValue,

                        //success callback
                        function (data) {

                            if (!data.HasError) {

                                var html = '';

                                if (app.utils.tools.typeOfTelephony(subscribers[0].ProductType)=="Móvil") {

                                    var init = moment(data.cicleDateStart, 'MM/DD/YYYY'),
                                        end = moment(data.cicleDateEnd, 'MM/DD/YYYY'),
                                        totalDays = end.diff(init, 'days'),
                                        cicleDaysPast = moment().diff(init, 'days'),
                                        cicleDaysLeft = end.diff(moment(), 'days');

                                        data.porcDaysPast = (cicleDaysPast * 100) / totalDays;
                                        data.cicleDaysLeft = cicleDaysLeft;

                                    // update usage
                                    app.utils.Storage.setSessionItem('usage', data);

                                    mainPlan = usageActiveSubscriber[0];

                                    // find subscriber
                                    var subscriber = self.getSubscriber(subscriberValue);

                                    // update the subscriber
                                    app.utils.Storage.setSessionItem('selected-subscriber-value', subscriber);

                                    if(usageActiveSubscriber.length > 0){
                                        var firstOffer = usageActiveSubscriber.shift();
                                        var mainUsageRatio = firstOffer.Used / firstOffer.Quota,
                                            percentage = 100.0 * mainUsageRatio,
                                            widthSize = parseInt(Math.round(percentage)) + '%';
                                    }

                                    html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                        '<div class="tabcell">'+
                                            'PLAN'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="autobar diff-i">'+
                                        '<div class="container">'+
                                            '<div class="basicrow m-top">';
                                                if (mainPlan != '') {
                                                    html += '<div class="contspace full f-med f-black text-justify f-bold">'
                                                        + mainPlan.DisplayName +
                                                    '</div>';
                                                }else{
                                                    html += '<div class="contspace full f-med f-black text-justify f-bold">'
                                                        + data.DPlanName +
                                                    '</div>';
                                                }
                                            html += '</div>'+

                                            '<div class="g-btn text-center f-med f-white vcenter btn-change-plan">'+
                                                '<div class="tabcell">'+
                                                    'Cambiar Plan'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                        '<div class="tabcell">'+
                                            'CONSUMO'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="autobar">'+
                                        '<div class="container">'+
                                            '<div class="basicrow text-center">'+
                                                '<span class="f-little f-dgray din-b">Ciclo de Facturaci&oacute;n</span><br/>'+
                                                '<span class="f-med f-black din-b">'+ data.CicloFact +'</span><br/>'+
                                                '<div class="c100 p'+Math.round(data.porcDaysPast)+' text-center center vcenter">'+
                                                    '<span class="change f-black">'+
                                                        '<span class="f-little">Quedan</span>'+
                                                        '<span class="f-big-m din-b">'+ data.cicleDaysLeft +'</span>'+
                                                        '<span class="f-little">D&iacute;as</span>'+
                                                    '</span>'+
                                                  '<div class="slice">'+
                                                    '<div class="bar"></div>'+
                                                    '<div class="fill"></div>'+
                                                  '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="autobar">'+
                                        '<div class="container">'+
                                            '<div class="basicrow f-little din-b f-gray">Consumo Actual</div>';

                                            if (mainPlan != '') {

                                                var mainUsageRatio = mainPlan.Used / mainPlan.Quota;

                                                html += '<div class="basicrow m-top-ii">'+
                                                    '<div class="contspace full f-med f-dgray text-justify f-bold">'
                                                        + mainPlan.UsedText + ' de ' + mainPlan.QuotaText + ' Usados'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="basicrow m-top-ii">'+
                                                    '<div class="consmpt-bar">';
                                                         /*if (parseInt(subscriber.PUJ_USED) >= parseInt(subscriber.PUJ_LIMIT)) {
                                                             html += '<div class="consmpt-in data-progress-bar" style="width:75%"></div>';
                                                         } else {
                                                             html += '<div class="consmpt-in data-progress-bar" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                         }*/
                                                         if (mainUsageRatio <= 0.8) {
                                                            html += '<div class="consmpt-green" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                         } else if (mainUsageRatio > 0.8 && mainUsageRatio < 0.9)  {
                                                            html += '<div class="consmpt-yellow" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                         } else if (mainUsageRatio >= 0.9 && mainUsageRatio < 1.0) {
                                                            html += '<div class="consmpt-in " style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                         } else if (mainUsageRatio > 1.0 ) {
                                                            mainUsageRatio = 1.0;
                                                            html += '<div class="consmpt-in" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                                         }
                                                    html += '</div>'+
                                                '</div>';
                                            } else {

                                                '<div class="basicrow m-top-ii">'+
                                                    '<div class="contspace full f-med f-dgray text-justify f-bold">'
                                                        + data.DLocal + ' de ' + data.DLoaclTop + ' Usados'+
                                                    '</div>'+
                                                '</div>';

                                                var mainUsageRatio = mainPlan.Used / mainPlan.Quota;

                                                html += '<div class="basicrow m-top-ii">'+
                                                    '<div class="consmpt-bar">';
                                                         /*if (parseInt(subscriber.PUJ_USED) >= parseInt(subscriber.PUJ_LIMIT)) {
                                                             html += '<div class="consmpt-in data-progress-bar" style="width:75%"></div>';
                                                         } else {
                                                             html += '<div class="consmpt-in data-progress-bar" style="width: '+ data.DLoaclPorc.toString() +'%' +'"></div>';
                                                         }*/
                                                         if (mainUsageRatio <= 0.8) {
                                                             html += '<div class="consmpt-green" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                         } else if (mainUsageRatio > 0.8 && mainUsageRatio < 0.9)  {
                                                             html += '<div class="consmpt-yellow" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                         } else if (mainUsageRatio >= 0.9 && mainUsageRatio < 1.0) {
                                                             html += '<div class="consmpt-in" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                         } else if (mainUsageRatio > 1.0 ) {
                                                             mainUsageRatio = 1.0;
                                                             html += '<div class="consmpt-in" style="width: '+  mainUsageRatio.toString() +'%' +'"></div>';
                                                         }
                                                    html += '</div>'+
                                                '</div>';
                                            }

                                        html += '</div>'+
                                    '</div>'+

                                    '<div class="autobar diff-i">'+
                                        '<div class="container">'+
                                            '<div class="r-btn text-center f-med f-white vcenter btn-consumption" data-subscriber="' + subscriber.subscriber + '">'+
                                                '<div class="tabcell" >'+
                                                    'Ver Consumo'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';

                                } else {
                                    //html += '<div class="nbott din-b f-little text-center f-red m-error">Informaci&oacute;n no disponible para servicios de telefon&iacute;a fija.</div>';
                                    var html = '';

                                    if (mainPlan != undefined) {

                                        html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                '<div class="tabcell">'+
                                                    'PLAN'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="autobar diff-i">'+
                                                '<div class="container">'+
                                                    '<div class="basicrow m-top">';
                                                            html += '<div class="contspace full f-med f-black text-left f-bold">'
                                                            + mainPlan.DisplayName +
                                                              '</div>';
                                            html += '</div>';

                                            // validate if exits dsl order
                                            if(subscribers[0].DSLQualification !== undefined && subscribers[0].DSLQualification.dslOrder){
                                                html += '<div class="g-btn text-center f-med f-white vcenter btn-change-plan">'+
                                                            '<div class="tabcell">'+
                                                              'Cambiar Plan'+
                                                            '</div>'+
                                                        '</div>';
                                            }

                                            html +='</div>'+
                                                '</div>';

                                    }

                                    html += '<div class="autobar">'+
                                                '<div class="container">'+
                                                    '<div class="basicrow f-little din-b f-gray">Minutos Usados</div>'+

                                                    '<div class="basicrow m-top-ii">'+
                                                        '<div class="contspace full f-med f-black text-justify f-bold">'+
                                                            + data.MinutesCount +
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="autobar">'+
                                                '<div class="container">'+
                                                    '<div class="basicrow f-little din-b f-gray">Larga Distancia</div>'+

                                                    '<div class="basicrow m-top-ii">'+
                                                        '<div class="contspace full f-med f-black text-justify f-bold">'+
                                                            + data.LDQty +
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="autobar">'+
                                                '<div class="container">'+
                                                    '<div class="basicrow f-little din-b f-gray">Larga Distancia Internacional</div>'+

                                                    '<div class="basicrow m-top-ii">'+
                                                        '<div class="contspace full f-med f-black text-justify f-bold">'+
                                                            + data.LDQty +
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';
                                }

                                $('#phone'+currentIndex).html(html);

                            } else {
                                showAlert('Error', data.Desc, 'Aceptar');
                            }

                        },

                        // error function
                        app.utils.network.errorFunction
                    );

                }

                // ************************************************************
                // *************** CHANGE WITH A FUNCTION
                // ************************************************************

            }
            
        },

        applyCredits: function() {
            var self = this;

            const creditAmount = $.mobile.activePage.find('#credit-amount').val();
            const amount = this.formatNumber(creditAmount.replace("$", ""));
            const amtDue = this.formatNumber(app.utils.Storage.getSessionItem('amtDue'));

            if (amount > amtDue) {
                showConfirm(
                    'Confirmación',
                    'El monto del descuento a aplicar es mayor al balance pendiente de su factura, al aplicar este monto usted perdera el valor del descuento restante.',
                    ['Regresar','Aplicar'],
                    function(button){
                        if(button == 2) {
                            showAlert('', 'Credito aplicado', 'OK');
                        }
                    }
                );
            } else if (amount < amtDue) {
                showConfirm(
                    'Confirmación',
                    'El valor del credito a aplicar es menor al balance pendiente de su factura. Favor realizar el pago del remanente de su factura antes de la fecha de vencimiento.',
                    ['Regresar','Aplicar'],
                    function(button){
                        if(button == 2) {
                            showAlert('', 'Credito aplicado', 'OK');
                        }
                    }
                );
            }
        },

		doPayment: function(e) {
			
			var self = this,
				browser = null,
				dueAmount = $('#due-amount').val(),
	            selectedAccount = app.utils.Storage.getSessionItem('selected-account');
			
			this.options.accountModel.doPayment(
					
				//parameters
				app.utils.Storage.getSessionItem('token'),
				app.utils.Storage.getSessionItem('selected-account-value'),
				dueAmount,
			
				// success
				function(data) {
			
					if(!data.HasError){		
							
						// open payment url
						browser = app.utils.browser.show(data.Url,true);
						
						app.utils.loader.show();
								
						// success event load url				
						browser.addEventListener('loadstop', function(e) {
							
							// hiden loader
							app.utils.loader.hide();
			
							// show navegator
							browser.show();
						});	
			
						// success event load url				
						browser.addEventListener('loadstart', function(e) {
							
							if(e.url=='https://ebill.claropr.com/login/login.jsf' ||
								e.url=='https://ebill.claropr.com/login/home.jsf' ||
								e.url=='https://checkout.evertecinc.com/Close.aspx'){
								browser.close();
							}
						});
						
						// error event load url
						browser.addEventListener('loaderror', function(e) {
							
							// hiden loader
							app.utils.loader.hide();
			
							// close browser
							browser.close();
						});	
			
						browser.addEventListener('exit', function(e) {
			
							var paymentId = data.PaymentId;
			
                            // get Bill info    
                            self.options.accountModel.getAccountBill(
                                
                                //parameters
                                app.utils.Storage.getSessionItem('token'),
                            
                                app.utils.Storage.getSessionItem('selected-account-value'),

                                //success callback
                                function(data){									

                                    if(!data.HasError){					

                                        // account bill info
                                        app.utils.Storage.setSessionItem('accounts-bill-info', data);	
                                        
                                        self.render(function(){
                                        	$.mobile.activePage.trigger('pagecreate');
                                        });
                                        
                                    }else{

                                        showAlert('Error', data.Desc, 'Aceptar');

                                    }							

                                },

                                // error function
                                app.utils.network.errorFunction	
                            );
							
						});	 							
			
					}else{
						
						showAlert('Error', data.Desc, 'Aceptar');
			
					}
					
					// send analytics statistics
					if(analytics!=null){
						analytics.trackEvent('button', 'click', 'billPayment button');
					}
			
				},
			
				// error function
				app.utils.network.errorFunction	
			
			);			
			
		},
		
		showPopOver: function(e) {
            setTimeout(function(){
                $.mobile.activePage.find('[data-toggle="popover"]').popover('hide');
            },4000);
        },

		showConsumption: function(e) {
		    var self = this;
		    var subscriberValue = $(e.currentTarget).data('subscriber');
		    var subscribers = app.utils.Storage.getSessionItem('subscribers');
		    var accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            app.utils.Storage.setSessionItem('subscribers', subscribers);

            // cache
            app.utils.Storage.setSessionItem('subscribers-'+accountNumber, subscribers);

            $.each(subscribers, function (index, subscriber) {
                if (subscriber.subscriber == subscriberValue) {
                    app.utils.Storage.setSessionItem('selected-subscriber', subscriber);
                    return;
                }
            });

		    self.consumption();
		},

        notifications: function (e) {
            //showAlert('', 'notifications called menu', 'OK');
            app.router.navigate('notifications_app', {trigger: true});
        }
	
	});
});
