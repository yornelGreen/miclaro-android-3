package org.apache.cordova.plugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * This class get the app information called from JavaScript.
 */
public class MiClaroPlugin extends CordovaPlugin {
	
    public final String ACTION_GET_VERSION_NAME = "GetVersionName"; //not used in this version
    public final String ACTION_GET_VERSION_CODE = "GetVersionCode";
    private final String key = "123456";
    private final String salt = "123456";
	
    /**
     * Constructor.
     */
    public MiClaroPlugin() {
    }
    
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("build")) {
            this.build(callbackContext);
            return true;
        }
        
        if (action.equals("version")) {
            this.version(callbackContext);
            return true;
        }
        
        if (action.equals("send-payment-info")) {
        	String url = args.getString(0);
        	String json = args.getString(1);
        	JSONObject object = new JSONObject(json);
            this.sendPaymentInfo(url, object, callbackContext);
            return true;
        }
        
        if (action.equals("send-post-form")) {
        	String url = args.getString(0);
        	String json = args.getString(1);
        	JSONObject object = new JSONObject(json);
            this.sendPostForm(url, object, callbackContext);
            return true;
        }
        
        if (action.equals("telephone")) {
            TelephonyManager telephonyManager =
                (TelephonyManager)this.cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String result = telephonyManager.getLine1Number();
            if (result != null) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
               return true;
            }
        }
        
        return false;
    }

    private void build(CallbackContext callbackContext) {
    	
    	PackageManager packageManager = this.cordova.getActivity().getPackageManager();
        try {
			PackageInfo packageInfo = packageManager.getPackageInfo(this.cordova.getActivity().getApplicationContext().getPackageName(), 0);
			callbackContext.success(packageInfo.versionCode);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			callbackContext.error("There are a error in call.");
		}
    }
    
    private void version(CallbackContext callbackContext) {
    	
    	PackageManager packageManager = this.cordova.getActivity().getPackageManager();
        try {
			PackageInfo packageInfo = packageManager.getPackageInfo(this.cordova.getActivity().getApplicationContext().getPackageName(), 0);
			callbackContext.success(packageInfo.versionName);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			callbackContext.error("There are a error in call.");
		}
    }  
    
    private void sendPaymentInfo(final String url, final JSONObject input, final CallbackContext callbackContext) {
    	
    	final JSONObject encryptedData = new JSONObject();
    	final JSONObject response = new JSONObject();
    	String message = null;
    	AES aes = new AES();            	
    	
    	// Encrypt the data
    	try {
    		
			// set object with encrypted data
			encryptedData.put("cardNum", AES.encrypt((String)input.get("cardNum"), key, salt));
			encryptedData.put("nameOnCard", AES.encrypt((String)input.get("nameOnCard"), key, salt));
			encryptedData.put("expDate", AES.encrypt((String)input.get("expDate"), key, salt));
			encryptedData.put("amount", AES.encrypt((String)input.get("amount"), key, salt));
			encryptedData.put("zip", AES.encrypt((String)input.get("zip"), key, salt));
			encryptedData.put("street", AES.encrypt((String)input.get("street"), key, salt));
			encryptedData.put("cvNum", AES.encrypt((String)input.get("cvNum"), key, salt));
			encryptedData.put("aplicationID", input.get("aplicationID"));
			encryptedData.put("accountNumber", AES.encrypt((String)input.get("accountNumber"), key, salt));
			encryptedData.put("pcrftransaID", AES.encrypt((String)input.get("pcrftransaID"), key, salt));

		} catch (Exception e) {
			e.printStackTrace();
		}
        		
		cordova.getThreadPool().execute(new Runnable() {
			   @Override
			   public void run() {
				   try {
					   
					   JSONObject output;
						output = sendServerData(url, encryptedData);
						
						// set json response
						response.put("message", "Success");
						response.put("error", "false");
						response.put("response", output);
						
						callbackContext.success(response.toString());
				   } catch (Exception e) {
		    			e.printStackTrace();
						
						try {
							response.put("message", "Disculpe hubo un error tratando de realizar el pago, por favor intente nuevamente");
							response.put("error", "true");
							response.put("response", "");
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		    			
		    			callbackContext.error(response);
		    		}	
			   }
			});        		
    	
    }      
    
    private void sendPostForm(final String url, final JSONObject input, final CallbackContext callbackContext) {
        
    	if(url==null || input==null){
    		
    		callbackContext.error("Se esperan 2 parametros");
    		
    	} else {
        		
			cordova.getThreadPool().execute(new Runnable() {
				   @Override
				   public void run() {
					   try {
						   JSONObject output;
							output = sendServerData(url, input);
							callbackContext.success(output.toString());
					   } catch (Exception e) {
			    			// TODO Auto-generated catch block
			    			e.printStackTrace();
			    			callbackContext.error("There are a error in call.");
			    		}	
				   }
				});        		
    	}
    	
    }     
    
    @SuppressWarnings("deprecation")
    public static JSONObject sendServerData(String url, JSONObject object) throws Exception{

    	URL myURL = new URL(url);
    	HttpURLConnection myURLConnection = (HttpURLConnection)myURL.openConnection();
    	
    	try {
	    	
	    	myURLConnection.setRequestMethod("POST");
	    	myURLConnection.setRequestProperty("Content-Type", "application/json");
	    	myURLConnection.setRequestProperty("Accept", "application/json");
	    	
	    	myURLConnection.setRequestProperty("Content-Length", "" + Integer.toString(object.toString().getBytes().length));
	    	myURLConnection.setUseCaches(false);
	    	myURLConnection.setDoInput(true);
	    	myURLConnection.setDoOutput(true);
	
	    	// Send request
	    	OutputStream os = myURLConnection.getOutputStream();
	    	BufferedWriter writer = new BufferedWriter(
	    	        new OutputStreamWriter(os, "UTF-8"));
	    	writer.write(object.toString());
	    	writer.flush();
	    	writer.close();
	    	os.close();
	    	
	    	//Get Response	
	        InputStream is = myURLConnection.getInputStream();
	        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	        String line;
	        StringBuffer response = new StringBuffer(); 
	        while((line = rd.readLine()) != null) {
	          response.append(line);
	          response.append('\r');
	        }
	        rd.close();	        
	        
	        Log.i("sendServerData url:",  url);
	        Log.i("sendServerData message:",  response.toString());
	        
	        return new JSONObject(response.toString());
	        
    	} catch (Exception e) {
    		return new JSONObject();
    	} finally {
    		myURLConnection.disconnect();
    	}
    	
    }
	
}